package com.getjavajob.training.ivshukovd.socialnetwork.domain;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "friends")
public class Friendship implements Serializable {

    @EmbeddedId
    private FriendshipKey id;

    @ManyToOne
    @MapsId("accountIdFrom")
    private Account accountFrom;

    @ManyToOne
    @MapsId("accountIdTo")
    private Account accountTo;

    public FriendshipKey getId() {
        return id;
    }

    public void setId(FriendshipKey id) {
        this.id = id;
    }

    public Account getAccountFrom() {
        return accountFrom;
    }

    public void setAccountFrom(Account accountFrom) {
        this.accountFrom = accountFrom;
    }

    public Account getAccountTo() {
        return accountTo;
    }

    public void setAccountTo(Account accountTo) {
        this.accountTo = accountTo;
    }

    @Embeddable
    public static class FriendshipKey implements Serializable {

        private long accountIdFrom;
        private long accountIdTo;

        public FriendshipKey() {
        }

        public FriendshipKey(long accountIdFrom, long accountIdTo) {
            this.accountIdFrom = accountIdFrom;
            this.accountIdTo = accountIdTo;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            FriendshipKey that = (FriendshipKey) o;
            return Objects.equals(accountIdFrom, that.accountIdFrom) && Objects.equals(accountIdTo, that.accountIdTo);
        }

        @Override
        public int hashCode() {
            return Objects.hash(accountIdFrom, accountIdTo);
        }

    }

}