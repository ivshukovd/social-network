package com.getjavajob.training.ivshukovd.socialnetwork.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import static javax.persistence.CascadeType.REMOVE;

@Entity
@Table(name = "groups")
public class Group implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String description;
    private LocalDate creationDate;
    private long photoId;
    @OneToMany(mappedBy = "group", fetch = FetchType.LAZY, cascade = REMOVE)
    private List<GroupJoinRequest> jointRequests;
    @OneToMany(mappedBy = "group", fetch = FetchType.LAZY, cascade = REMOVE)
    private List<GroupMember> groupMembers;

    public long getPhotoId() {
        return photoId;
    }

    public void setPhotoId(long photoId) {
        this.photoId = photoId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Group {" + "id=" + id + ", name=" + name + ", description: " + description + '}';
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

}