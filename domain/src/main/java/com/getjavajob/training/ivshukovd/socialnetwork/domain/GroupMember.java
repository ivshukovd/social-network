package com.getjavajob.training.ivshukovd.socialnetwork.domain;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "groupMembers")
public class GroupMember implements Serializable {

    @EmbeddedId
    private GroupMemberKey id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("accountId")
    private Account account;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("groupId")
    private Group group;
    private String accountRole;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public GroupMemberKey getId() {
        return id;
    }

    public void setId(GroupMemberKey groupMemberKey) {
        this.id = groupMemberKey;
    }

    public String getAccountRole() {
        return accountRole;
    }

    public void setAccountRole(String accountRole) {
        this.accountRole = accountRole;
    }

    @Embeddable
    public static class GroupMemberKey implements Serializable {

        private long groupId;
        private long accountId;

        public GroupMemberKey() {
        }

        public GroupMemberKey(long groupId, long accountId) {
            this.groupId = groupId;
            this.accountId = accountId;
        }

        public long getGroupId() {
            return groupId;
        }

        public void setGroupId(long groupId) {
            this.groupId = groupId;
        }

        public long getAccountId() {
            return accountId;
        }

        public void setAccountId(long accountId) {
            this.accountId = accountId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            GroupMemberKey that = (GroupMemberKey) o;
            return Objects.equals(groupId, that.groupId) && Objects.equals(accountId, that.accountId);
        }

        @Override
        public int hashCode() {
            return Objects.hash(accountId, groupId);
        }

    }

}
