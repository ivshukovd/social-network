package com.getjavajob.training.ivshukovd.socialnetwork.domain.dto;

public class GroupDto {

    private long id;
    private String name;
    private long photoId;

    public GroupDto(long id, String name, long photoId) {
        this.id = id;
        this.name = name;
        this.photoId = photoId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPhotoId() {
        return photoId;
    }

    public void setPhotoId(long photoId) {
        this.photoId = photoId;
    }

}