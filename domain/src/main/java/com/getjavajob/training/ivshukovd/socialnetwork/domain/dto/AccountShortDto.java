package com.getjavajob.training.ivshukovd.socialnetwork.domain.dto;

public class AccountShortDto {

    private long id;
    private String firstName;
    private String surName;
    private long photoId;

    public AccountShortDto(long id, String firstName, String surName, long photoId) {
        this.id = id;
        this.firstName = firstName;
        this.surName = surName;
        this.photoId = photoId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public long getPhotoId() {
        return photoId;
    }

    public void setPhotoId(long photoId) {
        this.photoId = photoId;
    }

    @Override
    public String toString() {
        return "AccountShortDto{" + "id=" + id + ", firstName='" + firstName + '\'' + ", surName='" + surName + '\''
                + '}';
    }

}