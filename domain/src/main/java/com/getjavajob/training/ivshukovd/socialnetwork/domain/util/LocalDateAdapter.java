package com.getjavajob.training.ivshukovd.socialnetwork.domain.util;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static java.time.LocalDate.parse;
import static java.time.format.DateTimeFormatter.ofPattern;

public class LocalDateAdapter extends XmlAdapter<String, LocalDate> {

    private final DateTimeFormatter formatter = ofPattern("dd-MM-yyyy");

    @Override
    public LocalDate unmarshal(String xml) {
        return parse(xml, formatter);
    }

    @Override
    public String marshal(LocalDate localDate) {
        return localDate.format(formatter);
    }

}