package com.getjavajob.training.ivshukovd.socialnetwork.domain;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "groupJoinRequests")
public class GroupJoinRequest implements Serializable {

    @EmbeddedId
    private GroupJoinRequestKey id;
    @ManyToOne
    @MapsId("accountId")
    private Account account;
    @ManyToOne
    @MapsId("groupId")
    private Group group;

    public GroupJoinRequestKey getId() {
        return id;
    }

    public void setId(GroupJoinRequestKey id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @Embeddable
    public static class GroupJoinRequestKey implements Serializable {

        private long groupId;
        private long accountId;

        public GroupJoinRequestKey() {
        }

        public GroupJoinRequestKey(long groupId, long accountId) {
            this.groupId = groupId;
            this.accountId = accountId;
        }

        public long getGroupId() {
            return groupId;
        }

        public void setGroupId(long groupId) {
            this.groupId = groupId;
        }

        public long getAccountId() {
            return accountId;
        }

        public void setAccountId(long accountId) {
            this.accountId = accountId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            GroupJoinRequestKey that = (GroupJoinRequestKey) o;
            return Objects.equals(groupId, that.groupId) && Objects.equals(accountId, that.accountId);
        }

        @Override
        public int hashCode() {
            return Objects.hash(accountId, groupId);
        }

    }

}