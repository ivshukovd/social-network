package com.getjavajob.training.ivshukovd.socialnetwork.domain.util;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Phone;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Post;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.AccountDto;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.PostDto;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import static java.util.Objects.nonNull;

public final class EntityDtoConverter {

    private EntityDtoConverter() {
    }

    public static Account convertAccountDtoToEntity(AccountDto accountDto, Account account) {
        account.setFirstName(accountDto.getFirstName());
        account.setSurName(accountDto.getSurName());
        account.setMiddleName(accountDto.getMiddleName());
        account.setBirthDate(accountDto.getBirthDate());
        account.setHomeAddress(accountDto.getHomeAddress());
        account.setWorkAddress(accountDto.getWorkAddress());
        account.setEmail(accountDto.getEmail());
        if (nonNull(accountDto.getPassword())) {
            account.setPassword(accountDto.getPassword());
        }
        account.setIcq(accountDto.getIcq());
        account.setSkype(accountDto.getSkype());
        List<Phone> phones = new ArrayList<>();
        for (String newNumber : accountDto.getPhones()) {
            if (nonNull(newNumber)) {
                Phone phone = new Phone();
                phone.setAccount(account);
                phone.setNumber(newNumber);
                for (Phone oldPhone : account.getPhones()) {
                    if (oldPhone.equals(phone)) {
                        phone.setId(oldPhone.getId());
                    }
                }
                phones.add(phone);
            }
        }
        account.getPhones().clear();
        account.getPhones().addAll(phones);
        return account;
    }

    public static PostDto convertPostEntityToDto(Post post, TimeZone timeZone) {
        PostDto postDto = new PostDto();
        postDto.setId(post.getId());
        postDto.setDate(
                post.getDate().plusSeconds(post.getDate().atZone(timeZone.toZoneId()).getOffset().getTotalSeconds()));
        postDto.setAuthor(post.getAuthor().getId());
        postDto.setAuthorFirstName(post.getAuthor().getFirstName());
        postDto.setAuthorSurName(post.getAuthor().getSurName());
        postDto.setType(post.getType());
        postDto.setTargetId(post.getTargetId());
        postDto.setMessage(post.getMessage());
        postDto.setPhotoId(post.getPhotoId());
        postDto.setAuthorPhotoId(post.getAuthor().getPhotoId());
        return postDto;
    }

    public static Post convertPostDtoToEntity(PostDto postDto, Post post) {
        post.setDate(postDto.getDate());
        post.setType(postDto.getType());
        post.setTargetId(postDto.getTargetId());
        post.setMessage(postDto.getMessage());
        post.setPhotoId(postDto.getPhotoId());
        return post;
    }

}