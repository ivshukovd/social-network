<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
<head>
    <title>Edit account</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="${pageContext.request.contextPath}/webjars/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="${pageContext.request.contextPath}/webjars/jquery/jquery.min.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/css/bootstrap.min.css"
          type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" type="text/css">
</head>
<body>
<div class="container">
    <jsp:include page="search.jsp"/>
    <br>
    <div class="offset-lg-2 btn-group mb-3" role="group" aria-label="edit pages">
        <a href="${pageContext.request.contextPath}/editAccount/${accountId}">
            <button type="button" class="btn btn-outline-primary">Common</button>
        </a>
        <a href="${pageContext.request.contextPath}/changeAccountAvatar/${accountId}">
            <button type="button" class="btn btn-outline-primary">Photo</button>
        </a>
        <button type="button" class="btn btn-outline-primary active">Password</button>
    </div>
    <div class="row">
        <div class="col-lg-2">
            <jsp:include page="menu.jsp"/>
        </div>
        <div class="col-lg-10">
            <c:if test="${wrongPassword}">
                <p style="color: red">${message}</p>
            </c:if>
            <form method="post" action="${pageContext.request.contextPath}/changePassword">
                <input type="hidden" name="accountId" value="${accountId}"/>
                <div class="row">
                    <div class="col-lg-3">
                        <label class="form-label" for="oldPassword">Old password: </label>
                        <input type="password" class="form-control form-control-sm" id="oldPassword" name="oldPassword">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <label class="form-label" for="newPassword">New password: </label>
                        <input type="password" class="form-control form-control-sm" id="newPassword" name="newPassword">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <label class="form-label" for="repeatedPassword">Repeat new password: </label>
                        <input type="password" class="form-control form-control-sm" id="repeatedPassword"
                               name="repeatedPassword">
                    </div>
                </div>
                <br>
                <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                        data-bs-target="#confirmationDialog">Save
                    changes
                </button>
                <div class="modal fade" id="confirmationDialog" data-bs-backdrop="static" data-bs-keyboard="false"
                     tabindex="-1"
                     aria-labelledby="staticBackdrop" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <p>Do you want to save changes?</p>
                            </div>
                            <div class="modal-footer">
                                <input type="submit" class="btn btn-primary btn-sm" value="Yes">
                                <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">No
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
