<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <title>Search result</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="${pageContext.request.contextPath}/webjars/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="${pageContext.request.contextPath}/webjars/jquery/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/ajaxPagination.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/css/bootstrap.min.css"
          type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" type="text/css">
</head>
<body>
<div class="container">
    <jsp:include page="search.jsp"/>
    <br>
    <div class="row">
        <div class="col-lg-2">
            <jsp:include page="menu.jsp"/>
        </div>
        <div class="col-lg-3">
            <h5>Users</h5>
            <nav id="resultList" class="nav flex-column" aria-label="Search result">
                <c:forEach var="account" items="${accounts}">
                    <a class="nav-link active" href="${pageContext.request.contextPath}/account/${account.id}">
                        <c:choose>
                            <c:when test="${account.photoId > 0}">
                                <img class="account-avatar-small"
                                     src="${pageContext.request.contextPath}/image/${account.photoId}"
                                     alt="Account photo"/>
                            </c:when>
                            <c:otherwise>
                                <img class="account-avatar-small"
                                     src="${pageContext.request.contextPath}/resources/img/accountDefaultPhoto.jpg"
                                     alt="Account photo"/>
                            </c:otherwise>
                        </c:choose>
                            ${account.surName} ${account.firstName}
                    </a>
                </c:forEach>
            </nav>
        </div>
        <div class="w-100"></div>
        <br>
        <div class="col-lg-3 offset-lg-2">
            <input type="hidden" id="contextPath" value="${pageContext.request.contextPath}">
            <input type="hidden" id="searchedString" value="${search}">
            <input type="hidden" id="elementType" value="searchedAccount">
            <nav aria-label="Search results pages">
                <ul id="pages" class="pagination pagination-sm">
                    <c:forEach begin="1" end="3" var="i">
                        <c:choose>
                            <c:when test="${i eq 1}">
                                <li class="page-item active" aria-current="page">
                                    <span class="page-link">${i}</span>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <c:if test="${i le 3 && i le numberPages}">
                                    <li class="page-item">
                                        <span class="pageButton page-link" id="${i}">${i}</span>
                                    </li>
                                </c:if>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </ul>
            </nav>
        </div>
    </div>
</div>
</body>
</html>