<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="en">
<head>
    <title>Group creation</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/css/bootstrap.min.css"
          type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" type="text/css">
</head>
<body>
<div class="container">
    <jsp:include page="search.jsp"/>
    <br>
    <div class="row">
        <div class="col-lg-8 offset-lg-2">
            <h3>
                Create a new group
            </h3>
        </div>
        <div class="row">
            <div class="col-lg-2">
                <jsp:include page="menu.jsp"/>
            </div>
            <div class="col-lg-10">
                <div class="row mb-1">
                    <form method="post" action="${pageContext.request.contextPath}/createGroup">
                        <div class="row">
                            <div class="col-lg-3">
                                <label for="groupName" class="form-label">Group name:</label>
                                <input type="text" class="form-control form-control-sm" id="groupName" name="name"
                                       required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5">
                                <label for="description" class="form-label">Group description:</label>
                                <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                            </div>
                        </div>
                        <br>
                        <input type="submit" class="btn btn-primary btn-sm" value="Submit">
                        <a href="${pageContext.request.contextPath}/groups">Back</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>