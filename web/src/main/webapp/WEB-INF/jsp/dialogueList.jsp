<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <title>Search result</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
    <jsp:include page="search.jsp"/>
    <br>
    <div class="row">
        <div class="col-lg-8 offset-lg-2">
            <h3>
                Dialogues
            </h3>
        </div>
        <div class="row">
            <div class="col-lg-2">
                <jsp:include page="menu.jsp"/>
            </div>
            <div class="col-lg-6">
                <nav class="nav flex-column" aria-label="Dialog list">
                    <c:forEach var="dialogue" items="${dialogues}">
                        <c:choose>
                            <c:when test="${dialogue.account1.id eq sessionScope.loggedInAccountId}">
                                <a class="nav-link active"
                                   href="${pageContext.request.contextPath}/dialogue?dialoguePartnerId=${dialogue.account2.id}">
                                    <div class="card mb-1">
                                        <div class="card-body">
                                            <c:choose>
                                                <c:when test="${dialogue.account2.photoId > 0}">
                                                    <img class="account-avatar-small"
                                                         src="${pageContext.request.contextPath}/image/${dialogue.account2.photoId}"
                                                         alt="Account photo"/>
                                                </c:when>
                                                <c:otherwise>
                                                    <img class="account-avatar-small"
                                                         src="${pageContext.request.contextPath}/resources/img/accountDefaultPhoto.jpg"
                                                         alt="Account photo"/>
                                                </c:otherwise>
                                            </c:choose>
                                                ${dialogue.account2.surName} ${dialogue.account2.firstName}
                                        </div>
                                    </div>
                                </a>
                            </c:when>
                            <c:otherwise>
                                <a class="nav-link active"
                                   href="${pageContext.request.contextPath}/dialogue?dialoguePartnerId=${dialogue.account1.id}">
                                    <div class="card mb-1">
                                        <div class="card-body">
                                            <c:choose>
                                                <c:when test="${dialogue.account1.photoId > 0}">
                                                    <img class="account-avatar-small"
                                                         src="${pageContext.request.contextPath}/image/${dialogue.account1.photoId}"
                                                         alt="Account photo"/>
                                                </c:when>
                                                <c:otherwise>
                                                    <img class="account-avatar-small"
                                                         src="${pageContext.request.contextPath}/resources/img/accountDefaultPhoto.jpg"
                                                         alt="Account photo"/>
                                                </c:otherwise>
                                            </c:choose>
                                                ${dialogue.account1.surName} ${dialogue.account1.firstName}
                                        </div>
                                    </div>
                                </a>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </nav>
            </div>
            <div class="w-100"></div>
            <br>
            <div class="col-lg-3 offset-lg-2">
                <nav aria-label="Dialogs pages">
                    <ul class="pagination pagination-sm">
                        <c:if test="${page == numberPages && numberPages > 2}">
                            <li class="page-item">
                                <a class="page-link"
                                   href="${pageContext.request.contextPath}/dialogues?page=${page - 2}">${page - 2}</a>
                            </li>
                        </c:if>
                        <c:forEach begin="${page - 1}" end="${page + 1}" var="i">
                            <c:if test="${i <= numberPages && i > 0}">
                                <c:choose>
                                    <c:when test="${page eq i}">
                                        <li class="page-item active" aria-current="page">
                                            <span class="page-link">${i}</span>
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="page-item">
                                            <a class="page-link"
                                               href="${pageContext.request.contextPath}/dialogues?page=${i}">${i}</a>
                                        </li>
                                    </c:otherwise>
                                </c:choose>
                            </c:if>
                        </c:forEach>
                        <c:if test="${page == 1 && numberPages > 2}">
                            <li class="page-item">
                                <a class="page-link"
                                   href="${pageContext.request.contextPath}/dialogues?page=${3}">${3}</a>
                            </li>
                        </c:if>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
</body>
</html>