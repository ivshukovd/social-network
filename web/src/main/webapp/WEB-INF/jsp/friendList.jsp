<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <title>Search result</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="${pageContext.request.contextPath}/webjars/bootstrap/js/bootstrap.bundle.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/css/bootstrap.min.css"
          type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" type="text/css">
</head>
<body>
<div class="container">
    <jsp:include page="search.jsp"/>
    <br>
    <div class="row">
        <div class="col-lg-8 offset-lg-2">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <h5 class="nav-link active" aria-current="page">Friends</h5>
                </li>
                <li class="nav-item">
                    <h5><a class="nav-link" href="${pageContext.request.contextPath}/friendRequests">Requests</a></h5>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="col-lg-2">
                <jsp:include page="menu.jsp"/>
            </div>
            <div class="col-lg-3">
                <nav class="nav flex-column" aria-label="Friend list">
                    <c:forEach var="account" items="${accounts}">
                        <a class="nav-link active" href="${pageContext.request.contextPath}/account/${account.id}">
                            <c:choose>
                                <c:when test="${account.photoId > 0}">
                                    <img class="account-avatar-small"
                                         src="${pageContext.request.contextPath}/image/${account.photoId}"
                                         alt="Account photo"/>
                                </c:when>
                                <c:otherwise>
                                    <img class="account-avatar-small"
                                         src="${pageContext.request.contextPath}/resources/img/accountDefaultPhoto.jpg"
                                         alt="Account photo"/>
                                </c:otherwise>
                            </c:choose>
                                ${account.surName} ${account.firstName}
                        </a>
                    </c:forEach>
                </nav>
            </div>
            <div class="w-100"></div>
            <br>
            <div class="col-lg-3 offset-lg-2">
                <nav aria-label="Friend list pages">
                    <ul class="pagination pagination-sm">
                        <c:if test="${page == numberPages && numberPages > 2}">
                            <li class="page-item">
                                <a class="page-link"
                                   href="${pageContext.request.contextPath}/friends?page=${page - 2}">${page - 2}</a>
                            </li>
                        </c:if>
                        <c:forEach begin="${page - 1}" end="${page + 1}" var="i">
                            <c:if test="${i <= numberPages && i > 0}">
                                <c:choose>
                                    <c:when test="${page eq i}">
                                        <li class="page-item active" aria-current="page">
                                            <span class="page-link">${i}</span>
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="page-item">
                                            <a class="page-link"
                                               href="${pageContext.request.contextPath}/friends?page=${i}">
                                                    ${i}
                                            </a>
                                        </li>
                                    </c:otherwise>
                                </c:choose>
                            </c:if>
                        </c:forEach>
                        <c:if test="${page == 1 && numberPages > 2}">
                            <li class="page-item">
                                <a class="page-link"
                                   href="${pageContext.request.contextPath}/friends?page=${3}">${3}</a>
                            </li>
                        </c:if>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
</body>
</html>