<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>
<html lang="en">
<head>
    <title>Account page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="${pageContext.request.contextPath}/webjars/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="${pageContext.request.contextPath}/webjars/jquery/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/ajaxPagination.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/deletePost.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/imageAttachment.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/css/bootstrap.min.css"
          type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap-icons/font/bootstrap-icons.css"
          type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" type="text/css">
</head>
<body>
<div class="container">
    <jsp:include page="search.jsp"/>
    <br>
    <div class="row">
        <div class="col-lg-8 offset-lg-2">
            <h3>
                ${account.surName}
                ${account.firstName}
                <c:if test="${not empty account.middleName}">
                    ${account.middleName}
                </c:if>
            </h3>
        </div>
        <div class="col-lg-1">
            <c:if test="${ownPage || admin}">
                <a href="${pageContext.request.contextPath}/editAccount/${account.id}"
                   role="button" class="btn btn-primary btn-sm">Edit</a>
            </c:if>
        </div>
        <div class="col-lg-1">
            <form action="${pageContext.request.contextPath}/exportXml" method="get" id="downloadXML">
                <input type="hidden" name="accountId" value="${account.id}"/>
                <input class="btn btn-primary btn-sm" type="submit" value="Export XML">
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2">
            <jsp:include page="menu.jsp"/>
        </div>
        <div class="col-lg-3">
            <div class="row mb-1">
                <div class="col-lg-10">
                    <c:choose>
                        <c:when test="${account.photoId > 0}">
                            <img class="account-avatar-big"
                                 src="${pageContext.request.contextPath}/image/${account.photoId}" alt="Account photo"/>
                        </c:when>
                        <c:otherwise>
                            <img class="account-avatar-big"
                                 src="${pageContext.request.contextPath}/resources/img/accountDefaultPhoto.jpg"
                                 alt="Account photo"/>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
            <c:if test="${!follower && !ownPage && !friend}">
                <form action="${pageContext.request.contextPath}/addFriend" method="post">
                    <input type="hidden" name="accountIdToBeFollow" value="${account.id}"/>
                    <input type="hidden" name="currentPageURL" value="/account/${account.id}"/>
                    <div class="row mb-1">
                        <div class="col-lg-10">
                            <div class="d-grid gap-2">
                                <button type="submit" class="btn btn-primary btn-sm">Add friend</button>
                            </div>
                        </div>
                    </div>
                </form>
            </c:if>
            <c:if test="${friend}">
                <div class="row mb-1">
                    <form action="${pageContext.request.contextPath}/deleteFriend" method="post">
                        <input type="hidden" name="friendId" value="${account.id}"/>
                        <input type="hidden" name="currentPageURL" value="/account/${account.id}"/>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="d-grid gap-2">
                                    <button type="submit" class="btn btn-danger btn-sm">Delete friend</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </c:if>
            <c:if test="${(friend || admin) && !ownPage }">
                <div class="row mb-1">
                    <form action="${pageContext.request.contextPath}/dialogue" method="get">
                        <input type="hidden" name="dialoguePartnerId" value="${account.id}"/>
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="d-grid gap-2">
                                    <button type="submit" class="btn btn-primary btn-sm">Send message</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </c:if>
        </div>
        <div class="col-lg-7">
            <nav aria-label="navigation bar">
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <button class="nav-link active" id="nav-personal-info-tab" data-bs-toggle="tab"
                            data-bs-target="#nav-personal-info" type="button" role="tab"
                            aria-controls="nav-personal-info" aria-selected="true">Personal Info
                    </button>
                    <button class="nav-link" id="nav-contacts-tab" data-bs-toggle="tab" data-bs-target="#nav-contacts"
                            type="button" role="tab" aria-controls="nav-contacts" aria-selected="false">Contacts
                    </button>
                    <button class="nav-link" id="nav-phones-tab" data-bs-toggle="tab" data-bs-target="#nav-phones"
                            type="button" role="tab" aria-controls="nav-phones" aria-selected="false">Phones
                    </button>
                    <button class="nav-link" id="nav-addresses-tab" data-bs-toggle="tab" data-bs-target="#nav-addresses"
                            type="button" role="tab" aria-controls="nav-addresses" aria-selected="false">Addresses
                    </button>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-personal-info" role="tabpanel"
                     aria-labelledby="nav-personal-info-tab">
                    <div class="row">
                        <div class="col-lg-2">
                            <p>Birth date: </p>
                        </div>
                        <div class="col">
                            <p>
                                <javatime:format value="${account.birthDate}" pattern="dd.MM.yyyy"/>
                            </p>
                        </div>
                    </div>
                    <br>
                    <p class="registration-date">
                        Registration date: <javatime:format value="${account.registrationDate}" pattern="dd.MM.yyyy"/>
                    </p>
                </div>
                <div class="tab-pane fade" id="nav-contacts" role="tabpanel" aria-labelledby="nav-contacts-tab">
                    <div class="row">
                        <div class="col-lg-2">
                            <p>E-mail: </p>
                        </div>
                        <div class="col-lg-10">
                            <p>${account.email}</p>
                        </div>
                        <c:if test="${not empty account.skype}">
                            <div class="col-lg-2">
                                <p>Skype: </p>
                            </div>
                            <div class="col-lg-10">
                                <p>${account.skype}</p>
                            </div>
                        </c:if>
                        <c:if test="${not empty account.icq}">
                            <div class="col-lg-2">
                                <p>ICQ: </p>
                            </div>
                            <div class="col-lg-10">
                                <p>${account.icq}</p>
                            </div>
                        </c:if>
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-phones" role="tabpanel" aria-labelledby="nav-phones-tab">
                    <c:forEach var="phone" items="${account.phones}">
                        <p>${phone.number}</p>
                    </c:forEach>
                </div>
                <div class="tab-pane fade" id="nav-addresses" role="tabpanel" aria-labelledby="nav-addresses-tab">
                    <div class="row">
                        <c:if test="${not empty account.homeAddress}">
                            <div class="col-lg-2">
                                <p>Home: </p>
                            </div>
                            <div class="col-lg-10">
                                <p>${account.homeAddress}</p>
                            </div>
                        </c:if>
                        <c:if test="${not empty account.workAddress}">
                            <div class="col-lg-2">
                                <p>Work: </p>
                            </div>
                            <div class="col-lg-10">
                                <p>${account.workAddress}</p>
                            </div>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
        <c:if test="${ownPage || admin || friend}">
            <div class="col-lg-7 offset-lg-2">
                <form id="wallPostForm" method="post" action="${pageContext.request.contextPath}/accountWallPost"
                      enctype="multipart/form-data">
                    <input type="hidden" id="author" name="author" value="${sessionScope.loggedInAccountId}"/>
                    <input type="hidden" id="wallType" name="wallType" value="ACCOUNT"/>
                    <input type="hidden" id="targetId" name="targetId" value="${account.id}"/>
                    <textarea class="form-control form-control-sm mb-1" rows="2" name="message"
                              placeholder="What are you thinking about?" aria-label="wall post text"></textarea>
                    <input type="file" class="form-control form-control-sm" id="image" name="image"
                           style="display: none" accept=".png, .jpg, .jpeg"/>
                    <input class="btn btn-primary btn-sm" type="submit" value="Post">
                    <button type="button" id="attachImage" class="btn btn-outline-primary btn-sm"
                            onclick="attachFile()">
                        Attach image
                    </button>
                </form>
                <br>
                <div id="resultList">
                    <c:forEach var="post" items="${posts}">
                        <div class="wall-post">
                            <div class="wall-post-body">
                                <div class="row mb-3">
                                    <div class="col-7">
                                        <a href="${pageContext.request.contextPath}/account/${post.author}">
                                            <c:choose>
                                                <c:when test="${post.authorPhotoId > 0}">
                                                    <img class="account-avatar"
                                                         src="${pageContext.request.contextPath}/image/${post.authorPhotoId}"
                                                         alt="Account photo"/>
                                                </c:when>
                                                <c:otherwise>
                                                    <img class="account-avatar"
                                                         src="${pageContext.request.contextPath}/resources/img/accountDefaultPhoto.jpg"
                                                         alt="Account photo"/>
                                                </c:otherwise>
                                            </c:choose>
                                            <span class="account-name">${post.authorSurName} ${post.authorFirstName}</span>
                                        </a>
                                    </div>
                                    <div class="offset-lg-4 col-lg-1">
                                        <em class="bi bi-trash" id="deletePost-${post.id}"></em>
                                    </div>
                                </div>
                                <c:if test="${post.photoId > 0}">
                                    <div class="row mb-3">
                                        <img class="content-image"
                                             src="${pageContext.request.contextPath}/image/${post.photoId}"
                                             alt="Attached image">
                                    </div>
                                </c:if>
                                <div class="row post-text mb-1">
                                    <span>${post.message}</span>
                                </div>
                                <div class="row post-date-time text-end">
                                    <p>
                                        <javatime:format value="${post.date}" pattern="dd.MM.yyyy HH:mm"/>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
            <div class="w-100"></div>
            <br>
            <c:if test="${!empty posts}">
                <div class="col-lg-3 offset-lg-2">
                    <input type="hidden" id="contextPath" value="${pageContext.request.contextPath}">
                    <input type="hidden" id="elementType" value="accountWallPost">
                    <nav aria-label="Search results pages">
                        <ul id="pages" class="pagination pagination-sm">
                            <c:forEach begin="1" end="3" var="i">
                                <c:choose>
                                    <c:when test="${i eq 1}">
                                        <li class="page-item active" aria-current="page">
                                            <span class="pageButton page-link" id="${i}">${i}</span>
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <c:if test="${i le 3 && i le numberPages}">
                                            <li class="page-item">
                                                <span class="pageButton page-link" id="${i}">${i}</span>
                                            </li>
                                        </c:if>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </ul>
                    </nav>
                </div>
            </c:if>
        </c:if>
    </div>
</div>
</body>
</html>