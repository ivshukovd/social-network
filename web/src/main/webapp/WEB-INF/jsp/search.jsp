<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <title>Search</title>
    <script src="${pageContext.request.contextPath}/webjars/jquery/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/webjars/jquery-ui/jquery-ui.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/searchAutocomplete.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/jquery-ui/jquery-ui.min.css"
          type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/css/bootstrap.min.css"
          type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" type="text/css">
</head>
<body>
<div class="container">
    <nav class="navbar navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand">My Social Network</a>
            <form class="d-flex" method="get" action="${pageContext.request.contextPath}/search">
                <select id="searchedElement" name="searchedElement" class="form-select-sm"
                        aria-label="Select element type">
                    <option value="account" selected>Account</option>
                    <option value="group">Group</option>
                </select>
                <input type="hidden" id="contextPath" value="${pageContext.request.contextPath}">
                <input id="search" class="form-control form-control-sm" name="search" type="search" placeholder="Search"
                       aria-label="Search">
                <button class="btn btn-outline-primary btn-sm" type="submit">Search</button>
            </form>
        </div>
    </nav>
</div>
</body>
</html>