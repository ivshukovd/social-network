<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
<head>
    <title>Edit account</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="${pageContext.request.contextPath}/webjars/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="${pageContext.request.contextPath}/webjars/jquery/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/operationsWithPhoneNumbers.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/xmlImport.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/inputDataValidation.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/5.1.0/css/bootstrap.min.css"
          type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" type="text/css">
</head>
<body onload="initCount()">
<input type="hidden" id="contextPath" value="${pageContext.request.contextPath}">
<input type="hidden" id="accountId" value="${account.id}">
<div class="container">
    <jsp:include page="search.jsp"/>
    <br>
    <div class="offset-lg-2 btn-group mb-3" role="group" aria-label="edit pages">
        <button type="button" class="btn btn-outline-primary active">Common</button>
        <c:if test="${ownPage}">
            <a href="${pageContext.request.contextPath}/changeAccountAvatar/${account.id}">
                <button type="button" class="btn btn-outline-primary">Photo</button>
            </a>
            <a href="${pageContext.request.contextPath}/changePassword/${account.id}">
                <button type="button" class="btn btn-outline-primary">Password</button>
            </a>
        </c:if>
        <input type="file" id="fileXML" name="fileXML" style="display: none" accept=".xml">
        <button type="button" id="uploadFileButton" class="btn btn-outline-success btn-sm" onclick="uploadFile()">
            Import XML
        </button>
    </div>
    <div class="row">
        <div class="col-lg-2">
            <jsp:include page="menu.jsp"/>
        </div>
        <div class="col-lg-10">
            <div class="row">
                <form method="post" action="${pageContext.request.contextPath}/editAccount">
                    <input type="hidden" name="accountId" value="${account.id}"/>
                    <div class="row">
                        <div class="col-lg-3">
                            <label for="updatedEmail" class="form-label">E-mail:</label>
                            <input type="email" class="form-control form-control-sm unique" id="updatedEmail"
                                   name="email"
                                   value="${account.email}" required>
                            <div id="updatedEmailExists" class="form-text">Such e-mail already exists</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2">
                            <label class="form-label" for="firstName">First name: </label>
                            <input type="text" class="form-control form-control-sm" id="firstName" name="firstName"
                                   value="${account.firstName}" required>
                        </div>
                        <div class="col-lg-2">
                            <label class="form-label" for="middleName">Middle name: </label>
                            <input type="text" class="form-control form-control-sm" id="middleName" name="middleName"
                                   value="${account.middleName}">
                        </div>
                        <div class="col-lg-2">
                            <label class="form-label" for="surName"> Second name: </label>
                            <input type="text" class="form-control form-control-sm" id="surName" name="surName"
                                   value="${account.surName}" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <label class="form-label" for="birthDate">Birth date: </label>
                            <input type="date" class="form-control form-control-sm" id="birthDate" name="birthDate"
                                   min="1920-01-01"
                                   max="2030-01-01" value="${account.birthDate}" required>
                        </div>
                    </div>
                    <div class="row">
                        <label class="form-label">Phones: </label>
                    </div>
                    <div id="phoneFields">
                        <c:forEach var="phone" items="${account.phones}" varStatus="count">
                            <div class="row mb-3" id="${phone.number}">
                                <div class="col-lg-3">
                                    <input type="tel" class="form-control form-control-sm" name="phones[${count.index}]"
                                           pattern="[0-9]{11}" maxlength="11" value="${phone.number}"
                                           aria-label="phone number" readonly>
                                </div>
                                <div class="col-lg-1">
                                    <button type="button" class="btn btn-danger btn-sm btn-block"
                                            onclick="deletePhone(${phone.number})">
                                        delete
                                    </button>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-3">
                            <input type="tel" class="form-control form-control-sm unique" id="newPhone"
                                   pattern="[0-9]{11}"
                                   maxlength="11" aria-label="new phone number">
                            <div id="newPhoneExists" class="form-text">Such phone already exists</div>
                        </div>
                        <div class="col-lg-1">
                            <button id="addPhoneButton" type="button" class="btn btn-primary btn-sm btn-block"
                                    onclick="addPhone()">add
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <label for="updatedIcq" class="form-label">ICQ: </label>
                            <input type="text" class="form-control form-control-sm unique" id="updatedIcq" name="icq"
                                   value="${account.icq}">
                            <div id="updatedIcqExists" class="form-text">Such icq already exists</div>
                        </div>
                        <div class="col-lg-3">
                            <label for="updatedSkype" class="form-label">Skype: </label>
                            <input type="text" class="form-control form-control-sm unique" id="updatedSkype"
                                   name="skype"
                                   value="${account.skype}">
                            <div id="updatedSkypeExists" class="form-text">Such skype already exists</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <label for="homeAddress" class="form-label">Home address: </label>
                            <input type="text" class="form-control form-control-sm" id="homeAddress" name="homeAddress"
                                   value="${account.homeAddress}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <label for="workAddress" class="form-label">Work address: </label>
                            <input type="text" class="form-control form-control-sm" id="workAddress" name="workAddress"
                                   value="${account.workAddress}">
                        </div>
                    </div>
                    <br>
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                            data-bs-target="#confirmationDialog">Save
                        changes
                    </button>
                    <div class="modal fade" id="confirmationDialog" data-bs-backdrop="static" data-bs-keyboard="false"
                         tabindex="-1"
                         aria-labelledby="staticBackdrop" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <p>Do you want to save changes?</p>
                                </div>
                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-primary btn-sm" value="Yes">
                                    <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">No
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <br>
            <c:if test="${!account.admin && !ownPage}">
                <form method="post" action="${pageContext.request.contextPath}/account/makeAdmin">
                    <input type="hidden" name="accountToMakeAdmin" value="${account.id}"/>
                    <button type="submit" class="btn btn-primary">Make admin</button>
                </form>
                <br>
            </c:if>
            <c:if test="${admin && (ownPage || !account.admin)}">
                <form method="post" action="${pageContext.request.contextPath}/account/delete">
                    <input type="hidden" name="accountToBeDelete" value="${account.id}"/>
                    <button type="submit" class="btn btn-danger">Delete account</button>
                </form>
            </c:if>
        </div>
    </div>
</body>
</html>