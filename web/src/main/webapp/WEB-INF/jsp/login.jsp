<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/css/bootstrap.min.css"
          type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" type="text/css">
    <title>Login</title>
</head>
<body>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-3 mt-5">
            <h1 class="mb-3">Login</h1>
            <c:if test="${error}">
                <p style="color: red">Either e-mail or password is wrong</p>
            </c:if>
            <form method="post" action="${pageContext.request.contextPath}/login">
                <label for="email" class="form-label visually-hidden">Email address</label>
                <input type="email" class="form-control form-control-sm login-email" id="email" name="email"
                       placeholder="Email">
                <label for="password" class="form-label visually-hidden">Password</label>
                <input type="password" class="form-control form-control-sm login-password" id="password" name="password"
                       placeholder="Password">
                <div class="form-check mt-2">
                    <input type="checkbox" class="form-check-input" id="remember" name="remember" value="true">
                    <label class="form-check-label" for="remember">Remember me</label>
                </div>
                <button type="submit" class="btn btn-primary btn-sm mt-2">login</button>
            </form>
            <a class="mt-2" href="${pageContext.request.contextPath}/registerAccount">Register account</a>
        </div>
    </div>
</div>
</body>
</html>