<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <title>Search result</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="${pageContext.request.contextPath}/webjars/bootstrap/js/bootstrap.bundle.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/css/bootstrap.min.css"
          type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" type="text/css">
</head>
<body>
<div class="container">
    <jsp:include page="search.jsp"/>
    <br>
    <div class="row">
        <div class="col-lg-8 offset-lg-2">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <h5 class="nav-link active" aria-current="page">Members</h5>
                </li>
                <c:if test="${groupAdmin || groupCreator}">
                    <li class="nav-item">
                        <h5><a class="nav-link"
                               href="${pageContext.request.contextPath}/groupJoinRequests?groupId=${groupId}">Join
                            Requests</a></h5>
                    </li>
                </c:if>
            </ul>
        </div>
        <div class="row">
            <div class="col-lg-2">
                <jsp:include page="menu.jsp"/>
            </div>
            <div class="col-lg-5">
                <nav class="nav flex-lg-column" aria-label="Group members">
                    <div class="row">
                        <c:forEach var="member" items="${members}">
                            <div class="col-lg-5">
                                <a class="nav-link active"
                                   href="${pageContext.request.contextPath}/account/${member.id}">
                                    <c:choose>
                                        <c:when test="${member.photoId > 0}">
                                            <img class="account-avatar-small"
                                                 src="${pageContext.request.contextPath}/image/${member.photoId}"
                                                 alt="Account photo"/>
                                        </c:when>
                                        <c:otherwise>
                                            <img class="account-avatar-small"
                                                 src="${pageContext.request.contextPath}/resources/img/accountDefaultPhoto.jpg"
                                                 alt="Account photo"/>
                                        </c:otherwise>
                                    </c:choose>
                                        ${member.surName} ${member.firstName}
                                </a>
                            </div>
                            <c:forEach var="role" items="${roles}">
                                <c:if test="${role.key eq member.id}">
                                    <c:set var="memberRole" value="${role.value}"/>
                                </c:if>
                            </c:forEach>
                            <c:if test="${(groupAdmin || groupCreator) && memberRole eq 'member'}">
                                <div class="nav-link col-lg-3">
                                    <form action="${pageContext.request.contextPath}/makeGroupAdmin" method="post">
                                        <input type="hidden" name="accountId" value="${member.id}"/>
                                        <input type="hidden" name="groupId" value="${groupId}"/>
                                        <input type="hidden" name="page" value="${page}"/>
                                        <input type="submit" class="btn btn-primary btn-sm" value="Make admin"/>
                                    </form>
                                </div>
                            </c:if>
                            <c:if test="${groupCreator && memberRole eq 'admin'}">
                                <div class="nav-link col-lg-3">
                                    <form action="${pageContext.request.contextPath}/makeGroupMember" method="post">
                                        <input type="hidden" name="accountId" value="${member.id}"/>
                                        <input type="hidden" name="groupId" value="${groupId}"/>
                                        <input type="hidden" name="page" value="${page}"/>
                                        <input type="submit" class="btn btn-danger btn-sm" value="Make member"/>
                                    </form>
                                </div>
                            </c:if>
                            <c:if test="${groupAdmin && memberRole eq 'member'
                        || groupCreator && member.id ne sessionScope.loggedInAccountId}">
                                <div class="nav-link col-lg-3">
                                    <form action="${pageContext.request.contextPath}/deleteGroupMember" method="post">
                                        <input type="hidden" name="accountId" value="${member.id}"/>
                                        <input type="hidden" name="groupId" value="${groupId}"/>
                                        <input type="hidden" name="page" value="${page}"/>
                                        <input type="submit" class="btn btn-danger btn-sm" value="Delete from group"/>
                                    </form>
                                </div>
                            </c:if>
                            <div class="w-100"></div>
                        </c:forEach>
                    </div>
                </nav>
            </div>
            <div class="w-100"></div>
            <br>
            <div class="col-lg-3 offset-lg-2">
                <nav aria-label="Members pages">
                    <ul class="pagination pagination-sm">
                        <c:if test="${page == numberPages && numberPages > 2}">
                            <li class="page-item">
                                <a class="page-link"
                                   href="${pageContext.request.contextPath}/showGroupMembers?page=${page - 2}&groupId=${groupId}">${page - 2}</a>
                            </li>
                        </c:if>
                        <c:forEach begin="${page - 1}" end="${page + 1}" var="i">
                            <c:if test="${i <= numberPages && i > 0}">
                                <c:choose>
                                    <c:when test="${page eq i}">
                                        <li class="page-item active" aria-current="page">
                                            <span class="page-link">${i}</span>
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="page-item">
                                            <a class="page-link"
                                               href="${pageContext.request.contextPath}/showGroupMembers?page=${i}&groupId=${groupId}">${i}</a>
                                        </li>
                                    </c:otherwise>
                                </c:choose>
                            </c:if>
                        </c:forEach>
                        <c:if test="${page == 1 && numberPages > 2}">
                            <li class="page-item">
                                <a class="page-link"
                                   href="${pageContext.request.contextPath}/showGroupMembers?page=${3}&groupId=${groupId}">${3}</a>
                            </li>
                        </c:if>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
</body>
</html>