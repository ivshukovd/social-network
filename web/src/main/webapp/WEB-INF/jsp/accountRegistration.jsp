<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="en">
<head>
    <title>Account Registration</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="${pageContext.request.contextPath}/webjars/jquery/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/inputDataValidation.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/css/bootstrap.min.css"
          type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" type="text/css">
</head>
<body>
<input type="hidden" id="contextPath" value="${pageContext.request.contextPath}">
<div class="container">
    <h1>Register your account</h1>
    <br>
    <form method="post" action="${pageContext.request.contextPath}/registerAccount">
        <div class="row">
            <div class="col-lg-3">
                <label for="email" class="form-label required">E-mail:</label>
                <input type="email" class="form-control form-control-sm unique" id="email" name="email" required>
                <div id="emailExists" class="form-text">Such e-mail already exists</div>
            </div>
            <div class="col-lg-3">
                <label class="form-label required" for="password">Password: </label>
                <input type="password" class="form-control form-control-sm" id="password" name="password" required>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2">
                <label class="form-label required" for="firstName">First name: </label>
                <input type="text" class="form-control form-control-sm" id="firstName" name="firstName" required>
            </div>
            <div class="col-lg-2">
                <label class="form-label" for="middleName">Middle name: </label>
                <input type="text" class="form-control form-control-sm" id="middleName" name="middleName">
            </div>
            <div class="col-lg-2">
                <label class="form-label required" for="surName"> Second name: </label>
                <input type="text" class="form-control form-control-sm" id="surName" name="surName" required>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <label class="form-label required" for="birthDate">Birth date: </label>
                <input type="date" class="form-control form-control-sm" id="birthDate" name="birthDate" min="1920-01-01"
                       max="2030-01-01" required>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <label class="form-label required" for="phone">Phone: </label>
                <input type="tel" class="form-control form-control-sm unique" id="phone" name="phones[0]"
                       pattern="[0-9]{11}"
                       maxlength="11" placeholder="79991231212" required>
                <div id="phoneExists" class="form-text">Such phone already exists</div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <label for="icq" class="form-label">ICQ: </label>
                <input type="text" class="form-control form-control-sm unique" id="icq" name="icq">
                <div id="icqExists" class="form-text">Such icq already exists</div>
            </div>
            <div class="col-lg-3">
                <label for="skype" class="form-label">Skype: </label>
                <input type="text" class="form-control form-control-sm unique" id="skype" name="skype">
                <div id="skypeExists" class="form-text">Such skype already exists</div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <label for="homeAddress" class="form-label">Home address: </label>
                <input type="text" class="form-control form-control-sm" id="homeAddress" name="homeAddress">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <label for="workAddress" class="form-label">Work address: </label>
                <input type="text" class="form-control form-control-sm" id="workAddress" name="workAddress">
            </div>
        </div>
        <br>
        <input type="submit" class="btn btn-primary btn-sm" value="Submit">
        <a href="${pageContext.request.contextPath}/login">Back</a>
    </form>
</div>
</body>
</html>