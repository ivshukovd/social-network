<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="en">
<head>
    <title>Menu</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/css/bootstrap.min.css"
          type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" type="text/css">
</head>
<body>
<ul class="nav flex-column">
    <li class="nav-item">
        <a class="nav-link" href="${pageContext.request.contextPath}/account/${sessionScope.loggedInAccountId}">
            My Page</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="${pageContext.request.contextPath}/friends">My Friends</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="${pageContext.request.contextPath}/groups">My Groups</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="${pageContext.request.contextPath}/dialogues">My Messages</a>
    </li>
    <br>
    <li class="nav-item">
        <form method="get" style="padding: 0.5rem 1rem" action="${pageContext.request.contextPath}/logout">
            <input type="submit" class="btn btn-primary btn-sm" value="Logout">
        </form>
    </li>
</ul>
</body>
</html>