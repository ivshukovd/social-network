<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
<head>
    <title>Edit account</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/5.1.0/css/bootstrap.min.css"
          type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" type="text/css">
</head>
<body>
<div class="container">
    <jsp:include page="search.jsp"/>
    <br>
    <div class="offset-lg-2 btn-group mb-3" role="group" aria-label="edit pages">
        <a href="${pageContext.request.contextPath}/editAccount/${accountId}">
            <button type="button" class="btn btn-outline-primary">Common</button>
        </a>
        <button type="button" class="btn btn-outline-primary active">Photo</button>
        <a href="${pageContext.request.contextPath}/changePassword/${accountId}">
            <button type="button" class="btn btn-outline-primary">Password</button>
        </a>
    </div>
    <div class="row">
        <div class="col-lg-2">
            <jsp:include page="menu.jsp"/>
        </div>
        <div class="col-lg-10">
            <div class="row">
                <div class="col-lg-3">
                    <c:choose>
                        <c:when test="${accountPhotoId > 0}">
                            <img class="account-avatar-big"
                                 src="${pageContext.request.contextPath}/image/${accountPhotoId}" alt="Account photo"/>
                        </c:when>
                        <c:otherwise>
                            <img class="account-avatar-big"
                                 src="${pageContext.request.contextPath}/resources/img/accountDefaultPhoto.jpg"
                                 alt="Account photo"/>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
            <br>
            <form action="${pageContext.request.contextPath}/uploadAccountPhoto" method="post"
                  enctype="multipart/form-data">
                <input type="hidden" name="accountId" value="${accountId}"/>
                <div class="row">
                    <div class="col-lg-3">
                        <input type="file" class="form-control form-control-sm" id="accPhoto" name="accPhoto"
                               accept=".png, .jpg, .jpeg"/>
                    </div>
                    <div class="col-lg-3">
                        <input type="submit" class="btn btn-primary btn-sm" value="Change photo"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
