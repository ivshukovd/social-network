<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <title>Search result</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="${pageContext.request.contextPath}/webjars/bootstrap/js/bootstrap.bundle.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/css/bootstrap.min.css"
          type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" type="text/css">
</head>
<body>
<div class="container">
    <jsp:include page="search.jsp"/>
    <br>
    <div class="row">
        <div class="col-lg-8 offset-lg-2">
            <h3>
                Groups
            </h3>
        </div>
        <div class="row">
            <div class="col-lg-2">
                <jsp:include page="menu.jsp"/>
            </div>
            <div class="col-lg-6">
                <div class="nav flex-lg-column">
                    <div class="row">
                        <c:forEach var="group" items="${groups}">
                            <div class="col-lg-12">
                                <a class="nav-link active" href="${pageContext.request.contextPath}/group/${group.id}">
                                    <c:choose>
                                        <c:when test="${group.photoId > 0}">
                                            <img class="account-avatar-small"
                                                 src="${pageContext.request.contextPath}/image/${group.photoId}"
                                                 alt="Group photo"/>
                                        </c:when>
                                        <c:otherwise>
                                            <img class="account-avatar-small"
                                                 src="${pageContext.request.contextPath}/resources/img/groupDefaultPhoto.jpg"
                                                 alt="Group photo"/>
                                        </c:otherwise>
                                    </c:choose>
                                        ${group.name}
                                </a>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <a href="${pageContext.request.contextPath}/createGroup"
                   role="button" class="btn btn-primary btn-sm">Create group</a>
            </div>
            <div class="w-100"></div>
            <br>
            <div class="col-lg-3 offset-lg-2">
                <nav aria-label="Groups pages">
                    <ul class="pagination pagination-sm">
                        <c:forEach begin="1" end="${numberPages}" var="i">
                            <c:choose>
                                <c:when test="${page eq i}">
                                    <li class="page-item active" aria-current="page">
                                        <span class="page-link">${i}</span>
                                    </li>
                                </c:when>
                                <c:otherwise>
                                    <li class="page-item">
                                        <a class="page-link"
                                           href="${pageContext.request.contextPath}/groups/${i}">${i}</a>
                                    </li>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
</body>
</html>