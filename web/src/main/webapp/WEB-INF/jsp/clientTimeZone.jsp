<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="en">
<head>
    <title>Client time zone</title>
</head>
<body>
<form method="post" id="tzForm" action="${pageContext.request.contextPath}/setClientTimeZone">
    <input id="zoneId" type="hidden" name="timeZoneId">
</form>
<script>
    window.onload = function () {
        document.getElementById("zoneId").value = Intl.DateTimeFormat().resolvedOptions().timeZone;
        document.getElementById("tzForm").submit();
    }
</script>
</body>
</html>
