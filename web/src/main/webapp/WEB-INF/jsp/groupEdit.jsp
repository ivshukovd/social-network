<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <title>Edit group</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="${pageContext.request.contextPath}/webjars/bootstrap/js/bootstrap.bundle.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/css/bootstrap.min.css"
          type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" type="text/css">
</head>
<body>
<div class="container">
    <jsp:include page="search.jsp"/>
    <br>
    <div class="row">
        <div class="col-lg-2">
            <jsp:include page="menu.jsp"/>
        </div>
        <div class="col-lg-10">
            <div class="row">
                <div class="col-lg-3">
                    <c:choose>
                        <c:when test="${group.photoId > 0}">
                            <img class="account-avatar-big"
                                 src="${pageContext.request.contextPath}/image/${group.photoId}" alt="Group photo"/>
                        </c:when>
                        <c:otherwise>
                            <img class="account-avatar-big"
                                 src="${pageContext.request.contextPath}/resources/img/groupDefaultPhoto.jpg"
                                 alt="Group photo"/>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
            <br>
            <form action="${pageContext.request.contextPath}/uploadGroupPhoto" method="post"
                  enctype="multipart/form-data">
                <input type="hidden" name="groupId" value="${group.id}"/>
                <div class="row">
                    <div class="col-lg-3">
                        <input type="file" class="form-control form-control-sm" id="groupPhoto" name="groupPhoto"
                               accept=".png, .jpg, .jpeg"/>
                    </div>
                    <div class="col-lg-3">
                        <input type="submit" class="btn btn-primary btn-sm" value="Change photo"/>
                    </div>
                </div>
            </form>
            <br>
            <form method="post" action="${pageContext.request.contextPath}/editGroup">
                <input type="hidden" value="${group.id}" name="id">
                <div class="row">
                    <div class="col-lg-3">
                        <label for="groupName" class="form-label">Group name: </label>
                        <input type="text" class="form-control form-control-sm" id="groupName" name="name"
                               value="${group.name}" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5">
                        <label for="description" class="form-label">Group description:</label>
                        <textarea class="form-control form-control-sm" id="description" name="description"
                                  rows="3">${group.description}</textarea>
                    </div>
                </div>
                <br>
                <button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal"
                        data-bs-target="#confirmationDialog">
                    Save changes
                </button>
                <div class="modal fade" id="confirmationDialog" data-bs-backdrop="static" data-bs-keyboard="false"
                     tabindex="-1"
                     aria-labelledby="staticBackdrop" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <p>Do you want to save changes?</p>
                            </div>
                            <div class="modal-footer">
                                <input type="submit" class="btn btn-primary btn-sm" value="Yes">
                                <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">No
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="${pageContext.request.contextPath}/group/${group.id}">Back</a>
            </form>
            <c:if test="${isCreator}">
                <form class="mt-3" action="${pageContext.request.contextPath}/deleteGroup" method="post">
                    <input type="hidden" name="groupId" value="${group.id}"/>
                    <button type="submit" class="btn btn-danger btn-sm">Delete group</button>
                </form>
            </c:if>
        </div>
    </div>
</div>
</body>
</html>