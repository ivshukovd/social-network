<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>

<html lang="en">
<head>
    <title>Search result</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="${pageContext.request.contextPath}/webjars/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="${pageContext.request.contextPath}/webjars/jquery/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/webjars/sockjs-client/sockjs.min.js"></script>
    <script src="${pageContext.request.contextPath}/webjars/stomp-websocket/stomp.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/webSocketMessaging.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/css/bootstrap.min.css"
          type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" type="text/css">
</head>
<body>
<div class="container">
    <jsp:include page="search.jsp"/>
    <br>
    <div class="offset-lg-2 col-lg-6">
        <div class="row mb-3 dialogue-partner text-center">
            <a href="${pageContext.request.contextPath}/account/${dialoguePartner.id}">
                <c:choose>
                    <c:when test="${dialoguePartner.photoId > 0}">
                        <img class="account-avatar"
                             src="${pageContext.request.contextPath}/image/${dialoguePartner.photoId}"
                             alt="Account photo"/>
                    </c:when>
                    <c:otherwise>
                        <img class="account-avatar"
                             src="${pageContext.request.contextPath}/resources/img/accountDefaultPhoto.jpg"
                             alt="Account photo"/>
                    </c:otherwise>
                </c:choose>
                <span class="account-name">${dialoguePartner.surName} ${dialoguePartner.firstName}</span>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2">
            <jsp:include page="menu.jsp"/>
        </div>
        <input type="hidden" id="contextPath" value="${pageContext.request.contextPath}">
        <div class="col-lg-6 dialogue">
            <div class="row mb-3">
                <form>
                    <input id="dialogueId" type="hidden" name="dialogueId" value="${dialogueId}"/>
                    <input id="accountFrom" type="hidden" name="accountFrom" value="${sessionScope.loggedInAccountId}"/>
                    <input id="accountTo" type="hidden" name="accountTo" value="${dialoguePartner.id}"/>
                    <label for="message"></label><textarea id="message" class="form-control form-control-sm" rows="2"
                                                           name="text"></textarea>
                    <div class="mt-1 text-end">
                        <button id="send" class="btn btn-primary btn-sm" type="button" onclick="sendMessage()">Send
                        </button>
                    </div>
                </form>
            </div>
            <div class="row mb-3" id="messages">
                <c:forEach var="message" items="${messages}">
                    <div class="row">
                        <c:choose>
                            <c:when test="${message.accountFrom eq sessionScope.loggedInAccountId}">
                                <div class="message my-message offset-lg-4 col-lg-8">
                                    <div class="row message-text mb-1">
                                        <span>${message.text}</span>
                                    </div>
                                    <div class="row message-date-time text-end">
                                        <p class="date-time">
                                            <javatime:format value="${message.date}" pattern="dd.MM.yyyy HH:mm"/>
                                        </p>
                                    </div>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="message col-lg-8">
                                    <div class="row message-text mb-1">
                                        <span>${message.text}</span>
                                    </div>
                                    <div class="row message-date-time text-end">
                                        <p class="date-time">
                                            <javatime:format value="${message.date}" pattern="dd.MM.yyyy HH:mm"/>
                                        </p>
                                    </div>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </c:forEach>
            </div>
        </div>
        <div class="w-100"></div>
        <br>
        <div class="col-lg-3 offset-lg-2">
            <nav aria-label="Messages pages">
                <ul id="pagination" class="pagination pagination-sm">
                    <c:if test="${page == numberPages && numberPages > 2}">
                        <li class="page-item">
                            <a class="page-link"
                               href="${pageContext.request.contextPath}/dialogue?dialoguePartnerId=${dialoguePartner.id}&page=${page - 2}">${page - 2}</a>
                        </li>
                    </c:if>
                    <c:forEach begin="${page - 1}" end="${page + 1}" var="i">
                        <c:if test="${i <= numberPages && i > 0}">
                            <c:choose>
                                <c:when test="${page eq i}">
                                    <li class="page-item active" aria-current="page">
                                        <span class="page-link">${i}</span>
                                    </li>
                                </c:when>
                                <c:otherwise>
                                    <li class="page-item">
                                        <a class="page-link"
                                           href="${pageContext.request.contextPath}/dialogue?dialoguePartnerId=${dialoguePartner.id}&page=${i}">${i}</a>
                                    </li>
                                </c:otherwise>
                            </c:choose>
                        </c:if>
                    </c:forEach>
                    <c:if test="${page == 1 && numberPages > 2}">
                        <li class="page-item">
                            <a class="page-link"
                               href="${pageContext.request.contextPath}/dialogue?dialoguePartnerId=${dialoguePartner.id}&page=${3}">${3}</a>
                        </li>
                    </c:if>
                </ul>
            </nav>
        </div>
    </div>
</div>
</body>
</html>