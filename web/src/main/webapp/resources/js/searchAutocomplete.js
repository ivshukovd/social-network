$(document).ready(function () {
    $("#search").autocomplete({
        source: function (request, response) {
            if (document.getElementById("searchedElement").value === "account") {
                $.get(document.getElementById("contextPath").value + "/showFoundAccounts?filter=" + request.term,
                    function (data) {
                        response($.map(data, function (account) {
                            return {
                                value: account.surName + " " + account.firstName,
                                label: account.surName + " " + account.firstName
                            }
                        }));
                    });
            } else {
                $.get(document.getElementById("contextPath").value + "/showFoundGroups?filter=" + request.term,
                    function (data) {
                        response($.map(data, function (group) {
                            return {value: group.name, label: group.name}
                        }));
                    });
            }
        },
        minLength: 2,
    });
});