$(document).ready(function () {
    $('#resultList').on('click', '.bi-trash', function (event) {
        contextPath = $('#contextPath').val()
        let deleteIconId = event.target.id
        let temp_array = deleteIconId.split('-')
        let postId = temp_array[temp_array.length - 1]
        console.log('post id: ' + postId)
        $.ajax({
            url: contextPath + '/deletePost',
            method: 'POST',
            data: {
                postId: postId,
                targetId: $('#targetId').val(),
                wallType: $('#wallType').val()
            },
            success: function (result) {
                console.log("result: " + result)
                if (result) {
                    $('.page-item.active .pageButton').trigger('click')
                }
            }
        });
    });
});