function uploadFile() {
    document.getElementById("fileXML").click()
}

$(document).ready(function () {
    $('#fileXML').on('change', function () {
        let contextPath = document.getElementById('contextPath').value
        let file = document.getElementById("fileXML").files[0]

        console.log('file name ' + file.name)
        console.log(file.text())
        console.log('contextPath ' + contextPath)

        let data = new FormData();
        data.append('fileXML', file)
        $.ajax({
            url: contextPath + "/importXml",
            data: data,
            headers: {
                Accept: "application/json; charset=utf-8",
            },
            cache: false,
            contentType: false,
            processData: false,
            method: 'POST',
            success: function (result) {
                console.log(result)
                document.getElementById("firstName").value = result.firstName
                document.getElementById("surName").value = result.surName
                document.getElementById("middleName").value = result.middleName
                document.getElementById("updatedEmail").value = result.email
                document.getElementById("birthDate").value = result.birthDate
                document.getElementById("updatedIcq").value = result.icq
                document.getElementById("updatedSkype").value = result.skype
                document.getElementById("homeAddress").value = result.homeAddress
                document.getElementById("workAddress").value = result.workAddress
                setPhones(result.phones)
            }
        });
    });
});

function setPhones(phones) {
    phoneCount = 0
    document.getElementById("phoneFields").innerText = ""
    phones.forEach(function (phone) {
        document.getElementById("newPhone").value = phone.number
        addPhone()
    })
}