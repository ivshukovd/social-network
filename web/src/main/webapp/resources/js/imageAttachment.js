$(document).ready(function () {
    $('#image').on('change', function () {
        let file = $('#image')[0].files[0]
        $('#attachImage').hide(0).after(
            $('<p/>', {id: 'fileName'}).text(file.name + ' ').css({'display': 'inline', 'font-size': '80%'}).append(
                $(' <i/>', {id: 'detachImage'}).addClass('bi bi-x-square')
            )
        )
    })
})

function attachFile() {
    $('#image').click()
}

$(document).on('click', '#detachImage', function () {
    $('#image')[0].value = ''
    $('#attachImage').show(0)
    $('#fileName').remove()
})