let contextPath

$(document).ready(function () {
    $('#pages').on('click', '.pageButton', function (event) {
        contextPath = $('#contextPath').val()
        let page = event.target.id
        let elementType = $('#elementType').val()
        console.log("Click on page button %s, element %s", page, elementType)
        console.log("context path", contextPath)
        $.ajax({
            url: getUrlForResultList(elementType),
            data: getDataForResultList(elementType, page),
            success: function (result) {
                console.log("is empty: " + $.isEmptyObject(result))
                if ($.isEmptyObject(result)) {
                    let previousPage = parseInt(page) - 1
                    $('span[id="' + previousPage + '"]').trigger('click')
                }
                clearList()
                drawResultList(elementType, result)
                redrawPages(elementType, page)
            }
        });
    });
});

function getUrlForResultList(elementType) {
    let url
    switch (elementType) {
        case 'accountWallPost':
        case 'groupWallPost':
            url = contextPath + '/getPosts'
            break
        case 'searchedAccount':
            url = contextPath + '/searchAccounts'
            break
        case 'searchedGroup':
            url = contextPath + '/searchGroups'
            break
    }
    return url
}

function getDataForResultList(elementType, page) {
    let data
    switch (elementType) {
        case 'accountWallPost':
        case 'groupWallPost':
            data = {
                targetId: $('#targetId').val(),
                page: page,
                wallType: $('#wallType').val()
            }
            break
        case 'searchedAccount':
        case 'searchedGroup':
            data = {
                page: page,
                search: $('#searchedString').val()
            }
            break
    }
    return data
}

function getUrlForPagesNumber(elementType) {
    let url
    switch (elementType) {
        case 'accountWallPost':
        case 'groupWallPost':
            url = contextPath + '/getNumberPostPages'
            break
        case 'searchedAccount':
            url = contextPath + '/numberSearchedAccountPages'
            break
        case 'searchedGroup':
            url = contextPath + '/numberSearchedGroupPages'
            break
    }
    return url
}

function getDataForPagesNumber(elementType) {
    let data
    switch (elementType) {
        case 'accountWallPost':
        case 'groupWallPost':
            data = {
                targetId: $('#targetId').val(),
                wallType: $('#wallType').val()
            }
            break
        case 'searchedAccount':
        case 'searchedGroup':
            data = {
                search: $('#searchedString').val()
            }
            break
    }
    return data
}

function clearList() {
    $('#resultList').html('')
}

function clearPagination() {
    $('#pages').html('')
}

function drawResultList(elementType, result) {
    switch (elementType) {
        case 'accountWallPost':
        case 'groupWallPost':
            drawPosts(result)
            break
        case 'searchedAccount':
            drawSearchResult(result, 'account')
            break
        case 'searchedGroup':
            drawSearchResult(result, 'group')
            break
    }
}

function drawPosts(posts) {
    let resultList = $('#resultList')
    posts.forEach(function (post) {
        console.log(post)
        let imgSrc
        if (post.authorPhotoId > 0) {
            imgSrc = contextPath + '/image/' + post.authorPhotoId
        } else {
            imgSrc = contextPath + '/resources/img/accountDefaultPhoto.jpg'
        }
        let image
        if (post.photoId === 0) {
            image = ''
        } else {
            image = $('<img>', {
                class: 'content-image',
                src: contextPath + '/image/' + post.photoId,
                alt: 'Attached image'
            })
        }
        resultList.append(
            $('<div/>', {class: 'wall-post'}).append(
                $('<div/>', {class: 'wall-post-body'}).append(
                    $('<div/>', {class: 'row mb-3'}).append(
                        $('<div/>', {class: 'col-lg-7'}).append(
                            $('<a/>', {href: contextPath + '/account/' + post.author}).append(
                                $('<img>', {
                                    class: 'account-avatar',
                                    src: imgSrc,
                                    alt: 'Account photo'
                                })
                            ).append(
                                $('<span/>', {class: 'account-name'}).text(post.authorSurName + ' ' + post.authorFirstName)
                            )
                        )
                    ).append(
                        $('<div/>', {class: 'offset-lg-4 col-lg-1'}).append(
                            $('<em/>', {class: 'bi bi-trash', id: 'deletePost-' + post.id})
                        )
                    )
                ).append(
                    $('<div/>', {class: 'row mb-3'}).append(image)
                ).append(
                    $('<div/>', {class: 'row post-text mb-1'}).append(
                        $('<span/>').text(post.message)
                    )
                ).append(
                    $('<div/>', {class: 'row post-date-time text-end'}).append(
                        $('<p/>').text(post.date)
                    )
                )
            )
        )
    })
}

function drawSearchResult(results, element) {
    let resultList = $('#resultList')
    results.forEach(function (result) {
        let imageSrc
        if (result.photo != null) {
            imageSrc = contextPath + '/image/' + result.photoId
        } else {
            imageSrc = contextPath + '/resources/img/' + element + 'DefaultPhoto.jpg'
        }
        let accountOrGroupName
        if (element === 'account') {
            accountOrGroupName = ' ' + result.surName + ' ' + result.firstName
        } else {
            accountOrGroupName = ' ' + result.name
        }

        resultList.append(
            $('<a/>', {
                class: 'nav-link active',
                href: contextPath + '/' + element + '/' + result.id
            }).append(
                $('<img>', {
                    src: imageSrc,
                    width: 35,
                    height: 35,
                    alt: 'Account photo'
                }), accountOrGroupName
            )
        )
    })
}

function redrawPages(elementType, page) {
    $.ajax({
        url: getUrlForPagesNumber(elementType),
        data: getDataForPagesNumber(elementType),
        success: function (numberPages) {
            clearPagination()
            drawPageNumbers(page, numberPages)
        }
    });
}

function drawPageNumbers(page, numberPages) {
    let pageNumber = parseInt(page)
    let pagesBar = $('#pages')
    if (numberPages > 0) {
        if (pageNumber === numberPages && numberPages > 2) {
            pagesBar.append(
                $('<li/>').addClass('page-item').append(
                    $('<span/>', {class: 'pageButton page-link', id: pageNumber - 2}).text(pageNumber - 2)
                )
            )
        }
        let previousPage
        if (pageNumber === 1) {
            previousPage = ''
        } else {
            previousPage = $('<li/>').addClass('page-item').append(
                $('<span/>', {class: 'pageButton page-link', id: pageNumber - 1}).text(pageNumber - 1)
            )
        }
        let currentPage = $('<li/>').addClass('page-item active').attr('aria-current', 'page').append(
            $('<span/>', {class: 'pageButton page-link', id: pageNumber}).text(pageNumber)
        )
        let nextPage
        if (pageNumber === numberPages) {
            nextPage = ''
        } else {
            nextPage = $('<li/>').addClass('page-item').append(
                $('<span/>', {class: 'pageButton page-link', id: pageNumber + 1}).text(pageNumber + 1)
            )
        }
        pagesBar.append(previousPage).append(currentPage).append(nextPage)
        if (pageNumber === 1 && numberPages > 2) {
            pagesBar.append(
                $('<li/>').addClass('page-item').append(
                    $('<span/>', {class: 'pageButton page-link', id: 3}).text(3)
                )
            )
        }
    }
}