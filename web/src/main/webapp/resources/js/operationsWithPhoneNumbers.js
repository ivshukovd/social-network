let phoneCount = 0;

function initCount() {
    phoneCount = document.getElementById("phoneFields").childElementCount;
}

function validatePhone(phone) {
    const phoneFormat = /[0-9]{11}/;
    if (!phoneFormat.test(phone)) {
        alert('incorrect phone number');
        return false;
    } else {
        return true;
    }
}

function addPhone() {
    let phoneValue = document.getElementById("newPhone").value;
    if (validatePhone(phoneValue)) {
        let newPhoneField = document.createElement('input');
        newPhoneField.type = 'tel';
        newPhoneField.className = 'form-control form-control-sm';
        newPhoneField.name = 'phones[' + phoneCount + ']';
        newPhoneField.pattern = '[0-9]{11}';
        newPhoneField.maxLength = 11;
        newPhoneField.value = phoneValue;

        let divForPhoneField = document.createElement('div');
        divForPhoneField.className = 'col-lg-3';
        divForPhoneField.appendChild(newPhoneField);

        let deleteButton = document.createElement('button');
        deleteButton.className = 'btn btn-danger btn-sm btn-block';
        deleteButton.type = 'button';
        deleteButton.onclick = function () {
            deletePhone(phoneValue);
        };
        deleteButton.innerText = 'delete';

        let divForDeleteButton = document.createElement('div');
        divForDeleteButton.className = 'col-lg-1';
        divForDeleteButton.appendChild(deleteButton);

        let newPhoneRow = document.createElement('div');
        newPhoneRow.className = 'row mb-3';
        newPhoneRow.id = phoneValue;
        newPhoneRow.appendChild(divForPhoneField);
        newPhoneRow.appendChild(divForDeleteButton);

        document.getElementById("phoneFields").appendChild(newPhoneRow);
        document.getElementById("newPhone").value = "";

        phoneCount++;
    }
}

function deletePhone(phone) {
    document.getElementById(phone).remove();
}