let stompClient = null
let contextPath
let dialogueId

window.onload = function () {
    console.log('Dialogue page onload')
    contextPath = document.getElementById('contextPath').value
    dialogueId = document.getElementById('dialogueId').value
    connect()
}

window.onbeforeunload = function () {
    stompClient.disconnect();
}

function connect() {
    let socket = new SockJS(contextPath + '/chat-messaging');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/messages/' + dialogueId, function (message) {
            showMessages(JSON.parse(message.body));
        });
    });
}

function sendMessage() {
    stompClient.send("/app/sendMessage/" + dialogueId, {'zoneId': Intl.DateTimeFormat().resolvedOptions().timeZone}, JSON.stringify({
        'text': $("#message").val(), 'accountFrom': $("#accountFrom").val(), 'accountTo': $("#accountTo").val()
    }));
    document.getElementById("message").value = ""
}

function showMessages(messages) {
    $('#messages').html('')
    messages.forEach(function (message) {
        let messageClasses
        if (message.accountFrom === parseInt($('#accountTo').val())) {
            messageClasses = 'message col-lg-8'
        } else {
            messageClasses = 'message my-message offset-lg-4 col-lg-8'
        }
        $('#messages').append(
            $('<div/>', {class: 'row'}).append(
                $('<div/>', {class: messageClasses}).append(
                    $('<div/>', {class: 'row message-text mb-1'}).append(
                        $('<span/>').text(message.text)
                    )
                ).append(
                    $('<div/>', {class: 'row message-date-time text-end'}).append(
                        $('<p/>').text(message.date)
                    )
                )
            )
        )
    })
    showPages()
}

function showPages() {
    $.ajax({
        url: contextPath + '/numberPagesInDialogue',
        data: {dialogueId: dialogueId},
        success: function (numberPages) {
            console.log("number pages: " + numberPages)
            let end = 3
            if (numberPages < 3) {
                end = numberPages
            }
            let paginationTag = $('#pagination')
            paginationTag.html('')
            for (let i = 1; i <= end; i++) {
                if (i === 1) {
                    paginationTag.append(
                        $('<li/>', {class: 'page-item active'}).append(
                            $('<a/>', {class: 'page-link'}).text(i)
                        )
                    )
                } else {
                    paginationTag.append(
                        $('<li/>', {class: 'page-item'}).append(
                            $('<a/>', {
                                class: 'page-link',
                                href: contextPath + '/dialogue?dialoguePartnerId=' + $('#accountTo').val() + '&page=' + i
                            }).text(i)
                        )
                    )
                }
            }
        }
    });
}