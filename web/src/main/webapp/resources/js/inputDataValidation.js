$(document).ready(function () {
    $(document).on('change', '.unique', function (event) {
        let contextPath = $('#contextPath').val()
        let element = event.target.id
        let insertedValue = $('#' + element).val()

        console.log('context path: ' + contextPath)
        console.log('event id: ' + element)
        console.log('inserted value: ' + insertedValue)
        console.log('starts with updated: ' + element.startsWith('updated'))

        let data
        if (element.startsWith('updated') || element === 'newPhone') {
            data = {
                parameter: insertedValue,
                accountId: $('#accountId').val()
            }
        } else {
            data = {
                parameter: insertedValue
            }
        }

        $.ajax({
            url: contextPath + '/' + element + 'Check',
            data: data,
            success: function (exists) {
                console.log('element exists: ' + exists)
                if (exists) {
                    $('#' + element + 'Exists').show(0)
                    $('input[type=submit]').attr('disabled', true)
                    if (element === 'newPhone') {
                        $('#addPhoneButton').attr('disabled', true)
                    }
                } else {
                    $('#' + element + 'Exists').hide(0)
                    $('input[type=submit]').attr('disabled', false)
                    if (element === 'newPhone') {
                        $('#addPhoneButton').attr('disabled', false)
                    }
                }
            }
        });
    })
})