package com.getjavajob.training.ivshukovd.socialnetwork.web.controllers;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.AccountShortDto;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.GroupDto;
import com.getjavajob.training.ivshukovd.socialnetwork.service.AccountService;
import com.getjavajob.training.ivshukovd.socialnetwork.service.GroupService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static org.slf4j.LoggerFactory.getLogger;

@Controller
public class SearchController {

    private static final Logger logger = getLogger(SearchController.class);
    private static final int RECORDS_PER_PAGE = 5;
    private final AccountService accountService;
    private final GroupService groupService;

    @Autowired
    public SearchController(AccountService accountService, GroupService groupService) {
        this.accountService = accountService;
        this.groupService = groupService;
    }

    @GetMapping("/search")
    public String showSearchResult(@RequestParam(defaultValue = "") String search, @RequestParam String searchedElement,
                                   Model model) {
        logger.info("Search {} by filter {}", searchedElement, search);
        model.addAttribute("page", 1);
        model.addAttribute("search", search);
        int numberPages = 1;
        if ("account".equals(searchedElement)) {
            String[] userName = search.split(" ");
            List<AccountShortDto> accounts;
            if (userName.length == 1) {
                int pagesWithAccounts = accountService.getNumberAccountPages(search, RECORDS_PER_PAGE);
                if (numberPages < pagesWithAccounts) {
                    numberPages = pagesWithAccounts;
                }
                accounts = accountService.getAccountsBySubstr(search, 1, RECORDS_PER_PAGE);
            } else {
                int pagesWithAccounts =
                        accountService.getNumberAccountPages(userName[0], userName[1], RECORDS_PER_PAGE);
                if (numberPages < pagesWithAccounts) {
                    numberPages = pagesWithAccounts;
                }
                accounts = accountService.getAccountsByFullName(userName[0], userName[1], 1, RECORDS_PER_PAGE);
            }
            model.addAttribute("accounts", accounts);
            model.addAttribute("numberPages", numberPages);
            logger.info("Found accounts, page: {}", 1);
            return "searchAccountResult";
        } else {
            int pagesWithGroups = groupService.getNumberGroupPages(search, RECORDS_PER_PAGE);
            if (numberPages < pagesWithGroups) {
                numberPages = pagesWithGroups;
            }
            List<GroupDto> groups = groupService.getGroupsBySubstring(search, 1, RECORDS_PER_PAGE);
            model.addAttribute("groups", groups);
            model.addAttribute("numberPages", numberPages);
            logger.info("Found groups, page: {}", 1);
            return "searchGroupResult";
        }
    }

    @GetMapping(value = "/showFoundAccounts")
    @ResponseBody
    public List<AccountShortDto> getIntermediateSearchResultForAccounts(@RequestParam String filter) {
        logger.info("Search accounts by substring '{}'", filter);
        return accountService.getAccountsBySubstr(filter, 1, RECORDS_PER_PAGE);
    }

    @GetMapping("/searchAccounts")
    @ResponseBody
    public List<AccountShortDto> searchAccounts(@RequestParam String search, @RequestParam int page) {
        logger.info("Searching page {} with account list contains '{}'", page, search);
        String[] userName = search.split(" ");
        List<AccountShortDto> accounts;
        if (userName.length == 1) {
            accounts = accountService.getAccountsBySubstr(search, page, RECORDS_PER_PAGE);
        } else {
            accounts = accountService.getAccountsByFullName(userName[0], userName[1], page, RECORDS_PER_PAGE);
        }
        return accounts;
    }

    @GetMapping("/numberSearchedAccountPages")
    @ResponseBody
    public int getNumberSearchedAccountPages(@RequestParam String search) {
        logger.info("Searching number pages of accounts by substring {}", search);
        String[] userName = search.split(" ");
        if (userName.length == 1) {
            return accountService.getNumberAccountPages(search, RECORDS_PER_PAGE);
        } else {
            return accountService.getNumberAccountPages(userName[0], userName[1], RECORDS_PER_PAGE);
        }
    }

    @GetMapping(value = "/showFoundGroups")
    @ResponseBody
    public List<GroupDto> getIntermediateSearchResultForGroups(@RequestParam String filter) {
        logger.info("Search groups by substring '{}'", filter);
        return groupService.getGroupsBySubstring(filter, 1, RECORDS_PER_PAGE);
    }

    @GetMapping("/searchGroups")
    @ResponseBody
    public List<GroupDto> searchGroups(@RequestParam String search, @RequestParam int page) {
        logger.info("Displaying page {} with group list contains '{}'", page, search);
        return groupService.getGroupsBySubstring(search, page, RECORDS_PER_PAGE);
    }

    @GetMapping("/numberSearchedGroupPages")
    @ResponseBody
    public int getNumberSearchedGroupPages(@RequestParam String search) {
        logger.info("Searching number pages of groups by substring {}", search);
        return groupService.getNumberGroupPages(search, RECORDS_PER_PAGE);
    }

}