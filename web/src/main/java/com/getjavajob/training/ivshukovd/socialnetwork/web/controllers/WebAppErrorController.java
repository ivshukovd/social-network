package com.getjavajob.training.ivshukovd.socialnetwork.web.controllers;

import org.slf4j.Logger;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import static org.slf4j.LoggerFactory.getLogger;

@Controller
public class WebAppErrorController implements ErrorController {

    private final Logger logger = getLogger(WebAppErrorController.class);

    @RequestMapping("/error")
    public String handleError(HttpServletRequest request) {
        logger.error("Error status code {}", request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE));
        return "error";
    }

}