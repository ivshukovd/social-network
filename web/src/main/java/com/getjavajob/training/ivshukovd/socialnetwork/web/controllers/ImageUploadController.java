package com.getjavajob.training.ivshukovd.socialnetwork.web.controllers;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Image;
import com.getjavajob.training.ivshukovd.socialnetwork.service.AccountService;
import com.getjavajob.training.ivshukovd.socialnetwork.service.GroupService;
import com.getjavajob.training.ivshukovd.socialnetwork.service.ImageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

@Controller
public class ImageUploadController {

    private static final Logger logger = LoggerFactory.getLogger(ImageUploadController.class);
    private final AccountService accountService;
    private final GroupService groupService;
    private final ImageService imageService;

    @Autowired
    public ImageUploadController(AccountService accountService, GroupService groupService, ImageService imageService) {
        this.accountService = accountService;
        this.groupService = groupService;
        this.imageService = imageService;
    }

    public static byte[] convertStreamToByteArray(InputStream photo) throws IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int nRead;
        byte[] data = new byte[1024];
        while ((nRead = photo.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
        }
        buffer.flush();
        return buffer.toByteArray();
    }

    @PostMapping("/uploadAccountPhoto")
    public String uploadAccountPhoto(@RequestParam MultipartFile accPhoto, @RequestParam long accountId,
                                     @SessionAttribute long loggedInAccountId) throws IOException {
        logger.info("Account {} tries to update account {} avatar", loggedInAccountId, accountId);
        if (loggedInAccountId == accountId) {
            byte[] imageAsArray = convertStreamToByteArray(accPhoto.getInputStream());
            if (imageAsArray.length > 0) {
                logger.info("Image is attached");
                Image img = imageService.saveImage(imageAsArray);
                accountService.updateAccountAvatar(img.getId(), accountId);
                logger.info("Account {} avatar updated", accountId);
            } else {
                logger.info("Image is not attached");

            }
        } else {
            logger.warn("Account {} avatar can't be updated", accountId);
        }
        return "redirect:/account/" + accountId;
    }

    @PostMapping("/uploadGroupPhoto")
    public String uploadGroupPhoto(@RequestParam MultipartFile groupPhoto, @RequestParam long groupId,
                                   @SessionAttribute long loggedInAccountId) throws IOException {
        logger.info("Account {} tries to update group {} avatar", loggedInAccountId, groupId);
        if (groupService.isAdmin(groupId, loggedInAccountId) || groupService.isCreator(groupId, loggedInAccountId)) {
            byte[] imageAsArray = convertStreamToByteArray(groupPhoto.getInputStream());
            if (imageAsArray.length > 0) {
                logger.info("Image is attached");
                Image img = imageService.saveImage(imageAsArray);
                groupService.updateGroupAvatar(img.getId(), groupId);
                logger.info("Group {} avatar updated", groupId);
            } else {
                logger.info("Image is not attached");

            }
        } else {
            logger.warn("Group {} avatar can't be updated", groupId);
        }
        return "redirect:/group/" + groupId;
    }

}