package com.getjavajob.training.ivshukovd.socialnetwork.web.controllers;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.service.AccountService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

import java.util.List;

import static org.slf4j.LoggerFactory.getLogger;

@Controller
public class FriendshipController {

    private static final Logger logger = getLogger(FriendshipController.class);
    private static final int RECORDS_PER_PAGE = 5;
    private static final String REDIRECT = "redirect:";
    private final AccountService accountService;

    @Autowired
    public FriendshipController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/friends")
    public String showFriedList(@RequestParam(defaultValue = "1") int page, @SessionAttribute long loggedInAccountId,
                                Model model) {
        logger.info("Account {} tries to see friend list", loggedInAccountId);
        int numberPages = accountService.getNumberOfPagesInFriendList(loggedInAccountId, RECORDS_PER_PAGE);
        List<Account> accounts = accountService.getFriends(loggedInAccountId, page, RECORDS_PER_PAGE);
        model.addAttribute("accounts", accounts);
        model.addAttribute("page", page);
        model.addAttribute("numberPages", numberPages);
        logger.info("Account {} friend list, page: {}", loggedInAccountId, page);
        return "friendList";
    }

    @GetMapping("/friendRequests")
    public String showFriendRequestList(@RequestParam(defaultValue = "1") int page,
                                        @SessionAttribute long loggedInAccountId, Model model) {
        logger.info("Account {} tries to see friend request list", loggedInAccountId);
        int numberPages = accountService.getNumberOfPagesInFollowerList(loggedInAccountId, RECORDS_PER_PAGE);
        List<Account> accounts = accountService.getFollowers(loggedInAccountId, page, RECORDS_PER_PAGE);
        model.addAttribute("accounts", accounts);
        model.addAttribute("page", page);
        model.addAttribute("numberPages", numberPages);
        logger.info("Account {} friend request list, page: {}", loggedInAccountId, page);
        return "friendRequests";
    }

    @PostMapping("/addFriend")
    public String addFriend(@SessionAttribute long loggedInAccountId, @RequestParam long accountIdToBeFollow,
                            @RequestParam String currentPageURL) {
        logger.info("Account {} tries to sent friend request account {}", loggedInAccountId, accountIdToBeFollow);
        accountService.addRelationship(loggedInAccountId, accountIdToBeFollow);
        logger.info("Account {} sent friend request account {}", loggedInAccountId, accountIdToBeFollow);
        return REDIRECT + currentPageURL;
    }

    @PostMapping("/deleteFriend")
    public String deleteFriend(@SessionAttribute long loggedInAccountId, @RequestParam long friendId,
                               @RequestParam String currentPageURL) {
        logger.info("Account {} deletes account {} from friend list", loggedInAccountId, friendId);
        accountService.deleteFriend(loggedInAccountId, friendId);
        logger.info("Account {} has deleted account {} from friends", loggedInAccountId, friendId);
        return REDIRECT + currentPageURL;
    }

    @PostMapping("/deleteFriendRequest")
    public String deleteFriendRequest(@SessionAttribute long loggedInAccountId, @RequestParam long followerId,
                                      @RequestParam String currentPageURL) {
        logger.info("Account {} wants to cancel friend request from account {}", loggedInAccountId, followerId);
        accountService.deleteFollower(followerId, loggedInAccountId);
        logger.info("Account {} canceled friend request from account {}", loggedInAccountId, followerId);
        return REDIRECT + currentPageURL;
    }

}