package com.getjavajob.training.ivshukovd.socialnetwork.web.config;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import static java.lang.String.format;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {

    private final AccountService accountService;

    @Autowired
    public UserDetailsServiceImpl(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<Account> accountOptional = accountService.getAccountByEmail(email);
        if (!accountOptional.isPresent()) {
            throw new UsernameNotFoundException(format("User with email %s not found", email));
        } else {
            Account account = accountOptional.get();
            return new User(account.getEmail(), account.getPassword(), mapRoleToAuthority(account.isAdmin()));
        }
    }

    private Collection<? extends GrantedAuthority> mapRoleToAuthority(boolean isAdmin) {
        Collection<GrantedAuthority> roles = new ArrayList<>();
        if (isAdmin) {
            roles.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        } else {
            roles.add(new SimpleGrantedAuthority("ROLE_MEMBER"));
        }
        return roles;
    }

}