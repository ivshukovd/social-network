package com.getjavajob.training.ivshukovd.socialnetwork.web.controllers;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Image;
import com.getjavajob.training.ivshukovd.socialnetwork.service.ImageService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static org.slf4j.LoggerFactory.getLogger;

@Controller
public class ImageOutputController extends HttpServlet {

    private static final Logger logger = getLogger(ImageOutputController.class);
    private final ImageService imageService;

    @Autowired
    public ImageOutputController(ImageService imageService) {
        this.imageService = imageService;
    }

    @GetMapping("/image/{id}")
    public void displayImage(@PathVariable long id, HttpServletResponse resp) {
        Optional<Image> img = imageService.getImage(id);
        img.ifPresent(image -> {
            resp.setContentType("image/jpeg");
            resp.setContentLength(image.getContent().length);
            try {
                resp.getOutputStream().write(image.getContent());
                logger.info("Displaying image {}", id);
            } catch (IOException e) {
                logger.error("Image could not be displayed");
            }
        });
    }

}