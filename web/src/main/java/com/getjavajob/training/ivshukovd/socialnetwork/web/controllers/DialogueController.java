package com.getjavajob.training.ivshukovd.socialnetwork.web.controllers;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Dialogue;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Message;
import com.getjavajob.training.ivshukovd.socialnetwork.service.AccountService;
import com.getjavajob.training.ivshukovd.socialnetwork.service.DialogueService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;

import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;

import static java.time.LocalDateTime.now;
import static java.time.ZoneOffset.UTC;
import static org.slf4j.LoggerFactory.getLogger;

@Controller
public class DialogueController {

    private static final Logger logger = getLogger(DialogueController.class);
    private static final int RECORDS_PER_PAGE = 5;
    private final DialogueService dialogueService;
    private final AccountService accountService;

    @Autowired
    public DialogueController(DialogueService dialogueService, AccountService accountService) {
        this.dialogueService = dialogueService;
        this.accountService = accountService;
    }

    @GetMapping("/dialogues")
    public String showDialogueList(@RequestParam(defaultValue = "1") int page, @SessionAttribute long loggedInAccountId,
                                   Model model) {
        logger.info("Account {} tries to see dialogue list", loggedInAccountId);
        int numberPages = dialogueService.getNumberDialoguePages(loggedInAccountId, RECORDS_PER_PAGE);
        List<Dialogue> dialogues = dialogueService.getDialogues(loggedInAccountId, page, RECORDS_PER_PAGE);
        model.addAttribute("dialogues", dialogues);
        model.addAttribute("page", page);
        model.addAttribute("numberPages", numberPages);
        logger.info("Account {} dialog list, page: {}", loggedInAccountId, page);
        return "dialogueList";
    }

    @GetMapping("/dialogue")
    public String showDialogue(@RequestParam(defaultValue = "1") int page, @RequestParam long dialoguePartnerId,
                               @SessionAttribute long loggedInAccountId, @SessionAttribute TimeZone timeZone,
                               Model model) {
        logger.info("Account {} tries to start dialogue with account {}, page {}", loggedInAccountId, dialoguePartnerId,
                    page);
        Optional<Dialogue> dialogueOptional =
                dialogueService.getDialogueByAccountsId(loggedInAccountId, dialoguePartnerId);
        if (accountService.isFriend(loggedInAccountId, dialoguePartnerId) || accountService.isAdmin(loggedInAccountId)
                || dialogueOptional.isPresent() && accountService.isAdmin(dialoguePartnerId)) {
            Dialogue dialogue = dialogueOptional.orElseGet(
                    () -> dialogueService.createDialogue(loggedInAccountId, dialoguePartnerId));
            if (dialogue.getAccount1().getId() == loggedInAccountId) {
                model.addAttribute("dialoguePartner", dialogue.getAccount2());
            } else {
                model.addAttribute("dialoguePartner", dialogue.getAccount1());
            }
            int numberPages = dialogueService.getNumberMessagePages(dialogue.getId(), RECORDS_PER_PAGE);
            List<Message> messages = dialogueService.getMessages(dialogue.getId(), page, RECORDS_PER_PAGE);
            for (Message message : messages) {
                message.setDate(message.getDate()
                                       .plusSeconds(message.getDate()
                                                           .atZone(timeZone.toZoneId())
                                                           .getOffset()
                                                           .getTotalSeconds()));
            }
            model.addAttribute("messages", messages);
            model.addAttribute("dialogueId", dialogue.getId());
            model.addAttribute("numberPages", numberPages);
            model.addAttribute("page", page);
            logger.info("Dialogue {}, page: {}", dialogue.getId(), page);
            return "dialogue";
        }
        return "redirect:/account/" + loggedInAccountId;
    }

    @MessageMapping("/sendMessage/{dialogueId}")
    @SendTo("/topic/messages/{dialogueId}")
    public List<Message> sendMessage(Message newMessage, @Header String zoneId, @DestinationVariable long dialogueId) {
        logger.info("Received message: {}", newMessage.getText());
        newMessage.setDate(now(UTC));
        dialogueService.writeMessage(newMessage);
        List<Message> messages = dialogueService.getMessages(dialogueId, 1, RECORDS_PER_PAGE);
        for (Message message : messages) {
            message.setDate(message.getDate()
                                   .plusSeconds(
                                           message.getDate().atZone(ZoneId.of(zoneId)).getOffset().getTotalSeconds()));
        }
        logger.info("Message from account {} to account {} was sent successfully", newMessage.getAccountFrom(),
                    newMessage.getAccountTo());
        return messages;
    }

    @GetMapping("/numberPagesInDialogue")
    @ResponseBody
    public int getNumberPagesInDialogue(@RequestParam long dialogueId) {
        return dialogueService.getNumberMessagePages(dialogueId, RECORDS_PER_PAGE);
    }

}