package com.getjavajob.training.ivshukovd.socialnetwork.web.controllers;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Image;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Post;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.PostDto;
import com.getjavajob.training.ivshukovd.socialnetwork.service.AccountService;
import com.getjavajob.training.ivshukovd.socialnetwork.service.GroupService;
import com.getjavajob.training.ivshukovd.socialnetwork.service.ImageService;
import com.getjavajob.training.ivshukovd.socialnetwork.service.PostService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import static com.getjavajob.training.ivshukovd.socialnetwork.domain.Post.Type;
import static com.getjavajob.training.ivshukovd.socialnetwork.domain.Post.Type.ACCOUNT;
import static com.getjavajob.training.ivshukovd.socialnetwork.domain.Post.Type.GROUP;
import static com.getjavajob.training.ivshukovd.socialnetwork.domain.util.EntityDtoConverter.convertPostEntityToDto;
import static com.getjavajob.training.ivshukovd.socialnetwork.web.controllers.ImageUploadController.convertStreamToByteArray;
import static java.time.LocalDateTime.now;
import static java.time.ZoneOffset.UTC;
import static org.slf4j.LoggerFactory.getLogger;

@Controller
public class WallPostController {

    private static final Logger logger = getLogger(WallPostController.class);
    private static final int RECORDS_PER_PAGE = 5;
    private final AccountService accountService;
    private final GroupService groupService;
    private final ImageService imageService;
    private final PostService postService;

    @Autowired
    public WallPostController(AccountService accountService, GroupService groupService, ImageService imageService,
                              PostService postService) {
        this.accountService = accountService;
        this.groupService = groupService;
        this.imageService = imageService;
        this.postService = postService;
    }

    @PostMapping("/accountWallPost")
    public String writePostOnAccountWall(@RequestParam(required = false) MultipartFile image,
                                         @ModelAttribute PostDto post) throws IOException {
        logger.info("Account {} tries to write post on account {} wall", post.getAuthor(), post.getTargetId());
        if (accountService.isFriend(post.getAuthor(), post.getTargetId()) || accountService.isAdmin(post.getAuthor())
                || post.getAuthor() == post.getTargetId()) {
            byte[] imageAsArray = convertStreamToByteArray(image.getInputStream());
            if (imageAsArray.length > 0) {
                logger.info("Image is attached");
                Image img = imageService.saveImage(imageAsArray);
                post.setPhotoId(img.getId());
            } else {
                post.setPhotoId(0);
            }
            if (imageAsArray.length > 0 || !post.getMessage().isEmpty()) {
                post.setDate(now(UTC));
                post.setType(ACCOUNT);
                postService.writePostToAccountWall(post);
                logger.info("Post has been written");
            }
        } else {
            logger.warn("Account {} can't write post on account {} wall", post.getAuthor(), post.getTargetId());
        }
        return "redirect:/account/" + post.getTargetId();
    }

    @PostMapping("/groupWallPost")
    public String writePostOnGroupWall(@RequestParam(required = false) MultipartFile image,
                                       @ModelAttribute PostDto post) throws IOException {
        logger.info("Account {} tries to write post on group {} wall", post.getAuthor(), post.getTargetId());
        if (groupService.isGroupMember(post.getTargetId(), post.getAuthor()) || accountService.isAdmin(
                post.getAuthor())) {
            byte[] imageAsArray = convertStreamToByteArray(image.getInputStream());
            if (imageAsArray.length > 0) {
                logger.info("Image is attached");
                Image img = imageService.saveImage(imageAsArray);
                post.setPhotoId(img.getId());
            } else {
                logger.info("Image is not attached");
                post.setPhotoId(0);
            }
            if (imageAsArray.length > 0 || !post.getMessage().isEmpty()) {
                post.setDate(now(UTC));
                post.setType(GROUP);
                postService.writePostToGroupWall(post);
                logger.info("Post has been written");
            }
        } else {
            logger.warn("Account {} can't write post on group {} wall", post.getAuthor(), post.getTargetId());
        }
        return "redirect:/group/" + post.getTargetId();
    }

    @GetMapping("/getPosts")
    @ResponseBody
    public List<PostDto> getPosts(@RequestParam long targetId, @RequestParam int page, @RequestParam Type wallType,
                                  @SessionAttribute long loggedInAccountId, @SessionAttribute TimeZone timeZone) {
        logger.info("Account {} tries to see posts on {} {} wall, page {}", loggedInAccountId, wallType, targetId,
                    page);
        List<PostDto> posts = new ArrayList<>();
        if (wallType == ACCOUNT) {
            if (accountService.isFriend(loggedInAccountId, targetId) || loggedInAccountId == targetId) {
                logger.info("Account {} posts on page {}", targetId, page);
                for (Post post : postService.getAccountWallPosts(targetId, page, RECORDS_PER_PAGE)) {
                    posts.add(convertPostEntityToDto(post, timeZone));
                }
            }
        } else {
            if (groupService.isGroupMember(targetId, loggedInAccountId)) {
                logger.info("Group {} posts on page {}", targetId, page);
                for (Post post : postService.getGroupWallPosts(targetId, page, RECORDS_PER_PAGE)) {
                    posts.add(convertPostEntityToDto(post, timeZone));
                }
            }
        }
        return posts;
    }

    @GetMapping("/getNumberPostPages")
    @ResponseBody
    public int getNumberPostPages(@RequestParam long targetId, @RequestParam Type wallType,
                                  @SessionAttribute long loggedInAccountId) {
        if (wallType == ACCOUNT) {
            if (accountService.isFriend(loggedInAccountId, targetId) || loggedInAccountId == targetId) {
                logger.info("Number account {} post pages", targetId);
                return postService.getNumberAccountPostPages(targetId, RECORDS_PER_PAGE);
            } else {
                return 0;
            }
        } else {
            if (groupService.isGroupMember(targetId, loggedInAccountId)) {
                logger.info("Number group {} post pages", targetId);
                return postService.getNumberGroupPostPages(targetId, RECORDS_PER_PAGE);
            } else {
                return 0;
            }
        }
    }

    @PostMapping("/deletePost")
    @ResponseBody
    public boolean deletePost(@RequestParam long postId, @RequestParam long targetId, @RequestParam Type wallType,
                              @SessionAttribute long loggedInAccountId) {
        logger.info("Account {} tries to delete post {}", loggedInAccountId, postId);
        if (checkPermissionForDeletePost(loggedInAccountId, targetId, wallType, postId)) {
            postService.deletePost(postId);
            logger.info("Post {} deleted successfully", postId);
            return true;
        } else {
            logger.warn("Account {} can't delete post {}", loggedInAccountId, postId);
        }
        return false;
    }

    private boolean checkPermissionForDeletePost(long accountId, long targetId, Type wallType, long postId) {
        boolean accountIsAdmin = accountService.isAdmin(accountId);
        boolean ownWall = wallType.equals(ACCOUNT) && accountId == targetId;
        boolean accountIsGroupAdminOrCreator =
                wallType.equals(GROUP) && (groupService.isAdmin(targetId, accountId) || groupService.isCreator(targetId,
                                                                                                               accountId));
        boolean accountIsAuthor = wallType.equals(GROUP) && postService.isAuthor(postId, accountId);
        return accountIsAdmin || ownWall || accountIsGroupAdminOrCreator || accountIsAuthor;
    }

}