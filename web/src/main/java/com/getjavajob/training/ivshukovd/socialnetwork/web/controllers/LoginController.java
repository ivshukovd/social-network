package com.getjavajob.training.ivshukovd.socialnetwork.web.controllers;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.service.AccountService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.security.Principal;
import java.util.Optional;

import static java.util.TimeZone.getTimeZone;
import static org.slf4j.LoggerFactory.getLogger;

@Controller
@SessionAttributes({"loggedInAccountId", "timeZone"})
public class LoginController {

    private static final Logger logger = getLogger(LoginController.class);
    private final AccountService accountService;

    @Autowired
    public LoginController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/login")
    public String showLoginPage(@RequestParam(defaultValue = "false") boolean error, Model model) {
        logger.info("Login page");
        model.addAttribute("error", error);
        return "login";
    }

    @PostMapping("/afterSuccessLogin")
    public String afterSuccessLogin(@RequestParam String email) {
        logger.info("Account with email {} logged in successfully", email);
        return "redirect:/setLoggedInAccountId";
    }

    @GetMapping("/setLoggedInAccountId")
    public String setGlobalId(Principal principal, Model model) {
        logger.info("Set Account with email {} as global account", principal.getName());
        Optional<Account> accountOptional = accountService.getAccountByEmail(principal.getName());
        if (accountOptional.isPresent()) {
            long loggedInAccountId = accountOptional.get().getId();
            model.addAttribute("loggedInAccountId", loggedInAccountId);
            return "clientTimeZone";
        }
        return "redirect:/login";
    }

    @PostMapping("/setClientTimeZone")
    public String setClientTimeZone(@RequestParam(defaultValue = "UTC") String timeZoneId, Model model) {
        logger.info("Set session time zone: {}", timeZoneId);
        model.addAttribute("timeZone", getTimeZone(timeZoneId));
        return "redirect:/account/0";
    }

}