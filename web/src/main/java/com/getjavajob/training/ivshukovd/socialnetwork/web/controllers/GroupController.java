package com.getjavajob.training.ivshukovd.socialnetwork.web.controllers;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Group;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Post;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.PostDto;
import com.getjavajob.training.ivshukovd.socialnetwork.service.AccountService;
import com.getjavajob.training.ivshukovd.socialnetwork.service.GroupService;
import com.getjavajob.training.ivshukovd.socialnetwork.service.PostService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TimeZone;

import static com.getjavajob.training.ivshukovd.socialnetwork.domain.util.EntityDtoConverter.convertPostEntityToDto;
import static java.time.LocalDate.now;
import static org.slf4j.LoggerFactory.getLogger;

@Controller
public class GroupController {

    private static final Logger logger = getLogger(GroupController.class);
    private static final String REDIRECT_TO_GROUP = "redirect:/group/";
    private static final String REDIRECT_TO_JOIN_REQUEST_LIST = "redirect:/groupJoinRequests?groupId=";
    private static final String REDIRECT_TO_GROUP_MEMBER_LIST = "redirect:/showGroupMembers?groupId=";
    private static final String AND_PAGE_PARAMETER = "&page=";
    private static final int RECORDS_PER_PAGE = 5;

    private final GroupService groupService;
    private final AccountService accountService;
    private final PostService postService;

    @Autowired
    public GroupController(GroupService groupService, AccountService accountService, PostService postService) {
        this.groupService = groupService;
        this.accountService = accountService;
        this.postService = postService;
    }

    @GetMapping("/createGroup")
    public String showGroupCreationPage() {
        logger.info("Create group page");
        return "createGroup";
    }

    @PostMapping("/createGroup")
    public String createGroup(@ModelAttribute Group group, @SessionAttribute long loggedInAccountId) {
        logger.info("Account {} tries to create group", loggedInAccountId);
        group.setCreationDate(now());
        accountService.getAccount(loggedInAccountId).ifPresent(account -> groupService.createGroup(group, account));
        logger.info("Group {} was created successfully", group.getId());
        return REDIRECT_TO_GROUP + group.getId();
    }

    @GetMapping("/group/{id}")
    public String showGroup(@PathVariable long id, @SessionAttribute long loggedInAccountId,
                            @SessionAttribute TimeZone timeZone, Model model) {
        logger.info("Account {} tries to see group {}", loggedInAccountId, id);
        groupService.getGroup(id).ifPresent(group -> model.addAttribute("group", group));
        addAttributesIsGroupAdminOrCreator(id, loggedInAccountId, model);
        List<PostDto> posts = new ArrayList<>();
        for (Post post : postService.getGroupWallPosts(id, 1, RECORDS_PER_PAGE)) {
            posts.add(convertPostEntityToDto(post, timeZone));
        }
        model.addAttribute("groupMember", groupService.isGroupMember(id, loggedInAccountId));
        model.addAttribute("admin", accountService.isAdmin(loggedInAccountId));
        model.addAttribute("joinRequestExists", groupService.isJoinRequest(id, loggedInAccountId));
        model.addAttribute("numberMembers", groupService.getNumberGroupMembers(id));
        model.addAttribute("posts", posts);
        model.addAttribute("numberPages", postService.getNumberGroupPostPages(id, RECORDS_PER_PAGE));
        logger.info("Displaying group {}", id);
        return "groupView";
    }

    @GetMapping("/editGroup/{groupId}")
    public String showEditGroupPage(@PathVariable long groupId, @SessionAttribute long loggedInAccountId, Model model) {
        logger.info("Account {} tries to see edit group {} page", loggedInAccountId, groupId);
        if (groupService.isAdmin(groupId, loggedInAccountId) || groupService.isCreator(groupId, loggedInAccountId)) {
            groupService.getGroup(groupId).ifPresent(group -> model.addAttribute("group", group));
            model.addAttribute("isCreator", groupService.isCreator(groupId, loggedInAccountId));
            logger.info("Edit group {} page", groupId);
            return "groupEdit";
        }
        logger.warn("Account {} does not have permission to edit group {}", loggedInAccountId, groupId);
        return REDIRECT_TO_GROUP + groupId;
    }

    @PostMapping("/editGroup")
    public String editGroup(@SessionAttribute long loggedInAccountId, @ModelAttribute Group group) {
        logger.info("Account {} tries to edit group {}", loggedInAccountId, group.getId());
        if (groupService.isAdmin(group.getId(), loggedInAccountId) || groupService.isCreator(group.getId(),
                                                                                             loggedInAccountId)) {
            groupService.updateGroup(group);
            logger.info("Group {} was updated successfully", group.getId());
        } else {
            logger.warn("Account {} does not have permission to edit group {}", loggedInAccountId, group.getId());
        }
        return REDIRECT_TO_GROUP + group.getId();
    }

    @GetMapping("/groups")
    public String showAccountGroupsList(@RequestParam(defaultValue = "1") int page,
                                        @SessionAttribute long loggedInAccountId, Model model) {
        logger.info("Account {} tries to see group list", loggedInAccountId);
        int numberPages = groupService.getNumberGroupPagesByMemberId(loggedInAccountId, RECORDS_PER_PAGE);
        List<Group> groups = groupService.getGroupsByMemberId(loggedInAccountId, page, RECORDS_PER_PAGE);
        model.addAttribute("groups", groups);
        addAttributesPageAndNumberPages(page, numberPages, model);
        logger.info("Account {} group list, page: {}", loggedInAccountId, page);
        return "groupList";
    }

    @GetMapping("/showGroupMembers")
    public String showGroupMembers(@RequestParam(defaultValue = "1") int page, @RequestParam long groupId,
                                   @SessionAttribute long loggedInAccountId, Model model) {
        logger.info("Account {} tries to see group {} member list", loggedInAccountId, groupId);
        if (groupService.isGroupMember(groupId, loggedInAccountId)) {
            int numberPages = groupService.getNumberGroupMemberPages(groupId, RECORDS_PER_PAGE);
            List<Account> members = groupService.getGroupMembers(groupId, page, RECORDS_PER_PAGE);
            Map<Long, String> roles = new HashMap<>();
            for (Account account : members) {
                roles.put(account.getId(), groupService.getMemberRole(groupId, account.getId()));
            }
            addAttributesIsGroupAdminOrCreator(groupId, loggedInAccountId, model);
            model.addAttribute("groupId", groupId);
            model.addAttribute("members", members);
            model.addAttribute("roles", roles);
            addAttributesPageAndNumberPages(page, numberPages, model);
            logger.info("Members of group {}, page: {}", groupId, page);
            return "groupMembers";
        } else {
            logger.warn("Account {} does not have permission to see group {} members", loggedInAccountId, groupId);
            return REDIRECT_TO_GROUP + groupId;
        }
    }

    @GetMapping("/groupJoinRequests")
    public String showGroupJoinRequests(@RequestParam(defaultValue = "1") int page, @RequestParam long groupId,
                                        @SessionAttribute long loggedInAccountId, Model model) {
        logger.info("Account {} tries to see group {} join request list", loggedInAccountId, groupId);
        if (isAdminOrCreator(groupId, loggedInAccountId)) {
            int numberPages = groupService.getNumberJoinRequestPages(groupId, RECORDS_PER_PAGE);
            List<Account> requests = groupService.getJoinAccounts(groupId, page, RECORDS_PER_PAGE);
            model.addAttribute("groupId", groupId);
            model.addAttribute("requests", requests);
            addAttributesPageAndNumberPages(page, numberPages, model);
            logger.info("Join requests to group {}, page: {}", groupId, page);
            return "joinGroupRequests";
        } else {
            logger.warn("Account {} does not have permission to see group {} join requests", loggedInAccountId,
                        groupId);
            return showGroupMembers(1, groupId, loggedInAccountId, model);
        }
    }

    @PostMapping("/joinGroup")
    public String joinGroup(@SessionAttribute long loggedInAccountId, @RequestParam long groupId) {
        logger.info("Account {} sent join request to group {}", accountService, groupId);
        if (!groupService.isGroupMember(groupId, loggedInAccountId) && !groupService.isJoinRequest(groupId,
                                                                                                   loggedInAccountId)) {
            groupService.getGroup(groupId)
                        .ifPresent(group -> accountService.getAccount(loggedInAccountId)
                                                          .ifPresent(account -> groupService.addGroupJoinRequest(group,
                                                                                                                 account)));
            logger.info("Request to join group {} has been sent", groupId);
        } else {
            logger.warn("Account{} is already a member of group {}", loggedInAccountId, groupId);
        }
        return REDIRECT_TO_GROUP + groupId;
    }

    @PostMapping("/leaveGroup")
    public String leaveGroup(@SessionAttribute long loggedInAccountId, @RequestParam long groupId) {
        logger.info("Account {} leave group {}", loggedInAccountId, groupId);
        if (groupService.isGroupMember(groupId, loggedInAccountId)) {
            groupService.deleteGroupMember(groupId, loggedInAccountId);
            if (groupService.getNumberGroupMembers(groupId) == 0) {
                groupService.deleteGroup(groupId);
            }
            logger.info("Account {} leave group {}", loggedInAccountId, groupId);
        } else {
            logger.warn("Account {} is not a member of group {}", loggedInAccountId, groupId);
        }
        return "redirect:/account/" + loggedInAccountId;
    }

    @PostMapping("/acceptJoinRequest")
    public String acceptJoinRequest(@SessionAttribute long loggedInAccountId, @RequestParam long groupId,
                                    @RequestParam long requestAccountId) {
        logger.info("Account {} accept join request to group {} from account {}", loggedInAccountId, groupId,
                    requestAccountId);
        if (isAdminOrCreator(groupId, loggedInAccountId) && groupService.isJoinRequest(groupId, requestAccountId)) {
            Optional<Group> group = groupService.getGroup(groupId);
            Optional<Account> account = accountService.getAccount(requestAccountId);
            if (group.isPresent() && account.isPresent()) {
                groupService.addMember(group.get(), account.get());
            }
            logger.info("Request to join group {} from account {} has bean accepted", groupId, requestAccountId);
        } else {
            logger.warn("Account {} does not have permission to accept member to group {}", loggedInAccountId, groupId);
        }
        return REDIRECT_TO_JOIN_REQUEST_LIST + groupId;
    }

    @PostMapping("/cancelJoinRequest")
    public String cancelJoinRequest(@SessionAttribute long loggedInAccountId, @RequestParam long groupId,
                                    @RequestParam long requestAccountId) {
        logger.info("Account {} accept join request to group {} from account {}", loggedInAccountId, groupId,
                    requestAccountId);
        if (isAdminOrCreator(groupId, loggedInAccountId) && groupService.isJoinRequest(groupId, requestAccountId)) {
            groupService.deleteGroupJoinRequest(groupId, requestAccountId);
            logger.info("Request to join group {} from account {} has bean canceled", groupId, requestAccountId);
        } else {
            logger.warn("Account {} does not have permission to cancel join group {} request", loggedInAccountId,
                        groupId);
        }
        return REDIRECT_TO_JOIN_REQUEST_LIST + groupId;
    }

    @PostMapping("/makeGroupAdmin")
    public String makeGroupAdmin(@SessionAttribute long loggedInAccountId, @RequestParam long groupId,
                                 @RequestParam int page, @RequestParam long accountId) {
        logger.info("Account {} wants to make account {} admin of group {}", loggedInAccountId, accountId, groupId);
        if (isAdminOrCreator(groupId, loggedInAccountId) && groupService.isGroupMember(groupId, accountId)) {
            groupService.makeAdmin(groupId, accountId);
            logger.info("Account {} promoted to group {} admin", accountId, groupId);
        } else {
            logger.warn("Account {} does not permission to manage group {} member roles", loggedInAccountId, groupId);
        }
        return REDIRECT_TO_GROUP_MEMBER_LIST + groupId + AND_PAGE_PARAMETER + page;
    }

    @PostMapping("/makeGroupMember")
    public String makeGroupMember(@SessionAttribute long loggedInAccountId, @RequestParam long groupId,
                                  @RequestParam int page, @RequestParam long accountId) {
        logger.info("Account {} wants to make account {} member of group {}", loggedInAccountId, accountId, groupId);
        if (groupService.isCreator(groupId, loggedInAccountId) && groupService.isAdmin(groupId, accountId)) {
            groupService.makeMember(groupId, accountId);
            logger.info("Account {} demoted to group {} member", accountId, groupId);
        } else {
            logger.warn("Account {} does not permission to manage group {} member roles", loggedInAccountId, groupId);
        }
        return REDIRECT_TO_GROUP_MEMBER_LIST + groupId + AND_PAGE_PARAMETER + page;
    }

    @PostMapping("/deleteGroupMember")
    public String deleteGroupMember(@SessionAttribute long loggedInAccountId, @RequestParam long groupId,
                                    @RequestParam int page, @RequestParam long accountId) {
        logger.info("Account {} delete account {} from group {}", loggedInAccountId, accountId, groupId);
        if (isAdminOrCreator(groupId, loggedInAccountId) && groupService.isMember(groupId, accountId)) {
            groupService.deleteGroupMember(groupId, accountId);
            logger.info("Account {} has bean deleted from group {}", accountId, groupId);
        } else {
            logger.warn("Account {} could not be deleted", accountId);
        }
        return REDIRECT_TO_GROUP_MEMBER_LIST + groupId + AND_PAGE_PARAMETER + page;
    }

    @PostMapping("/deleteGroup")
    public String deleteGroup(@SessionAttribute long loggedInAccountId, @RequestParam long groupId) {
        logger.info("Account {} tries to delete group {}", loggedInAccountId, groupId);
        if (groupService.isCreator(groupId, loggedInAccountId)) {
            groupService.deleteGroup(groupId);
            logger.info("Group {} deleted successfully", groupId);
            return "redirect:/account/" + loggedInAccountId;
        } else {
            logger.warn("Account {} can't delete group {}", loggedInAccountId, groupId);
            return REDIRECT_TO_GROUP + groupId;
        }
    }

    private void addAttributesIsGroupAdminOrCreator(long groupId, long accountId, Model model) {
        model.addAttribute("groupAdmin", groupService.isAdmin(groupId, accountId));
        model.addAttribute("groupCreator", groupService.isCreator(groupId, accountId));
    }

    private boolean isAdminOrCreator(long groupId, long accountId) {
        return groupService.isCreator(groupId, accountId) || groupService.isAdmin(groupId, accountId);
    }

    private void addAttributesPageAndNumberPages(int page, int numberPages, Model model) {
        model.addAttribute("page", page);
        model.addAttribute("numberPages", numberPages);
    }

}