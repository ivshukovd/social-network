package com.getjavajob.training.ivshukovd.socialnetwork.web.controllers;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Post;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.AccountDto;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.PostDto;
import com.getjavajob.training.ivshukovd.socialnetwork.service.AccountService;
import com.getjavajob.training.ivshukovd.socialnetwork.service.PostService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;

import static com.getjavajob.training.ivshukovd.socialnetwork.domain.util.EntityDtoConverter.convertAccountDtoToEntity;
import static com.getjavajob.training.ivshukovd.socialnetwork.domain.util.EntityDtoConverter.convertPostEntityToDto;
import static java.lang.Boolean.TRUE;
import static java.time.LocalDate.now;
import static javax.xml.bind.JAXBContext.newInstance;
import static javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT;
import static org.slf4j.LoggerFactory.getLogger;

@Controller
public class AccountController {

    private static final int RECORDS_PER_PAGE = 5;
    private static final String REDIRECT_TO_ERROR_PAGE = "redirect:/error";
    private static final String MODEL_ATTRIBUTE_ACCOUNT = "account";
    private static final String REDIRECT_TO_ACCOUNT_PAGE = "redirect:/account/";

    private static final Logger logger = getLogger(AccountController.class);
    private final AccountService accountService;
    private final PostService postService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AccountController(AccountService accountService, PostService postService, PasswordEncoder passwordEncoder) {
        this.accountService = accountService;
        this.postService = postService;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/")
    public String init() {
        logger.info("init method");
        return "redirect:/account/0";
    }

    @GetMapping("/registerAccount")
    public String showRegistrationPage() {
        logger.info("Account registration page");
        return "accountRegistration";
    }

    @PostMapping("/registerAccount")
    public String registerNewAccount(@ModelAttribute AccountDto newAccount) {
        logger.info("New account registration");
        newAccount.setPassword(passwordEncoder.encode(newAccount.getPassword()));
        Account account = new Account();
        account.setPhones(new ArrayList<>());
        convertAccountDtoToEntity(newAccount, account);
        account.setRegistrationDate(now());
        accountService.addAccount(account);
        logger.info("Account {} registered successfully", account.getId());
        return "redirect:/login";
    }

    @GetMapping("/account/{id}")
    public String showAccount(@PathVariable long id, @SessionAttribute long loggedInAccountId,
                              @SessionAttribute TimeZone timeZone, Model model) {
        logger.info("Account {} tries to see account {} page", loggedInAccountId, id);
        if (!accountService.existsAccountById(id) && id != 0) {
            return REDIRECT_TO_ERROR_PAGE;
        }
        if (id == 0) {
            id = loggedInAccountId;
        }
        boolean ownPage = loggedInAccountId == id;
        List<PostDto> posts = new ArrayList<>();
        for (Post post : postService.getAccountWallPosts(id, 1, RECORDS_PER_PAGE)) {
            posts.add(convertPostEntityToDto(post, timeZone));
        }
        accountService.getAccountWithPhones(id)
                      .ifPresent(account -> model.addAttribute(MODEL_ATTRIBUTE_ACCOUNT, account));
        model.addAttribute("admin", accountService.isAdmin(loggedInAccountId));
        model.addAttribute("ownPage", ownPage);
        model.addAttribute("follower", accountService.isFollower(loggedInAccountId, id));
        model.addAttribute("friend", accountService.isFriend(loggedInAccountId, id));
        model.addAttribute("posts", posts);
        model.addAttribute("numberPages", postService.getNumberAccountPostPages(id, RECORDS_PER_PAGE));
        logger.info("Displaying account {} page", loggedInAccountId);
        return "accountView";
    }

    @GetMapping("/editAccount/{accountId}")
    public String showEditAccountPage(@PathVariable long accountId, @SessionAttribute long loggedInAccountId,
                                      Model model) {
        logger.info("Account {} tries to see edit account {} page", loggedInAccountId, accountId);
        if (!accountService.existsAccountById(accountId)) {
            return REDIRECT_TO_ERROR_PAGE;
        }
        if (hasPermissionForEditing(loggedInAccountId, accountId)) {
            accountService.getAccountWithPhones(accountId)
                          .ifPresent(account -> model.addAttribute(MODEL_ATTRIBUTE_ACCOUNT, account));
            model.addAttribute("ownPage", loggedInAccountId == accountId);
            model.addAttribute("admin", accountService.isAdmin(loggedInAccountId));
            logger.info("Edit account {} page", accountId);
            return "accountEdit";
        }
        logger.warn("Account {} does not have permission to edit account {}", loggedInAccountId, accountId);
        return REDIRECT_TO_ACCOUNT_PAGE + loggedInAccountId;
    }

    @PostMapping("/editAccount")
    public String editAccount(@ModelAttribute AccountDto account, @RequestParam long accountId,
                              @SessionAttribute long loggedInAccountId) {
        logger.info("Account {} tries to edit account {} common information", loggedInAccountId, accountId);
        if (hasPermissionForEditing(loggedInAccountId, accountId)) {
            account.setId(accountId);
            accountService.updateAccount(account);
            logger.info("Successful account {} editing", accountId);
        } else {
            logger.warn("Account {} does not have permission to edit account {}", loggedInAccountId, accountId);
        }
        return REDIRECT_TO_ACCOUNT_PAGE + accountId;
    }

    @GetMapping("/changeAccountAvatar/{accountId}")
    public String showChangeAvatarPage(@PathVariable long accountId, Model model,
                                       @SessionAttribute long loggedInAccountId) {
        logger.info("Account {} tries to see change account {} avatar page", loggedInAccountId, accountId);
        if (!accountService.existsAccountById(accountId)) {
            return REDIRECT_TO_ERROR_PAGE;
        }
        if (hasPermissionForEditing(loggedInAccountId, accountId)) {
            model.addAttribute("accountId", accountId);
            accountService.getAccount(accountId)
                          .ifPresent(account -> model.addAttribute("accountPhotoId", account.getPhotoId()));
            logger.info("Avatar change page for account {}", accountId);
            return "accountAvatarChange";
        } else {
            logger.warn("Account {} does not have permission to change account {} avatar", loggedInAccountId,
                        accountId);
            return REDIRECT_TO_ACCOUNT_PAGE + accountId;
        }
    }

    @GetMapping("/changePassword/{accountId}")
    public String showChangePasswordPage(@RequestParam(required = false) boolean wrongPassword,
                                         @RequestParam(required = false) String message, @PathVariable long accountId,
                                         Model model, @SessionAttribute long loggedInAccountId) {
        logger.info("Account {} tries to see change account {} password page", loggedInAccountId, accountId);
        if (!accountService.existsAccountById(accountId)) {
            return REDIRECT_TO_ERROR_PAGE;
        }
        if (loggedInAccountId == accountId) {
            model.addAttribute("accountId", accountId);
            model.addAttribute("wrongPassword", wrongPassword);
            model.addAttribute("message", message);
            logger.info("Password change page for account {}", accountId);
            return "passwordChange";
        } else {
            logger.warn("Account {} does not have permission to change account {} password", loggedInAccountId,
                        accountId);
            return REDIRECT_TO_ACCOUNT_PAGE + accountId;
        }
    }

    @PostMapping("/changePassword")
    public String changePassword(@RequestParam long accountId, @RequestParam String oldPassword,
                                 @RequestParam String newPassword, @RequestParam String repeatedPassword,
                                 @SessionAttribute long loggedInAccountId) {
        logger.info("Account {} tries to change account {} password", loggedInAccountId, accountId);
        if (loggedInAccountId == accountId) {
            if (!newPassword.equals(repeatedPassword)) {
                logger.info("Incorrect repeated password");
                String message = "Repeated password is incorrect";
                return "redirect:/changePassword/" + accountId + "?wrongPassword=true&message=" + message;
            }
            Optional<Account> optionalAccount = accountService.getAccount(accountId);
            if (optionalAccount.isPresent()) {
                Account account = optionalAccount.get();
                if (passwordEncoder.matches(oldPassword, account.getPassword())) {
                    account.setPassword(passwordEncoder.encode(newPassword));
                    accountService.updateAccount(account);
                    logger.info("Account {} password is changed successfully", accountId);
                } else {
                    logger.info("Incorrect old password");
                    String message = "The old password is incorrect";
                    return "redirect:/changePassword/" + accountId + "?wrongPassword=true&message=" + message;
                }
            }
        }
        return REDIRECT_TO_ACCOUNT_PAGE + accountId;
    }

    @PostMapping("/account/makeAdmin")
    public String makeAdmin(@RequestParam("accountToMakeAdmin") long id, @SessionAttribute long loggedInAccountId) {
        logger.info("Account {} tries to make account {} admin", loggedInAccountId, id);
        if (!accountService.isAdmin(id)) {
            accountService.makeAdmin(id);
            logger.info("Account {} became admin", id);
        } else {
            logger.warn("Account {} is already admin", id);
        }
        return REDIRECT_TO_ACCOUNT_PAGE + id;
    }

    @PostMapping("/account/delete")
    public String deleteAccount(@RequestParam("accountToBeDelete") long id, @SessionAttribute long loggedInAccountId) {
        logger.info("Account {} tries to delete account {}", loggedInAccountId, id);
        if (id == loggedInAccountId || !accountService.isAdmin(id)) {
            accountService.deleteAccount(id);
            logger.info("Account {} deleted successfully", id);
            if (id == loggedInAccountId) {
                return "redirect:/logout";
            } else {
                return REDIRECT_TO_ACCOUNT_PAGE + loggedInAccountId;
            }
        } else {
            logger.warn("Account {} can't delete account {}", loggedInAccountId, id);
        }
        return "redirect:/editAccount/" + id;
    }

    @GetMapping("/exportXml")
    public void exportXml(@RequestParam long accountId, @SessionAttribute long loggedInAccountId,
                          HttpServletResponse response) {
        logger.info("Account {} tries to export XML with account {} information", loggedInAccountId, accountId);
        accountService.getAccountWithPhones(accountId).ifPresent(account -> {
            try {
                response.setContentType("application/xml");
                String fileName = "account" + accountId + ".xml";
                response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
                JAXBContext context = newInstance(Account.class);
                Marshaller marshaller = context.createMarshaller();
                marshaller.setProperty(JAXB_FORMATTED_OUTPUT, TRUE);
                marshaller.marshal(account, response.getOutputStream());
                response.flushBuffer();
                logger.info("XML exported successfully");
            } catch (JAXBException | IOException e) {
                logger.error("XML could not be exported");
            }
        });
    }

    @PostMapping("/importXml")
    @ResponseBody
    public Account importXml(@RequestParam MultipartFile fileXML) {
        logger.info("Import XML");
        try {
            JAXBContext context = newInstance(Account.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            Account account = (Account) unmarshaller.unmarshal(fileXML.getInputStream());
            logger.info("Account data from XML imported successfully");
            return account;
        } catch (JAXBException | IOException e) {
            return null;
        }
    }

    @GetMapping("/emailCheck")
    @ResponseBody
    public boolean checkEmailExists(@RequestParam String parameter) {
        logger.info("Check if email {} exists", parameter);
        return accountService.existsAccountByEmail(parameter);
    }

    @GetMapping("/phoneCheck")
    @ResponseBody
    public boolean checkPhoneExists(@RequestParam String parameter) {
        logger.info("Check if phone {} exists", parameter);
        return accountService.existsPhoneByNumber(parameter);
    }

    @GetMapping("/icqCheck")
    @ResponseBody
    public boolean checkIcqExists(@RequestParam String parameter) {
        logger.info("Check if icq {} exists", parameter);
        return accountService.existsAccountByIcq(parameter);
    }

    @GetMapping("/skypeCheck")
    @ResponseBody
    public boolean checkSkypeExists(@RequestParam String parameter) {
        logger.info("Check if skype {} exists", parameter);
        return accountService.existsAccountBySkype(parameter);
    }

    @GetMapping("/updatedEmailCheck")
    @ResponseBody
    public boolean checkUpdatedEmailExists(@RequestParam String parameter, @RequestParam long accountId) {
        logger.info("Check if updated email {} exists", parameter);
        return accountService.existsAccountByEmail(parameter) && !accountService.existsAccountByIdAndEmail(accountId,
                                                                                                           parameter);
    }

    @GetMapping("/newPhoneCheck")
    @ResponseBody
    public boolean checkNewPhoneExists(@RequestParam String parameter, @RequestParam long accountId) {
        logger.info("Check if phone {} exists", parameter);
        return accountService.existsPhoneByNumber(parameter) && !accountService.existsPhoneByAccountIdAndNumber(
                accountId, parameter);
    }

    @GetMapping("/updatedIcqCheck")
    @ResponseBody
    public boolean checkUpdatedIcqExists(@RequestParam String parameter, @RequestParam long accountId) {
        logger.info("Check if updated icq {} exists", parameter);
        return accountService.existsAccountByIcq(parameter) && !accountService.existsAccountByIdAndIcq(accountId,
                                                                                                       parameter);
    }

    @GetMapping("/updatedSkypeCheck")
    @ResponseBody
    public boolean checkUpdatedSkypeExists(@RequestParam String parameter, @RequestParam long accountId) {
        logger.info("Check if updated skype {} exists", parameter);
        return accountService.existsAccountBySkype(parameter) && !accountService.existsAccountByIdAndSkype(accountId,
                                                                                                           parameter);
    }

    private boolean hasPermissionForEditing(long loggedInUserId, long accountId) {
        return accountService.isAdmin(loggedInUserId) || loggedInUserId == accountId;
    }

}