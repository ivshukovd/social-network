package com.getjavajob.training.ivshukovd.socialnetwork.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

@EnableWebSecurity
@ComponentScan("com.getjavajob.training.ivshukovd.socialnetwork.web.config")
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final int COOKIE_TOKEN_VALIDITY_TIME_IN_SECONDS = 60 * 60 * 24 * 30;

    private final UserDetailsService userDetailsService;

    @Autowired
    public SecurityConfig(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
            .antMatchers("/resources/**", "/webjars/**")
            .permitAll()
            .antMatchers("/registerAccount", "/login", "/emailCheck", "/icqCheck", "/phoneCheck", "/skypeCheck")
            .anonymous()
            .antMatchers("/account/delete", "/account/makeAdmin")
            .hasRole("ADMIN")
            .anyRequest()
            .authenticated()
            .and()
            .formLogin()
            .loginPage("/login")
            .usernameParameter("email")
            .successForwardUrl("/afterSuccessLogin")
            .failureUrl("/login?error=true")
            .and()
            .rememberMe()
            .userDetailsService(userDetailsService)
            .rememberMeParameter("remember")
            .authenticationSuccessHandler(new SimpleUrlAuthenticationSuccessHandler("/setLoggedInAccountId"))
            .tokenValiditySeconds(COOKIE_TOKEN_VALIDITY_TIME_IN_SECONDS)
            .and()
            .logout()
            .logoutSuccessUrl("/afterLogout")
            .and()
            .exceptionHandling()
            .accessDeniedPage("/account/0")
            .and()
            .csrf()
            .disable();
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setPasswordEncoder(passwordEncoder());
        authenticationProvider.setUserDetailsService(userDetailsService);
        return authenticationProvider;
    }

}