package com.getjavajob.training.ivshukovd.socialnetwork.web.controllers;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Group;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Phone;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Post;
import com.getjavajob.training.ivshukovd.socialnetwork.service.AccountService;
import com.getjavajob.training.ivshukovd.socialnetwork.service.GroupService;
import com.getjavajob.training.ivshukovd.socialnetwork.service.PostService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.time.ZoneOffset.UTC;
import static java.util.Collections.singletonList;
import static java.util.Optional.of;
import static java.util.TimeZone.getTimeZone;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest(GroupController.class)
class GroupControllerTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    private GroupService groupService;
    @MockBean
    private AccountService accountService;
    @MockBean
    private PostService postService;

    @Test
    @WithAnonymousUser
    void testShowGroupCreatePage_AnonymousUser() throws Exception {
        mockMvc.perform(get("/createGroup"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testShowGroupCreatePage() throws Exception {
        mockMvc.perform(get("/createGroup"))
               .andExpect(status().isOk())
               .andExpect(view().name("createGroup"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/createGroup.jsp"));
    }

    @Test
    @WithAnonymousUser
    void testCreateGroup_AnonymousUser() throws Exception {
        mockMvc.perform(post("/createGroup"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testCreateGroup() throws Exception {
        Account account = new Account();
        account.setId(1);
        account.setFirstName("Ivan");
        account.setPhones(singletonList(new Phone()));
        when(accountService.getAccount(1)).thenReturn(of(account));
        doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            ((Group) args[0]).setId(1);
            return 1L;
        }).when(groupService).createGroup(ArgumentMatchers.any(Group.class), ArgumentMatchers.any(Account.class));
        mockMvc.perform(post("/createGroup").param("name", "Group")
                                            .param("description", "First group")
                                            .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/group/1"));
    }

    @Test
    @WithAnonymousUser
    void testShowGroup_AnonymousUser() throws Exception {
        mockMvc.perform(get("/group/1"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testShowGroup_AccountIsGroupAdmin_IsSocialNetworkAdmin() throws Exception {
        when(groupService.getNumberGroupMembers(1)).thenReturn(5L);
        when(groupService.isCreator(1, 1)).thenReturn(false);
        when(groupService.isGroupMember(1, 1)).thenReturn(true);
        when(groupService.isAdmin(1, 1)).thenReturn(true);
        when(accountService.isAdmin(1)).thenReturn(true);
        when(postService.getNumberGroupPostPages(1, 5)).thenReturn(1);
        Group group = new Group();
        group.setId(1);
        group.setName("Group");
        when(groupService.getGroup(1)).thenReturn(of(group));
        Post post1 = new Post();
        post1.setMessage("123");
        post1.setDate(LocalDateTime.now(UTC));
        Account author = new Account();
        author.setId(1);
        post1.setAuthor(author);
        List<Post> posts = new ArrayList<>();
        posts.add(post1);
        when(postService.getGroupWallPosts(1, 1, 5)).thenReturn(posts);
        mockMvc.perform(get("/group/1").sessionAttr("loggedInAccountId", 1)
                                       .sessionAttr("timeZone", getTimeZone("Europe/Moscow")))
               .andExpect(status().isOk())
               .andExpect(view().name("groupView"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/groupView.jsp"))
               .andExpect(model().attribute("groupMember", true))
               .andExpect(model().attribute("joinRequestExists", false))
               .andExpect(model().attribute("admin", true))
               .andExpect(model().attribute("groupAdmin", true))
               .andExpect(model().attribute("groupCreator", false))
               .andExpect(model().attribute("group", group))
               .andExpect(model().attribute("numberMembers", 5L))
               .andExpect(model().attribute("numberPages", 1))
               .andExpect(model().attribute("posts", hasItem(allOf(hasProperty("message", is("123"))))));
    }

    @Test
    @WithAnonymousUser
    void testShowEditGroupPage_AnonymousUser() throws Exception {
        mockMvc.perform(get("/editGroup/1"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testShowEditGroupPage_AccountIsAdmin_AccountIsNotCreator() throws Exception {
        when(groupService.isAdmin(1, 1)).thenReturn(true);
        when(groupService.isCreator(1, 1)).thenReturn(false);
        Group group = new Group();
        group.setId(1);
        group.setName("Group");
        when(groupService.getGroup(1)).thenReturn(of(group));
        mockMvc.perform(get("/editGroup/1").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(view().name("groupEdit"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/groupEdit.jsp"))
               .andExpect(model().attribute("group", group));
    }

    @Test
    @WithMockUser
    void testShowEditGroupPage_AccountIsNotAdmin_AccountIsCreator() throws Exception {
        when(groupService.isAdmin(1, 1)).thenReturn(false);
        when(groupService.isCreator(1, 1)).thenReturn(true);
        Group group = new Group();
        group.setId(1);
        group.setName("Group");
        when(groupService.getGroup(1)).thenReturn(of(group));
        mockMvc.perform(get("/editGroup/1").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(view().name("groupEdit"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/groupEdit.jsp"))
               .andExpect(model().attribute("group", group));
    }

    @Test
    @WithMockUser
    void testShowEditGroupPage_AccountIsNotAdmin_AccountIsNotCreator() throws Exception {
        when(groupService.isAdmin(1, 1)).thenReturn(false);
        when(groupService.isCreator(1, 1)).thenReturn(false);
        Group group = new Group();
        group.setId(1);
        group.setName("Group");
        when(groupService.getGroup(1)).thenReturn(of(group));
        mockMvc.perform(get("/editGroup/1").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/group/1"));
    }

    @Test
    @WithAnonymousUser
    void testEditGroup_AnonymousUser() throws Exception {
        mockMvc.perform(post("/editGroup"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @ParameterizedTest
    @CsvSource({"true, false, Updated group", "false, true, Updated group", "false, false, Group"})
    @WithMockUser
    void testEditGroup(boolean accountIsGroupAdmin, boolean accountIsGroupCreator, String groupName) throws Exception {
        when(groupService.isAdmin(1, 1)).thenReturn(accountIsGroupAdmin);
        when(groupService.isCreator(1, 1)).thenReturn(accountIsGroupCreator);
        Group group = new Group();
        group.setId(1);
        group.setName("Group");
        doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            Group updatedGroup = (Group) args[0];
            group.setName(updatedGroup.getName());
            return null;
        }).when(groupService).updateGroup(ArgumentMatchers.any(Group.class));
        when(groupService.getGroup(1)).thenReturn(of(group));
        mockMvc.perform(
                       post("/editGroup").param("id", "1").param("name", "Updated group").sessionAttr(
                               "loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/group/1"));
        assertEquals(groupName, group.getName());
    }

    @Test
    @WithAnonymousUser
    void testShowAccountGroupList_AnonymousUser() throws Exception {
        mockMvc.perform(get("/groups"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testShowAccountGroupList() throws Exception {
        when(groupService.getNumberGroupPagesByMemberId(1, 5)).thenReturn(1);
        Group group = new Group();
        group.setId(1);
        group.setName("Group");
        List<Group> groups = singletonList(group);
        when(groupService.getGroupsByMemberId(1, 1, 5)).thenReturn(groups);
        mockMvc.perform(get("/groups").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(view().name("groupList"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/groupList.jsp"))
               .andExpect(model().attribute("page", 1))
               .andExpect(model().attribute("numberPages", 1))
               .andExpect(model().attribute("groups", hasItem(equalTo(group))));
    }

    @Test
    @WithAnonymousUser
    void testShowGroupMembers_AnonymousUser() throws Exception {
        mockMvc.perform(get("/showGroupMembers"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    @WithMockUser
    void testShowGroupMembers_AccountIsGroupMember() throws Exception {
        when(groupService.isGroupMember(1, 1)).thenReturn(true);
        when(groupService.getNumberGroupMemberPages(1, 5)).thenReturn(1);
        Account account = new Account();
        account.setId(1);
        account.setFirstName("Ivan");
        account.setPhones(singletonList(new Phone()));
        List<Account> members = singletonList(account);
        when(groupService.getGroupMembers(1, 1, 5)).thenReturn(members);
        Map<Long, String> roles = new HashMap<>();
        roles.put(1L, "admin");
        when((groupService.getMemberRole(1, 1))).thenReturn("admin");
        mockMvc.perform(get("/showGroupMembers").param("groupId", "1").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(view().name("groupMembers"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/groupMembers.jsp"))
               .andExpect(model().attribute("groupId", 1L))
               .andExpect(model().attribute("page", 1))
               .andExpect(model().attribute("numberPages", 1))
               .andExpect(model().attribute("members", hasItem(equalTo(account))))
               .andExpect(model().attribute("roles", equalTo(roles)));
    }

    @Test
    @WithMockUser
    void testShowGroupMembers_AccountIsNotGroupMember() throws Exception {
        when(groupService.isGroupMember(1, 1)).thenReturn(false);
        when(groupService.getNumberGroupMemberPages(1, 5)).thenReturn(1);
        Account account = new Account();
        account.setId(1);
        account.setFirstName("Ivan");
        account.setPhones(singletonList(new Phone()));
        List<Account> members = singletonList(account);
        when(groupService.getGroupMembers(1, 1, 5)).thenReturn(members);
        when((groupService.getMemberRole(1, 1))).thenReturn("admin");
        mockMvc.perform(get("/showGroupMembers").param("groupId", "1").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/group/1"));
    }

    @Test
    @WithAnonymousUser
    void testShowGroupJoinRequests_AnonymousUser() throws Exception {
        mockMvc.perform(get("/groupJoinRequests"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    @WithMockUser
    void testShowGroupJoinRequests_AccountIsGroupAdmin_AccountIsNotGroupCreator() throws Exception {
        when(groupService.isAdmin(1, 1)).thenReturn(true);
        when(groupService.isCreator(1, 1)).thenReturn(false);
        when(groupService.getNumberJoinRequestPages(1, 5)).thenReturn(1);
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        account.setPhones(singletonList(new Phone()));
        List<Account> requests = singletonList(account);
        when(groupService.getJoinAccounts(1, 1, 5)).thenReturn(requests);
        mockMvc.perform(get("/groupJoinRequests").param("groupId", "1").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(view().name("joinGroupRequests"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/joinGroupRequests.jsp"))
               .andExpect(model().attribute("page", 1))
               .andExpect(model().attribute("numberPages", 1))
               .andExpect(model().attribute("groupId", 1L))
               .andExpect(model().attribute("requests", equalTo(requests)));
    }

    @Test
    @WithMockUser
    void testShowGroupJoinRequests_AccountIsNotGroupAdmin_AccountIsGroupCreator() throws Exception {
        when(groupService.isAdmin(1, 1)).thenReturn(false);
        when(groupService.isCreator(1, 1)).thenReturn(true);
        when(groupService.getNumberJoinRequestPages(1, 5)).thenReturn(1);
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        account.setPhones(singletonList(new Phone()));
        List<Account> requests = singletonList(account);
        when(groupService.getJoinAccounts(1, 1, 5)).thenReturn(requests);
        mockMvc.perform(get("/groupJoinRequests").param("groupId", "1").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(view().name("joinGroupRequests"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/joinGroupRequests.jsp"))
               .andExpect(model().attribute("page", 1))
               .andExpect(model().attribute("numberPages", 1))
               .andExpect(model().attribute("groupId", 1L))
               .andExpect(model().attribute("requests", equalTo(requests)));
    }

    @Test
    @WithMockUser
    void testShowGroupJoinRequests_AccountIsNotGroupAdmin_AccountIsNotGroupCreator() throws Exception {
        when(groupService.isAdmin(1, 1)).thenReturn(false);
        when(groupService.isCreator(1, 1)).thenReturn(false);
        when(groupService.isGroupMember(1, 1)).thenReturn(false);
        when(groupService.getNumberJoinRequestPages(1, 5)).thenReturn(1);
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        account.setPhones(singletonList(new Phone()));
        List<Account> requests = singletonList(account);
        when(groupService.getJoinAccounts(1, 1, 5)).thenReturn(requests);
        mockMvc.perform(get("/showGroupMembers").param("groupId", "1").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/group/1"));
    }

    @Test
    @WithAnonymousUser
    void testJoinGroup_AnonymousUser() throws Exception {
        mockMvc.perform(post("/joinGroup"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @ParameterizedTest
    @CsvSource({"false, false, 1", "false, true, 0", "true, false, 0", "true, true, 0"})
    @WithMockUser
    void testJoinGroup_AccountIsNotMember_RequestNotExistsYet(boolean accountIsGroupMember, boolean joinRequestExists,
                                                              int numberRequests) throws Exception {
        when(groupService.isGroupMember(1, 1)).thenReturn(accountIsGroupMember);
        when(groupService.isJoinRequest(1, 1)).thenReturn(joinRequestExists);
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        account.setPhones(singletonList(new Phone()));
        List<Account> requests = new ArrayList<>();
        Group group = new Group();
        group.setId(1);
        group.setName("Group");
        when(groupService.getGroup(1)).thenReturn(of(group));
        when(accountService.getAccount(1)).thenReturn(of(account));
        doAnswer(invocation -> {
            requests.add(account);
            return null;
        }).when(groupService).addGroupJoinRequest(group, account);
        mockMvc.perform(post("/joinGroup").param("groupId", "1").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/group/1"));
        assertEquals(numberRequests, requests.size());
    }

    @Test
    @WithAnonymousUser
    void testLeaveGroup_AnonymousUser() throws Exception {
        mockMvc.perform(post("/leaveGroup"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testLeaveGroup_AccountIsMember() throws Exception {
        when(groupService.isGroupMember(1, 1)).thenReturn(true);
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        account.setPhones(singletonList(new Phone()));
        List<Account> members = new ArrayList<>();
        members.add(account);
        doAnswer(invocation -> {
            members.remove(account);
            return null;
        }).when(groupService).deleteGroupMember(1, 1);
        mockMvc.perform(post("/leaveGroup").param("groupId", "1").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/1"));
        assertEquals(0, members.size());
    }

    @Test
    @WithMockUser
    void testLeaveGroup_AccountIsNotMember() throws Exception {
        when(groupService.isGroupMember(1, 1)).thenReturn(false);
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        account.setPhones(singletonList(new Phone()));
        List<Account> members = new ArrayList<>();
        members.add(account);
        doAnswer(invocation -> {
            members.remove(account);
            return null;
        }).when(groupService).deleteGroupMember(1, 1);
        mockMvc.perform(post("/leaveGroup").param("groupId", "1").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/1"));
        assertEquals(1, members.size());
    }

    @Test
    @WithAnonymousUser
    void testAcceptJoinRequest_AnonymousUser() throws Exception {
        mockMvc.perform(post("/acceptJoinRequest"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    @WithMockUser
    void testAcceptJoinRequest_LoggedInAccountIsGroupAdmin_JoinRequestExists() throws Exception {
        when(groupService.isAdmin(1, 1)).thenReturn(true);
        when(groupService.isCreator(1, 1)).thenReturn(false);
        when(groupService.isJoinRequest(1, 2)).thenReturn(true);
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        account.setPhones(singletonList(new Phone()));
        List<Account> requests = new ArrayList<>();
        requests.add(account);
        List<Account> members = new ArrayList<>();
        Group group = new Group();
        group.setId(1);
        group.setName("Group");
        when(groupService.getGroup(1)).thenReturn(of(group));
        when(accountService.getAccount(2)).thenReturn(of(account));
        doAnswer(invocation -> {
            members.add(account);
            requests.remove(account);
            return null;
        }).when(groupService).addMember(group, account);
        mockMvc.perform(post("/acceptJoinRequest").param("groupId", "1")
                                                  .param("requestAccountId", "2")
                                                  .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/groupJoinRequests?groupId=1"));
        assertEquals(1, members.size());
        assertEquals(0, requests.size());
    }

    @Test
    @WithMockUser
    void testAcceptJoinRequest_LoggedInAccountIsGroupCreator_JoinRequestExists() throws Exception {
        when(groupService.isAdmin(1, 1)).thenReturn(false);
        when(groupService.isCreator(1, 1)).thenReturn(true);
        when(groupService.isJoinRequest(1, 2)).thenReturn(true);
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        account.setPhones(singletonList(new Phone()));
        List<Account> requests = new ArrayList<>();
        requests.add(account);
        List<Account> members = new ArrayList<>();
        Group group = new Group();
        group.setId(1);
        group.setName("Group");
        when(groupService.getGroup(1)).thenReturn(of(group));
        when(accountService.getAccount(2)).thenReturn(of(account));
        doAnswer(invocation -> {
            members.add(account);
            requests.remove(account);
            return null;
        }).when(groupService).addMember(group, account);
        mockMvc.perform(post("/acceptJoinRequest").param("groupId", "1")
                                                  .param("requestAccountId", "2")
                                                  .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/groupJoinRequests?groupId=1"));
        assertEquals(1, members.size());
        assertEquals(0, requests.size());
    }

    @Test
    @WithMockUser
    void testAcceptJoinRequest_JoinRequestNotExists() throws Exception {
        when(groupService.isAdmin(1, 1)).thenReturn(true);
        when(groupService.isCreator(1, 1)).thenReturn(false);
        when(groupService.isJoinRequest(1, 2)).thenReturn(false);
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        account.setPhones(singletonList(new Phone()));
        List<Account> requests = new ArrayList<>();
        List<Account> members = new ArrayList<>();
        Group group = new Group();
        group.setId(1);
        group.setName("Group");
        when(groupService.getGroup(1)).thenReturn(of(group));
        when(accountService.getAccount(2)).thenReturn(of(account));
        doAnswer(invocation -> {
            members.add(account);
            return null;
        }).when(groupService).addMember(group, account);
        doAnswer(invocation -> {
            requests.remove(account);
            return null;
        }).when(groupService).deleteGroupJoinRequest(1, 2);
        mockMvc.perform(post("/acceptJoinRequest").param("groupId", "1")
                                                  .param("requestAccountId", "2")
                                                  .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/groupJoinRequests?groupId=1"));
        assertEquals(0, members.size());
        assertEquals(0, requests.size());
    }

    @Test
    @WithAnonymousUser
    void testCancelJoinRequest_AnonymousUser() throws Exception {
        mockMvc.perform(post("/cancelJoinRequest"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    @WithMockUser
    void testCancelJoinRequest_LoggedInAccountIsGroupAdmin_JoinRequestExists() throws Exception {
        when(groupService.isAdmin(1, 1)).thenReturn(true);
        when(groupService.isCreator(1, 1)).thenReturn(false);
        when(groupService.isJoinRequest(1, 2)).thenReturn(true);
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        account.setPhones(singletonList(new Phone()));
        List<Account> requests = new ArrayList<>();
        requests.add(account);
        Group group = new Group();
        group.setId(1);
        group.setName("Group");
        when(groupService.getGroup(1)).thenReturn(of(group));
        when(accountService.getAccount(2)).thenReturn(of(account));
        doAnswer(invocation -> {
            requests.remove(account);
            return null;
        }).when(groupService).deleteGroupJoinRequest(1, 2);
        mockMvc.perform(post("/cancelJoinRequest").param("groupId", "1")
                                                  .param("requestAccountId", "2")
                                                  .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/groupJoinRequests?groupId=1"));
        assertEquals(0, requests.size());
    }

    @Test
    @WithMockUser
    void testCancelJoinRequest_LoggedInAccountIsGroupCreator_JoinRequestExists() throws Exception {
        when(groupService.isAdmin(1, 1)).thenReturn(false);
        when(groupService.isCreator(1, 1)).thenReturn(true);
        when(groupService.isJoinRequest(1, 2)).thenReturn(true);
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        account.setPhones(singletonList(new Phone()));
        List<Account> requests = new ArrayList<>();
        requests.add(account);
        Group group = new Group();
        group.setId(1);
        group.setName("Group");
        when(groupService.getGroup(1)).thenReturn(of(group));
        when(accountService.getAccount(2)).thenReturn(of(account));
        doAnswer(invocation -> {
            requests.remove(account);
            return null;
        }).when(groupService).deleteGroupJoinRequest(1, 2);
        mockMvc.perform(post("/cancelJoinRequest").param("groupId", "1")
                                                  .param("requestAccountId", "2")
                                                  .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/groupJoinRequests?groupId=1"));
        assertEquals(0, requests.size());
    }

    @Test
    @WithMockUser
    void testCancelJoinRequest_JoinRequestNotExists() throws Exception {
        when(groupService.isAdmin(1, 1)).thenReturn(true);
        when(groupService.isCreator(1, 1)).thenReturn(false);
        when(groupService.isJoinRequest(1, 2)).thenReturn(false);
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        account.setPhones(singletonList(new Phone()));
        List<Account> requests = new ArrayList<>();
        requests.add(account);
        Group group = new Group();
        group.setId(1);
        group.setName("Group");
        when(groupService.getGroup(1)).thenReturn(of(group));
        when(accountService.getAccount(2)).thenReturn(of(account));
        doAnswer(invocation -> {
            requests.remove(account);
            return null;
        }).when(groupService).deleteGroupJoinRequest(1, 2);
        mockMvc.perform(post("/cancelJoinRequest").param("groupId", "1")
                                                  .param("requestAccountId", "2")
                                                  .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/groupJoinRequests?groupId=1"));
        assertEquals(1, requests.size());
    }

    @Test
    @WithAnonymousUser
    void testMakeGroupAdmin_AnonymousUser() throws Exception {
        mockMvc.perform(post("/makeGroupAdmin"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testMakeGroupAdmin_LoggedInAccountIsGroupAdmin_AccountIsGroupMember() throws Exception {
        when(groupService.isAdmin(1, 1)).thenReturn(true);
        when(groupService.isCreator(1, 1)).thenReturn(false);
        when(groupService.isGroupMember(1, 2)).thenReturn(true);
        final String[] role = {"member"};
        doAnswer(invocation -> {
            role[0] = "admin";
            return null;
        }).when(groupService).makeAdmin(1, 2);
        mockMvc.perform(post("/makeGroupAdmin").param("groupId", "1")
                                               .param("accountId", "2")
                                               .param("page", "1")
                                               .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/showGroupMembers?groupId=1&page=1"));
        assertEquals("admin", role[0]);
    }

    @Test
    @WithMockUser
    void testMakeGroupAdmin_LoggedInAccountIsGroupCreator_AccountIsGroupMember() throws Exception {
        when(groupService.isAdmin(1, 1)).thenReturn(false);
        when(groupService.isCreator(1, 1)).thenReturn(true);
        when(groupService.isGroupMember(1, 2)).thenReturn(true);
        final String[] role = {"role"};
        doAnswer(invocation -> {
            role[0] = "admin";
            return null;
        }).when(groupService).makeAdmin(1, 2);
        mockMvc.perform(post("/makeGroupAdmin").param("groupId", "1")
                                               .param("accountId", "2")
                                               .param("page", "1")
                                               .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/showGroupMembers?groupId=1&page=1"));
        assertEquals("admin", role[0]);
    }

    @Test
    @WithMockUser
    void testMakeGroupAdmin_AccountIsNotGroupMember() throws Exception {
        when(groupService.isAdmin(1, 1)).thenReturn(true);
        when(groupService.isCreator(1, 1)).thenReturn(false);
        when(groupService.isGroupMember(1, 2)).thenReturn(false);
        final String[] role = {"role"};
        doAnswer(invocation -> {
            role[0] = "admin";
            return null;
        }).when(groupService).makeAdmin(1, 2);
        mockMvc.perform(post("/makeGroupAdmin").param("groupId", "1")
                                               .param("accountId", "2")
                                               .param("page", "1")
                                               .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/showGroupMembers?groupId=1&page=1"));
        assertEquals("role", role[0]);
    }

    @Test
    @WithAnonymousUser
    void testMakeGroupMember_AnonymousUser() throws Exception {
        mockMvc.perform(post("/makeGroupMember"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    @WithMockUser
    void testMakeGroupMember_LoggedInAccountIsGroupCreator_AccountIsGroupAdmin() throws Exception {
        when(groupService.isAdmin(1, 2)).thenReturn(true);
        when(groupService.isCreator(1, 1)).thenReturn(true);
        final String[] role = {"role"};
        doAnswer(invocation -> {
            role[0] = "member";
            return null;
        }).when(groupService).makeMember(1, 2);
        mockMvc.perform(post("/makeGroupMember").param("groupId", "1")
                                                .param("accountId", "2")
                                                .param("page", "1")
                                                .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/showGroupMembers?groupId=1&page=1"));
        assertEquals("member", role[0]);
    }

    @Test
    @WithMockUser
    void testMakeGroupMember_LoggedInAccountIsGroupCreator_AccountIsNotGroupAdmin() throws Exception {
        when(groupService.isAdmin(1, 2)).thenReturn(false);
        when(groupService.isCreator(1, 1)).thenReturn(true);
        final String[] role = {"role"};
        doAnswer(invocation -> {
            role[0] = "member";
            return null;
        }).when(groupService).makeMember(1, 2);
        mockMvc.perform(post("/makeGroupMember").param("groupId", "1")
                                                .param("accountId", "2")
                                                .param("page", "1")
                                                .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/showGroupMembers?groupId=1&page=1"));
        assertEquals("role", role[0]);
    }

    @Test
    @WithAnonymousUser
    void testDeleteGroupMember_AnonymousUser() throws Exception {
        mockMvc.perform(post("/deleteGroupMember"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    @WithMockUser
    void testDeleteGroupMember_LoggedInAccountIsGroupAdmin_AccountIsMember() throws Exception {
        when(groupService.isMember(1, 2)).thenReturn(true);
        when(groupService.isAdmin(1, 1)).thenReturn(true);
        when(groupService.isCreator(1, 1)).thenReturn(false);
        List<Account> accounts = new ArrayList<>();
        accounts.add(new Account());
        doAnswer(invocation -> {
            accounts.remove(0);
            return null;
        }).when(groupService).deleteGroupMember(1, 2);
        mockMvc.perform(post("/deleteGroupMember").param("groupId", "1")
                                                  .param("accountId", "2")
                                                  .param("page", "1")
                                                  .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/showGroupMembers?groupId=1&page=1"));
        assertEquals(0, accounts.size());
    }

    @Test
    @WithMockUser
    void testDeleteGroupMember_LoggedInAccountIsGroupCreator_AccountIsMember() throws Exception {
        when(groupService.isMember(1, 2)).thenReturn(true);
        when(groupService.isAdmin(1, 1)).thenReturn(false);
        when(groupService.isCreator(1, 1)).thenReturn(true);
        List<Account> accounts = new ArrayList<>();
        accounts.add(new Account());
        doAnswer(invocation -> {
            accounts.remove(0);
            return null;
        }).when(groupService).deleteGroupMember(1, 2);
        mockMvc.perform(post("/deleteGroupMember").param("groupId", "1")
                                                  .param("accountId", "2")
                                                  .param("page", "1")
                                                  .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/showGroupMembers?groupId=1&page=1"));
        assertEquals(0, accounts.size());
    }

    @Test
    @WithMockUser
    void testDeleteGroupMember_LoggedInAccountIsNotGroupAdminOrAdmin_AccountIsMember() throws Exception {
        when(groupService.isMember(1, 2)).thenReturn(true);
        when(groupService.isAdmin(1, 1)).thenReturn(false);
        when(groupService.isCreator(1, 1)).thenReturn(false);
        List<Account> accounts = new ArrayList<>();
        accounts.add(new Account());
        doAnswer(invocation -> {
            accounts.remove(0);
            return null;
        }).when(groupService).deleteGroupMember(1, 2);
        mockMvc.perform(post("/deleteGroupMember").param("groupId", "1")
                                                  .param("accountId", "2")
                                                  .param("page", "1")
                                                  .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/showGroupMembers?groupId=1&page=1"));
        assertEquals(1, accounts.size());
    }

    @Test
    @WithMockUser
    void testDeleteGroupMember_LoggedInAccountIsGroupAdmin_AccountIsNotMember() throws Exception {
        when(groupService.isMember(1, 2)).thenReturn(false);
        when(groupService.isAdmin(1, 1)).thenReturn(true);
        when(groupService.isCreator(1, 1)).thenReturn(false);
        List<Account> accounts = new ArrayList<>();
        accounts.add(new Account());
        doAnswer(invocation -> {
            accounts.remove(0);
            return null;
        }).when(groupService).deleteGroupMember(1, 2);
        mockMvc.perform(post("/deleteGroupMember").param("groupId", "1")
                                                  .param("accountId", "2")
                                                  .param("page", "1")
                                                  .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/showGroupMembers?groupId=1&page=1"));
        assertEquals(1, accounts.size());
    }

    @Test
    @WithAnonymousUser
    void testDeleteGroup_AnonymousUser() throws Exception {
        mockMvc.perform(post("/deleteGroup"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    @WithMockUser
    void testDeleteGroup_AccountIsCreator() throws Exception {
        when(groupService.isCreator(1, 1)).thenReturn(true);
        final int[] numberGroups = {1};
        doAnswer(invocation -> {
            numberGroups[0] = 0;
            return null;
        }).when(groupService).deleteGroup(1);
        mockMvc.perform(post("/deleteGroup").param("groupId", "1").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/1"));
        assertEquals(0, numberGroups[0]);
    }

    @Test
    @WithMockUser
    void testDeleteGroup_AccountIsNotCreator() throws Exception {
        when(groupService.isCreator(1, 1)).thenReturn(false);
        final int[] numberGroups = {1};
        doAnswer(invocation -> {
            numberGroups[0] = 0;
            return null;
        }).when(groupService).deleteGroup(1);
        mockMvc.perform(post("/deleteGroup").param("groupId", "1").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/group/1"));
        assertEquals(1, numberGroups[0]);
    }

}