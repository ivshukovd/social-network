package com.getjavajob.training.ivshukovd.socialnetwork.web.controllers;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Image;
import com.getjavajob.training.ivshukovd.socialnetwork.service.AccountService;
import com.getjavajob.training.ivshukovd.socialnetwork.service.GroupService;
import com.getjavajob.training.ivshukovd.socialnetwork.service.ImageService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ImageUploadController.class)
class ImageUploadControllerTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    private AccountService accountService;
    @MockBean
    private GroupService groupService;
    @MockBean
    private ImageService imageService;

    @Test
    @WithAnonymousUser
    void testUploadAccountPhoto_AnonymousAccount() throws Exception {
        mockMvc.perform(post("/uploadAccountPhoto"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testUploadAccountPhoto() throws Exception {
        byte[] content = new byte[]{1, 2, 3};
        MockMultipartFile accPhoto = new MockMultipartFile("accPhoto", content);
        Image image = new Image();
        image.setId(1);
        image.setContent(content);
        when(imageService.saveImage(content)).thenReturn(image);
        final long[] imageId = {0};
        doAnswer(invocation -> {
            imageId[0] = 1;
            return null;
        }).when(accountService).updateAccountAvatar(1, 1);
        mockMvc.perform(multipart("/uploadAccountPhoto").file(accPhoto)
                                                        .param("accountId", "1")
                                                        .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/1"));
        Assertions.assertEquals(1, imageId[0]);
    }

    @Test
    @WithMockUser
    void testUploadAccountPhoto_AnotherAccountAvatar() throws Exception {
        byte[] content = new byte[]{1, 2, 3};
        MockMultipartFile accPhoto = new MockMultipartFile("accPhoto", content);
        Image image = new Image();
        image.setId(1);
        image.setContent(content);
        when(imageService.saveImage(content)).thenReturn(image);
        final long[] imageId = {0};
        doAnswer(invocation -> {
            imageId[0] = 1;
            return null;
        }).when(accountService).updateAccountAvatar(1, 1);
        mockMvc.perform(multipart("/uploadAccountPhoto").file(accPhoto)
                                                        .param("accountId", "1")
                                                        .sessionAttr("loggedInAccountId", 2))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/1"));
        Assertions.assertEquals(0, imageId[0]);
    }

    @Test
    @WithMockUser
    void testUploadAccountPhoto_PhotoNotAttached() throws Exception {
        byte[] content = new byte[]{};
        MockMultipartFile accPhoto = new MockMultipartFile("accPhoto", content);
        Image image = new Image();
        image.setId(1);
        image.setContent(content);
        when(imageService.saveImage(content)).thenReturn(image);
        final long[] imageId = {0};
        doAnswer(invocation -> {
            imageId[0] = 1;
            return null;
        }).when(accountService).updateAccountAvatar(1, 1);
        mockMvc.perform(multipart("/uploadAccountPhoto").file(accPhoto)
                                                        .param("accountId", "1")
                                                        .sessionAttr("loggedInAccountId", 2))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/1"));
        Assertions.assertEquals(0, imageId[0]);
    }

    @Test
    @WithAnonymousUser
    void testUploadGroupPhoto_AnonymousAccount() throws Exception {
        mockMvc.perform(post("/uploadGroupPhoto"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @ParameterizedTest
    @CsvSource({"true, false, 1", "false, true, 1", "false, false, 0"})
    @WithMockUser
    void testUploadGroupPhoto_PhotoIsAttached(boolean accountIsGroupAdmin, boolean accountIsGroupCreator,
                                              int expectedImageId) throws Exception {
        when(groupService.isAdmin(1, 1)).thenReturn(accountIsGroupAdmin);
        when(groupService.isCreator(1, 1)).thenReturn(accountIsGroupCreator);
        byte[] content = new byte[]{1, 2, 3};
        MockMultipartFile groupPhoto = new MockMultipartFile("groupPhoto", content);
        Image image = new Image();
        image.setId(1);
        image.setContent(content);
        when(imageService.saveImage(content)).thenReturn(image);
        final long[] imageId = {0};
        doAnswer(invocation -> {
            imageId[0] = 1;
            return null;
        }).when(groupService).updateGroupAvatar(1, 1);
        mockMvc.perform(multipart("/uploadGroupPhoto").file(groupPhoto)
                                                      .param("groupId", "1")
                                                      .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/group/1"));
        Assertions.assertEquals(expectedImageId, imageId[0]);
    }

    @Test
    @WithMockUser
    void testUploadGroupPhoto_PhotoIsNotAttached() throws Exception {
        when(groupService.isAdmin(1, 1)).thenReturn(true);
        when(groupService.isCreator(1, 1)).thenReturn(false);
        byte[] content = new byte[]{};
        MockMultipartFile groupPhoto = new MockMultipartFile("groupPhoto", content);
        Image image = new Image();
        image.setId(1);
        image.setContent(content);
        when(imageService.saveImage(content)).thenReturn(image);
        final long[] imageId = {0};
        doAnswer(invocation -> {
            imageId[0] = 1;
            return null;
        }).when(groupService).updateGroupAvatar(1, 1);
        mockMvc.perform(multipart("/uploadGroupPhoto").file(groupPhoto)
                                                      .param("groupId", "1")
                                                      .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/group/1"));
        Assertions.assertEquals(0, imageId[0]);
    }

}