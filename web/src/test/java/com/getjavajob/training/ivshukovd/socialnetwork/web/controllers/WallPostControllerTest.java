package com.getjavajob.training.ivshukovd.socialnetwork.web.controllers;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Image;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Post;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.PostDto;
import com.getjavajob.training.ivshukovd.socialnetwork.service.AccountService;
import com.getjavajob.training.ivshukovd.socialnetwork.service.GroupService;
import com.getjavajob.training.ivshukovd.socialnetwork.service.ImageService;
import com.getjavajob.training.ivshukovd.socialnetwork.service.PostService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.time.ZoneOffset.UTC;
import static java.util.TimeZone.getTimeZone;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(WallPostController.class)
class WallPostControllerTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    private AccountService accountService;
    @MockBean
    private GroupService groupService;
    @MockBean
    private ImageService imageService;
    @MockBean
    private PostService postService;

    @Test
    @WithAnonymousUser
    void testWritePostOnAccountWall_AnonymousUser() throws Exception {
        mockMvc.perform(post("/accountWallPost"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    @WithMockUser
    void testWritePostOnAccountWall_ImageAttached_TestWritten_AccountsAreFriends() throws Exception {
        when(accountService.isFriend(1, 2)).thenReturn(true);
        when(accountService.isAdmin(1)).thenReturn(false);
        byte[] content = new byte[]{1, 2, 3};
        MockMultipartFile file = new MockMultipartFile("image", content);
        final long[] imageId = {0};
        doAnswer(invocation -> {
            imageId[0] = 1;
            Image image = new Image();
            image.setId(1);
            return image;
        }).when(imageService).saveImage(content);
        final int[] posts = {0};
        doAnswer(invocation -> {
            posts[0] = 1;
            return null;
        }).when(postService).writePostToAccountWall(any(PostDto.class));
        mockMvc.perform(multipart("/accountWallPost").file(file)
                                                     .param("author", "1")
                                                     .param("targetId", "2")
                                                     .param("message", "text"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/2"));
        assertEquals(1, imageId[0]);
        assertEquals(1, posts[0]);
    }

    @Test
    @WithMockUser
    void testWritePostOnAccountWall_ImageAttached_TestWritten_AuthorIsAdmin() throws Exception {
        when(accountService.isFriend(1, 2)).thenReturn(false);
        when(accountService.isAdmin(1)).thenReturn(true);
        byte[] content = new byte[]{1, 2, 3};
        MockMultipartFile file = new MockMultipartFile("image", content);
        final long[] imageId = {0};
        doAnswer(invocation -> {
            imageId[0] = 1;
            Image image = new Image();
            image.setId(1);
            return image;
        }).when(imageService).saveImage(content);
        final int[] posts = {0};
        doAnswer(invocation -> {
            posts[0] = 1;
            return null;
        }).when(postService).writePostToAccountWall(any(PostDto.class));
        mockMvc.perform(multipart("/accountWallPost").file(file)
                                                     .param("author", "1")
                                                     .param("targetId", "2")
                                                     .param("message", "text"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/2"));
        assertEquals(1, imageId[0]);
        assertEquals(1, posts[0]);
    }

    @Test
    @WithMockUser
    void testWritePostOnAccountWall_ImageAttached_TestWritten_PostToOwnWall() throws Exception {
        when(accountService.isFriend(1, 2)).thenReturn(false);
        when(accountService.isAdmin(1)).thenReturn(false);
        byte[] content = new byte[]{1, 2, 3};
        MockMultipartFile file = new MockMultipartFile("image", content);
        final long[] imageId = {0};
        doAnswer(invocation -> {
            imageId[0] = 1;
            Image image = new Image();
            image.setId(1);
            return image;
        }).when(imageService).saveImage(content);
        final int[] posts = {0};
        doAnswer(invocation -> {
            posts[0] = 1;
            return null;
        }).when(postService).writePostToAccountWall(any(PostDto.class));
        mockMvc.perform(multipart("/accountWallPost").file(file)
                                                     .param("author", "1")
                                                     .param("targetId", "1")
                                                     .param("message", "text"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/1"));
        assertEquals(1, imageId[0]);
        assertEquals(1, posts[0]);
    }

    @Test
    @WithMockUser
    void testWritePostOnAccountWall_ImageNotAttached_TestWritten_AuthorIsAdmin() throws Exception {
        when(accountService.isFriend(1, 2)).thenReturn(false);
        when(accountService.isAdmin(1)).thenReturn(true);
        byte[] content = new byte[]{};
        MockMultipartFile file = new MockMultipartFile("image", content);
        final long[] imageId = {0};
        doAnswer(invocation -> {
            imageId[0] = 1;
            Image image = new Image();
            image.setId(1);
            return image;
        }).when(imageService).saveImage(content);
        final int[] posts = {0};
        doAnswer(invocation -> {
            posts[0] = 1;
            return null;
        }).when(postService).writePostToAccountWall(any(PostDto.class));
        mockMvc.perform(multipart("/accountWallPost").file(file)
                                                     .param("author", "1")
                                                     .param("targetId", "2")
                                                     .param("message", "text"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/2"));
        assertEquals(0, imageId[0]);
        assertEquals(1, posts[0]);
    }

    @Test
    @WithMockUser
    void testWritePostOnAccountWall_ImageAttached_TestNotWritten_AuthorIsAdmin() throws Exception {
        when(accountService.isFriend(1, 2)).thenReturn(false);
        when(accountService.isAdmin(1)).thenReturn(true);
        byte[] content = new byte[]{1, 2, 3};
        MockMultipartFile file = new MockMultipartFile("image", content);
        final long[] imageId = {0};
        doAnswer(invocation -> {
            imageId[0] = 1;
            Image image = new Image();
            image.setId(1);
            return image;
        }).when(imageService).saveImage(content);
        final int[] posts = {0};
        doAnswer(invocation -> {
            posts[0] = 1;
            return null;
        }).when(postService).writePostToAccountWall(any(PostDto.class));
        mockMvc.perform(multipart("/accountWallPost").file(file)
                                                     .param("author", "1")
                                                     .param("targetId", "2")
                                                     .param("message", ""))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/2"));
        assertEquals(1, imageId[0]);
        assertEquals(1, posts[0]);
    }

    @Test
    @WithMockUser
    void testWritePostOnAccountWall_ImageNotAttached_TestNotWritten_AuthorIsAdmin() throws Exception {
        when(accountService.isFriend(1, 2)).thenReturn(false);
        when(accountService.isAdmin(1)).thenReturn(true);
        byte[] content = new byte[]{};
        MockMultipartFile file = new MockMultipartFile("image", content);
        final long[] imageId = {0};
        doAnswer(invocation -> {
            imageId[0] = 1;
            Image image = new Image();
            image.setId(1);
            return image;
        }).when(imageService).saveImage(content);
        final int[] posts = {0};
        doAnswer(invocation -> {
            posts[0] = 1;
            return null;
        }).when(postService).writePostToAccountWall(any(PostDto.class));
        mockMvc.perform(multipart("/accountWallPost").file(file)
                                                     .param("author", "1")
                                                     .param("targetId", "2")
                                                     .param("message", ""))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/2"));
        assertEquals(0, imageId[0]);
        assertEquals(0, posts[0]);
    }

    @Test
    @WithAnonymousUser
    void testWritePostOnGroupWall_AnonymousUser() throws Exception {
        mockMvc.perform(post("/groupWallPost"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    @WithMockUser
    void testWritePostOnGroupWall_ImageAttached_TestWritten_AccountIsGroupMember() throws Exception {
        when(groupService.isGroupMember(2, 1)).thenReturn(true);
        byte[] content = new byte[]{1, 2, 3};
        MockMultipartFile file = new MockMultipartFile("image", content);
        final long[] imageId = {0};
        doAnswer(invocation -> {
            imageId[0] = 1;
            Image image = new Image();
            image.setId(1);
            return image;
        }).when(imageService).saveImage(content);
        final int[] posts = {0};
        doAnswer(invocation -> {
            posts[0] = 1;
            return null;
        }).when(postService).writePostToGroupWall(any(PostDto.class));
        mockMvc.perform(multipart("/groupWallPost").file(file)
                                                   .param("author", "1")
                                                   .param("targetId", "2")
                                                   .param("message", "text"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/group/2"));
        assertEquals(1, imageId[0]);
        assertEquals(1, posts[0]);
    }

    @Test
    @WithMockUser
    void testWritePostOnGroupWall_ImageNotAttached_TestWritten_AccountIsGroupMember() throws Exception {
        when(groupService.isGroupMember(2, 1)).thenReturn(true);
        byte[] content = new byte[]{};
        MockMultipartFile file = new MockMultipartFile("image", content);
        final long[] imageId = {0};
        doAnswer(invocation -> {
            imageId[0] = 1;
            Image image = new Image();
            image.setId(1);
            return image;
        }).when(imageService).saveImage(content);
        final int[] posts = {0};
        doAnswer(invocation -> {
            posts[0] = 1;
            return null;
        }).when(postService).writePostToGroupWall(any(PostDto.class));
        mockMvc.perform(multipart("/groupWallPost").file(file)
                                                   .param("author", "1")
                                                   .param("targetId", "2")
                                                   .param("message", "text"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/group/2"));
        assertEquals(0, imageId[0]);
        assertEquals(1, posts[0]);
    }

    @Test
    @WithMockUser
    void testWritePostOnGroupWall_ImageAttached_TestNotWritten_AccountIsGroupMember() throws Exception {
        when(groupService.isGroupMember(2, 1)).thenReturn(true);
        byte[] content = new byte[]{1, 2, 3};
        MockMultipartFile file = new MockMultipartFile("image", content);
        final long[] imageId = {0};
        doAnswer(invocation -> {
            imageId[0] = 1;
            Image image = new Image();
            image.setId(1);
            return image;
        }).when(imageService).saveImage(content);
        final int[] posts = {0};
        doAnswer(invocation -> {
            posts[0] = 1;
            return null;
        }).when(postService).writePostToGroupWall(any(PostDto.class));
        mockMvc.perform(
                       multipart("/groupWallPost").file(file).param("author", "1").param("targetId", "2").param(
                               "message", ""))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/group/2"));
        assertEquals(1, imageId[0]);
        assertEquals(1, posts[0]);
    }

    @Test
    @WithMockUser
    void testWritePostOnGroupWall_ImageNotAttached_TestNotWritten_AccountIsGroupMember() throws Exception {
        when(groupService.isGroupMember(2, 1)).thenReturn(true);
        byte[] content = new byte[]{};
        MockMultipartFile file = new MockMultipartFile("image", content);
        final long[] imageId = {0};
        doAnswer(invocation -> {
            imageId[0] = 1;
            Image image = new Image();
            image.setId(1);
            return image;
        }).when(imageService).saveImage(content);
        final int[] posts = {0};
        doAnswer(invocation -> {
            posts[0] = 1;
            return null;
        }).when(postService).writePostToGroupWall(any(PostDto.class));
        mockMvc.perform(
                       multipart("/groupWallPost").file(file).param("author", "1").param("targetId", "2").param(
                               "message", ""))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/group/2"));
        assertEquals(0, imageId[0]);
        assertEquals(0, posts[0]);
    }

    @Test
    @WithMockUser
    void testWritePostOnGroupWall_ImageAttached_TestWritten_AccountIsNotGroupMember() throws Exception {
        when(groupService.isGroupMember(2, 1)).thenReturn(false);
        byte[] content = new byte[]{1, 2, 3};
        MockMultipartFile file = new MockMultipartFile("image", content);
        final long[] imageId = {0};
        doAnswer(invocation -> {
            imageId[0] = 1;
            Image image = new Image();
            image.setId(1);
            return image;
        }).when(imageService).saveImage(content);
        final int[] posts = {0};
        doAnswer(invocation -> {
            posts[0] = 1;
            return null;
        }).when(postService).writePostToGroupWall(any(PostDto.class));
        mockMvc.perform(multipart("/groupWallPost").file(file)
                                                   .param("author", "1")
                                                   .param("targetId", "2")
                                                   .param("message", "text"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/group/2"));
        assertEquals(0, imageId[0]);
        assertEquals(0, posts[0]);
    }

    @Test
    @WithAnonymousUser
    void testGetPosts_AnonymousUser() throws Exception {
        mockMvc.perform(post("/groupWallPost"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    @WithMockUser
    void testGetPosts_OnAccountWall_AccountsAreFriends() throws Exception {
        when(accountService.isFriend(1, 2)).thenReturn(true);
        List<Post> posts = new ArrayList<>();
        Post post = new Post();
        post.setId(1);
        post.setMessage("text");
        post.setAuthor(new Account());
        post.setDate(LocalDateTime.now(UTC));
        posts.add(post);
        when(postService.getAccountWallPosts(2, 1, 5)).thenReturn(posts);
        mockMvc.perform(get("/getPosts").param("targetId", "2")
                                        .param("page", "1")
                                        .param("wallType", "ACCOUNT")
                                        .sessionAttr("loggedInAccountId", 1)
                                        .sessionAttr("timeZone", getTimeZone("Europe/Moscow")))
               .andExpect(status().isOk())
               .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$[0].message", is("text")));
    }

    @Test
    @WithMockUser
    void testGetPosts_OnAccountWall_OnOwnWall() throws Exception {
        when(accountService.isFriend(1, 1)).thenReturn(false);
        List<Post> posts = new ArrayList<>();
        Post post = new Post();
        post.setId(1);
        post.setMessage("text");
        post.setAuthor(new Account());
        post.setDate(LocalDateTime.now(UTC));
        posts.add(post);
        when(postService.getAccountWallPosts(1, 1, 5)).thenReturn(posts);
        mockMvc.perform(get("/getPosts").param("targetId", "1")
                                        .param("page", "1")
                                        .param("wallType", "ACCOUNT")
                                        .sessionAttr("loggedInAccountId", 1)
                                        .sessionAttr("timeZone", getTimeZone("Europe/Moscow")))
               .andExpect(status().isOk())
               .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$[0].message", is("text")));
    }

    @Test
    @WithMockUser
    void testGetPosts_OnAccountWall_AccountsAreNotFriends() throws Exception {
        when(accountService.isFriend(1, 2)).thenReturn(false);
        List<Post> posts = new ArrayList<>();
        Post post = new Post();
        post.setId(1);
        post.setMessage("text");
        post.setAuthor(new Account());
        posts.add(post);
        when(postService.getAccountWallPosts(2, 1, 5)).thenReturn(posts);
        mockMvc.perform(get("/getPosts").param("targetId", "2")
                                        .param("page", "1")
                                        .param("wallType", "ACCOUNT")
                                        .sessionAttr("loggedInAccountId", 1)
                                        .sessionAttr("timeZone", getTimeZone("Europe/Moscow")))
               .andExpect(status().isOk())
               .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    @WithMockUser
    void testGetPosts_OnGroupWall_AccountIsGroupMember() throws Exception {
        when(groupService.isGroupMember(2, 1)).thenReturn(true);
        List<Post> posts = new ArrayList<>();
        Post post = new Post();
        post.setId(1);
        post.setMessage("text");
        post.setAuthor(new Account());
        post.setDate(LocalDateTime.now(UTC));
        posts.add(post);
        when(postService.getGroupWallPosts(2, 1, 5)).thenReturn(posts);
        mockMvc.perform(get("/getPosts").param("targetId", "2")
                                        .param("page", "1")
                                        .param("wallType", "GROUP")
                                        .sessionAttr("loggedInAccountId", 1)
                                        .sessionAttr("timeZone", getTimeZone("Europe/Moscow")))
               .andExpect(status().isOk())
               .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$[0].message", is("text")));
    }

    @Test
    @WithMockUser
    void testGetPosts_OnGroupWall_AccountIsNotGroupMember() throws Exception {
        when(groupService.isGroupMember(2, 1)).thenReturn(false);
        List<Post> posts = new ArrayList<>();
        Post post = new Post();
        post.setId(1);
        post.setMessage("text");
        post.setAuthor(new Account());
        posts.add(post);
        when(postService.getGroupWallPosts(2, 1, 5)).thenReturn(posts);
        mockMvc.perform(get("/getPosts").param("targetId", "2")
                                        .param("page", "1")
                                        .param("wallType", "GROUP")
                                        .sessionAttr("loggedInAccountId", 1)
                                        .sessionAttr("timeZone", getTimeZone("Europe/Moscow")))
               .andExpect(status().isOk())
               .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    @WithAnonymousUser
    void testGetNumberPostPages_AnonymousUser() throws Exception {
        mockMvc.perform(post("/getNumberPostPages"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    @WithMockUser
    void testGetNumberPostPages_OnAccountWall_AccountsAreFriends() throws Exception {
        when(accountService.isFriend(1, 2)).thenReturn(true);
        when(postService.getNumberAccountPostPages(2, 5)).thenReturn(1);
        mockMvc.perform(get("/getNumberPostPages").param("targetId", "2")
                                                  .param("wallType", "ACCOUNT")
                                                  .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(content().string("1"));
    }

    @Test
    @WithMockUser
    void testGetNumberPostPages_OnAccountWall_OwnWall() throws Exception {
        when(accountService.isFriend(1, 1)).thenReturn(false);
        when(postService.getNumberAccountPostPages(1, 5)).thenReturn(1);
        mockMvc.perform(get("/getNumberPostPages").param("targetId", "1")
                                                  .param("wallType", "ACCOUNT")
                                                  .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(content().string("1"));
    }

    @Test
    @WithMockUser
    void testGetNumberPostPages_OnAccountWall_AccountsAreNotFriends() throws Exception {
        when(accountService.isFriend(1, 2)).thenReturn(false);
        when(postService.getNumberAccountPostPages(2, 5)).thenReturn(1);
        mockMvc.perform(get("/getNumberPostPages").param("targetId", "2")
                                                  .param("wallType", "ACCOUNT")
                                                  .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(content().string("0"));
    }

    @Test
    @WithMockUser
    void testGetNumberPostPages_OnGroupWall_AccountsIsGroupMember() throws Exception {
        when(groupService.isGroupMember(2, 1)).thenReturn(true);
        when(postService.getNumberGroupPostPages(2, 5)).thenReturn(1);
        mockMvc.perform(get("/getNumberPostPages").param("targetId", "2")
                                                  .param("wallType", "GROUP")
                                                  .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(content().string("1"));
    }

    @Test
    @WithMockUser
    void testGetNumberPostPages_OnGroupWall_AccountsIsNotGroupMember() throws Exception {
        when(groupService.isGroupMember(2, 1)).thenReturn(false);
        when(postService.getNumberGroupPostPages(2, 5)).thenReturn(1);
        mockMvc.perform(get("/getNumberPostPages").param("targetId", "2")
                                                  .param("wallType", "GROUP")
                                                  .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(content().string("0"));
    }

    @Test
    @WithAnonymousUser
    void testDeletePost_AnonymousUser() throws Exception {
        mockMvc.perform(post("/deletePost"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    @WithMockUser
    void testDeletePost_FromAccountWall_LoggedInAccountIsAdmin() throws Exception {
        when(accountService.isAdmin(1)).thenReturn(true);
        final int[] numberPosts = {1};
        doAnswer(invocation -> {
            numberPosts[0] = 0;
            return null;
        }).when(postService).deletePost(1);
        mockMvc.perform(post("/deletePost").param("postId", "1")
                                           .param("targetId", "2")
                                           .param("wallType", "ACCOUNT")
                                           .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(content().string("true"));
        assertEquals(0, numberPosts[0]);
    }

    @Test
    @WithMockUser
    void testDeletePost_FromAccountWall_OwnWall() throws Exception {
        when(accountService.isAdmin(1)).thenReturn(false);
        final int[] numberPosts = {1};
        doAnswer(invocation -> {
            numberPosts[0] = 0;
            return null;
        }).when(postService).deletePost(1);
        mockMvc.perform(post("/deletePost").param("postId", "1")
                                           .param("targetId", "1")
                                           .param("wallType", "ACCOUNT")
                                           .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(content().string("true"));
        assertEquals(0, numberPosts[0]);
    }

    @Test
    @WithMockUser
    void testDeletePost_FromAccountWall_AnotherAccountWall_LoggedInAccountIsNotAdmin() throws Exception {
        when(accountService.isAdmin(1)).thenReturn(false);
        final int[] numberPosts = {1};
        doAnswer(invocation -> {
            numberPosts[0] = 0;
            return null;
        }).when(postService).deletePost(1);
        mockMvc.perform(post("/deletePost").param("postId", "1")
                                           .param("targetId", "2")
                                           .param("wallType", "ACCOUNT")
                                           .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(content().string("false"));
        assertEquals(1, numberPosts[0]);
    }

    @Test
    @WithMockUser
    void testDeletePost_FromGroupWall_LoggedInAccountIsAdmin_IsNotGroupAdmin_IsNotGroupCreator_IsNotAuthor()
            throws Exception {
        when(accountService.isAdmin(1)).thenReturn(true);
        when(groupService.isAdmin(2, 1)).thenReturn(false);
        when(groupService.isCreator(2, 1)).thenReturn(false);
        when(postService.isAuthor(1, 1)).thenReturn(false);
        final int[] numberPosts = {1};
        doAnswer(invocation -> {
            numberPosts[0] = 0;
            return null;
        }).when(postService).deletePost(1);
        mockMvc.perform(post("/deletePost").param("postId", "1")
                                           .param("targetId", "2")
                                           .param("wallType", "GROUP")
                                           .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(content().string("true"));
        assertEquals(0, numberPosts[0]);
    }

    @Test
    @WithMockUser
    void testDeletePost_FromGroupWall_LoggedInAccountIsNotAdmin_IsGroupAdmin_IsNotGroupCreator_IsNotAuthor()
            throws Exception {
        when(accountService.isAdmin(1)).thenReturn(false);
        when(groupService.isAdmin(2, 1)).thenReturn(true);
        when(groupService.isCreator(2, 1)).thenReturn(false);
        when(postService.isAuthor(1, 1)).thenReturn(false);
        final int[] numberPosts = {1};
        doAnswer(invocation -> {
            numberPosts[0] = 0;
            return null;
        }).when(postService).deletePost(1);
        mockMvc.perform(post("/deletePost").param("postId", "1")
                                           .param("targetId", "2")
                                           .param("wallType", "GROUP")
                                           .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(content().string("true"));
        assertEquals(0, numberPosts[0]);
    }

    @Test
    @WithMockUser
    void testDeletePost_FromGroupWall_LoggedInAccountIsNotAdmin_IsNotGroupAdmin_IsGroupCreator_IsNotAuthor()
            throws Exception {
        when(accountService.isAdmin(1)).thenReturn(false);
        when(groupService.isAdmin(2, 1)).thenReturn(false);
        when(groupService.isCreator(2, 1)).thenReturn(true);
        when(postService.isAuthor(1, 1)).thenReturn(false);
        final int[] numberPosts = {1};
        doAnswer(invocation -> {
            numberPosts[0] = 0;
            return null;
        }).when(postService).deletePost(1);
        mockMvc.perform(post("/deletePost").param("postId", "1")
                                           .param("targetId", "2")
                                           .param("wallType", "GROUP")
                                           .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(content().string("true"));
        assertEquals(0, numberPosts[0]);
    }

    @Test
    @WithMockUser
    void testDeletePost_FromGroupWall_LoggedInAccountIsNotAdmin_IsNotGroupAdmin_IsNotGroupCreator_IsAuthor()
            throws Exception {
        when(accountService.isAdmin(1)).thenReturn(false);
        when(groupService.isAdmin(2, 1)).thenReturn(false);
        when(groupService.isCreator(2, 1)).thenReturn(false);
        when(postService.isAuthor(1, 1)).thenReturn(true);
        final int[] numberPosts = {1};
        doAnswer(invocation -> {
            numberPosts[0] = 0;
            return null;
        }).when(postService).deletePost(1);
        mockMvc.perform(post("/deletePost").param("postId", "1")
                                           .param("targetId", "2")
                                           .param("wallType", "GROUP")
                                           .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(content().string("true"));
        assertEquals(0, numberPosts[0]);
    }

    @Test
    @WithMockUser
    void testDeletePost_FromGroupWall_LoggedInAccountIsNotAdmin_IsNotGroupAdmin_IsNotGroupCreator_IsNotAuthor()
            throws Exception {
        when(accountService.isAdmin(1)).thenReturn(false);
        when(groupService.isAdmin(2, 1)).thenReturn(false);
        when(groupService.isCreator(2, 1)).thenReturn(false);
        when(postService.isAuthor(1, 1)).thenReturn(false);
        final int[] numberPosts = {1};
        doAnswer(invocation -> {
            numberPosts[0] = 0;
            return null;
        }).when(postService).deletePost(1);
        mockMvc.perform(post("/deletePost").param("postId", "1")
                                           .param("targetId", "2")
                                           .param("wallType", "GROUP")
                                           .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(content().string("false"));
        assertEquals(1, numberPosts[0]);
    }

}