package com.getjavajob.training.ivshukovd.socialnetwork.web.controllers;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.AccountShortDto;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.GroupDto;
import com.getjavajob.training.ivshukovd.socialnetwork.service.AccountService;
import com.getjavajob.training.ivshukovd.socialnetwork.service.GroupService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest(SearchController.class)
class SearchControllerTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    private AccountService accountService;
    @MockBean
    private GroupService groupService;

    @Test
    @WithAnonymousUser
    void testShowSearchedAccounts_AnonymousUser() throws Exception {
        mockMvc.perform(get("/search"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testShowSearchedResult_Accounts_EmptyString() throws Exception {
        when(accountService.getNumberAccountPages("", 5)).thenReturn(1);
        List<AccountShortDto> accounts = new ArrayList<>();
        AccountShortDto account = new AccountShortDto(1, "Ivan", "Ivanov", 1);
        accounts.add(account);
        when(accountService.getAccountsBySubstr("", 1, 5)).thenReturn(accounts);
        mockMvc.perform(get("/search").param("searchedElement", "account"))
               .andExpect(status().isOk())
               .andExpect(view().name("searchAccountResult"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/searchAccountResult.jsp"))
               .andExpect(model().attribute("accounts", equalTo(accounts)))
               .andExpect(model().attribute("page", 1))
               .andExpect(model().attribute("numberPages", 1))
               .andExpect(model().attribute("search", ""));
    }

    @Test
    @WithMockUser
    void testShowSearchedResult_Accounts() throws Exception {
        when(accountService.getNumberAccountPages("Iv", 5)).thenReturn(1);
        List<AccountShortDto> accounts = new ArrayList<>();
        AccountShortDto account = new AccountShortDto(1, "Ivan", "Ivanov", 1);
        accounts.add(account);
        when(accountService.getAccountsBySubstr("Iv", 1, 5)).thenReturn(accounts);
        mockMvc.perform(get("/search").param("searchedElement", "account").param("search", "Iv"))
               .andExpect(status().isOk())
               .andExpect(view().name("searchAccountResult"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/searchAccountResult.jsp"))
               .andExpect(model().attribute("accounts", equalTo(accounts)))
               .andExpect(model().attribute("page", 1))
               .andExpect(model().attribute("numberPages", 1))
               .andExpect(model().attribute("search", "Iv"));
    }

    @Test
    @WithMockUser
    void testShowSearchedResult_Accounts_FullName() throws Exception {
        when(accountService.getNumberAccountPages("Ivanov", "Ivan", 5)).thenReturn(1);
        List<AccountShortDto> accounts = new ArrayList<>();
        AccountShortDto account = new AccountShortDto(1, "Ivan", "Ivanov", 1);
        accounts.add(account);
        when(accountService.getAccountsByFullName("Ivan", "Ivanov", 1, 5)).thenReturn(accounts);
        mockMvc.perform(get("/search").param("searchedElement", "account").param("search", "Ivan Ivanov"))
               .andExpect(status().isOk())
               .andExpect(view().name("searchAccountResult"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/searchAccountResult.jsp"))
               .andExpect(model().attribute("accounts", equalTo(accounts)))
               .andExpect(model().attribute("page", 1))
               .andExpect(model().attribute("numberPages", 1))
               .andExpect(model().attribute("search", "Ivan Ivanov"));
    }

    @Test
    @WithMockUser
    void testShowSearchedResult_Groups_EmptyString() throws Exception {
        when(groupService.getNumberGroupPages("", 5)).thenReturn(1);
        List<GroupDto> groups = new ArrayList<>();
        GroupDto group = new GroupDto(1, "Group", 1);
        groups.add(group);
        when(groupService.getGroupsBySubstring("", 1, 5)).thenReturn(groups);
        mockMvc.perform(get("/search").param("searchedElement", "group"))
               .andExpect(status().isOk())
               .andExpect(view().name("searchGroupResult"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/searchGroupResult.jsp"))
               .andExpect(model().attribute("groups", equalTo(groups)))
               .andExpect(model().attribute("page", 1))
               .andExpect(model().attribute("numberPages", 1))
               .andExpect(model().attribute("search", ""));
    }

    @Test
    @WithMockUser
    void testShowSearchedResult_Groups() throws Exception {
        when(groupService.getNumberGroupPages("Gr", 5)).thenReturn(1);
        List<GroupDto> groups = new ArrayList<>();
        GroupDto group = new GroupDto(1, "Group", 1);
        groups.add(group);
        when(groupService.getGroupsBySubstring("Gr", 1, 5)).thenReturn(groups);
        mockMvc.perform(get("/search").param("searchedElement", "group").param("search", "Gr"))
               .andExpect(status().isOk())
               .andExpect(view().name("searchGroupResult"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/searchGroupResult.jsp"))
               .andExpect(model().attribute("groups", equalTo(groups)))
               .andExpect(model().attribute("page", 1))
               .andExpect(model().attribute("numberPages", 1))
               .andExpect(model().attribute("search", "Gr"));
    }

    @Test
    @WithAnonymousUser
    void testGetIntermediateSearchResultForAccounts_AnonymousUser() throws Exception {
        mockMvc.perform(get("/showFoundAccounts"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testGetIntermediateSearchResultForAccounts() throws Exception {
        List<AccountShortDto> accounts = new ArrayList<>();
        AccountShortDto account = new AccountShortDto(1, "Ivan", "Ivanov", 1);
        accounts.add(account);
        when(accountService.getAccountsBySubstr("iv", 1, 5)).thenReturn(accounts);
        mockMvc.perform(get("/showFoundAccounts").param("filter", "iv"))
               .andExpect(status().isOk())
               .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$[0].surName", is("Ivanov")));
    }

    @Test
    @WithAnonymousUser
    void testSearchAccounts_AnonymousUser() throws Exception {
        mockMvc.perform(get("/showFoundAccounts"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testSearchAccounts_ByOneWord() throws Exception {
        List<AccountShortDto> accounts = new ArrayList<>();
        AccountShortDto account = new AccountShortDto(1, "Ivan", "Ivanov", 1);
        accounts.add(account);
        when(accountService.getAccountsBySubstr("iv", 1, 5)).thenReturn(accounts);
        mockMvc.perform(get("/searchAccounts").param("search", "iv").param("page", "1"))
               .andExpect(status().isOk())
               .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$[0].surName", is("Ivanov")));
    }

    @Test
    @WithMockUser
    void testSearchAccounts_ByFullName() throws Exception {
        List<AccountShortDto> accounts = new ArrayList<>();
        AccountShortDto account = new AccountShortDto(1, "Ivan", "Ivanov", 1);
        accounts.add(account);
        when(accountService.getAccountsByFullName("Ivan", "Ivanov", 1, 5)).thenReturn(accounts);
        mockMvc.perform(get("/searchAccounts").param("search", "Ivan Ivanov").param("page", "1"))
               .andExpect(status().isOk())
               .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$[0].surName", is("Ivanov")));
    }

    @Test
    @WithAnonymousUser
    void testGetNumberSearchedAccountPages_AnonymousUser() throws Exception {
        mockMvc.perform(get("/numberSearchedAccountPages"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testGetNumberSearchedAccountPages_ByOneWord() throws Exception {
        when(accountService.getNumberAccountPages("iv", 5)).thenReturn(1);
        mockMvc.perform(get("/numberSearchedAccountPages").param("search", "iv"))
               .andExpect(status().isOk())
               .andExpect(content().string("1"));
    }

    @Test
    @WithMockUser
    void testGetNumberSearchedAccountPages_ByFullName() throws Exception {
        when(accountService.getNumberAccountPages("Ivan", "Ivanov", 5)).thenReturn(1);
        mockMvc.perform(get("/numberSearchedAccountPages").param("search", "Ivan Ivanov"))
               .andExpect(status().isOk())
               .andExpect(content().string("1"));
    }

    @Test
    @WithAnonymousUser
    void getIntermediateSearchResultForGroups_AnonymousUser() throws Exception {
        mockMvc.perform(get("/showFoundGroups"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void getIntermediateSearchResultForGroups() throws Exception {
        List<GroupDto> groups = new ArrayList<>();
        GroupDto group = new GroupDto(1, "Group", 1);
        groups.add(group);
        when(groupService.getGroupsBySubstring("gr", 1, 5)).thenReturn(groups);
        mockMvc.perform(get("/showFoundGroups").param("filter", "gr"))
               .andExpect(status().isOk())
               .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$[0].name", is("Group")));
    }

    @Test
    @WithAnonymousUser
    void testSearchGroups_AnonymousUser() throws Exception {
        mockMvc.perform(get("/searchGroups"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testSearchGroups() throws Exception {
        List<GroupDto> groups = new ArrayList<>();
        GroupDto group = new GroupDto(1, "Group", 1);
        groups.add(group);
        when(groupService.getGroupsBySubstring("gr", 1, 5)).thenReturn(groups);
        mockMvc.perform(get("/searchGroups").param("search", "gr").param("page", "1"))
               .andExpect(status().isOk())
               .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$[0].name", is("Group")));
    }

    @Test
    @WithAnonymousUser
    void getNumberSearchedGroupPages_AnonymousUser() throws Exception {
        mockMvc.perform(get("/numberSearchedGroupPages"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void getNumberSearchedGroupPages() throws Exception {
        when(groupService.getNumberGroupPages("gr", 5)).thenReturn(1);
        mockMvc.perform(get("/numberSearchedGroupPages").param("search", "gr"))
               .andExpect(status().isOk())
               .andExpect(content().string("1"));
    }

}