package com.getjavajob.training.ivshukovd.socialnetwork.web.controllers;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Dialogue;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Message;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Phone;
import com.getjavajob.training.ivshukovd.socialnetwork.service.AccountService;
import com.getjavajob.training.ivshukovd.socialnetwork.service.DialogueService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static java.time.ZoneOffset.UTC;
import static java.util.Collections.singletonList;
import static java.util.Optional.of;
import static java.util.TimeZone.getTimeZone;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest(DialogueController.class)
class DialogueControllerTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    private DialogueService dialogueService;
    @MockBean
    private AccountService accountService;

    @Test
    @WithAnonymousUser
    void testShowDialogueList_AnonymousUser() throws Exception {
        mockMvc.perform(get("/dialogues"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testShowDialogueList() throws Exception {
        Dialogue dialogue = new Dialogue();
        dialogue.setId(1);
        List<Dialogue> dialogues = singletonList(dialogue);
        when(dialogueService.getNumberDialoguePages(1, 5)).thenReturn(1);
        when(dialogueService.getDialogues(1, 1, 5)).thenReturn(dialogues);
        mockMvc.perform(get("/dialogues").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(view().name("dialogueList"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/dialogueList.jsp"))
               .andExpect(model().attribute("dialogues", hasItem(allOf(hasProperty("id", is(1L))))))
               .andExpect(model().attribute("page", 1))
               .andExpect(model().attribute("numberPages", 1));
    }

    @Test
    @WithAnonymousUser
    void testShowDialogue_AnonymousUser() throws Exception {
        mockMvc.perform(get("/dialogue"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testShowDialogue_Friends_LoggedInAccountIsAdmin_DialogueExists() throws Exception {
        when(accountService.isFriend(2, 1)).thenReturn(true);
        when(accountService.isAdmin(1)).thenReturn(true);
        Account account1 = new Account();
        account1.setId(1);
        account1.setPhones(singletonList(new Phone()));
        Account account2 = new Account();
        account2.setPhones(singletonList(new Phone()));
        account2.setId(2);
        Dialogue dialogue1 = new Dialogue();
        dialogue1.setId(1);
        dialogue1.setAccount1(account1);
        dialogue1.setAccount2(account2);
        Dialogue dialogue2 = new Dialogue();
        dialogue2.setId(2);
        dialogue2.setAccount1(account1);
        dialogue2.setAccount2(account2);
        when(dialogueService.getDialogueByAccountsId(2, 1)).thenReturn(of(dialogue1));
        when(dialogueService.createDialogue(2, 1)).thenReturn(dialogue2);
        Message message = new Message();
        message.setText("new message");
        message.setDate(LocalDateTime.now(UTC));
        List<Message> messages = singletonList(message);
        when(dialogueService.getNumberMessagePages(1, 5)).thenReturn(1);
        when(dialogueService.getMessages(1, 1, 5)).thenReturn(messages);
        mockMvc.perform(get("/dialogue").param("dialoguePartnerId", "1")
                                        .sessionAttr("loggedInAccountId", 2)
                                        .sessionAttr("timeZone", getTimeZone("Europe/Moscow")))
               .andExpect(status().isOk())
               .andExpect(view().name("dialogue"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/dialogue.jsp"))
               .andExpect(
                       model().attribute("messages", Matchers.hasItem(allOf(hasProperty("text", is("new message"))))))
               .andExpect(model().attribute("dialogueId", 1L))
               .andExpect(model().attribute("dialoguePartner", account1))
               .andExpect(model().attribute("numberPages", 1))
               .andExpect(model().attribute("page", 1));
    }

    @Test
    @WithMockUser
    void testShowDialogue_Friends_LoggedInAccountIsAdmin_DialogueNotExists() throws Exception {
        when(accountService.isFriend(2, 1)).thenReturn(true);
        when(accountService.isAdmin(1)).thenReturn(true);
        Account account1 = new Account();
        account1.setId(1);
        account1.setPhones(singletonList(new Phone()));
        Account account2 = new Account();
        account2.setId(2);
        account2.setPhones(singletonList(new Phone()));
        Dialogue dialogue2 = new Dialogue();
        dialogue2.setId(2);
        dialogue2.setAccount1(account1);
        dialogue2.setAccount2(account2);
        when(dialogueService.getDialogueByAccountsId(2, 1)).thenReturn(Optional.empty());
        when(dialogueService.createDialogue(2, 1)).thenReturn(dialogue2);
        Message message = new Message();
        message.setText("new message");
        message.setDate(LocalDateTime.now(UTC));
        List<Message> messages = singletonList(message);
        when(dialogueService.getNumberMessagePages(2, 5)).thenReturn(1);
        when(dialogueService.getMessages(2, 1, 5)).thenReturn(messages);
        mockMvc.perform(get("/dialogue").param("dialoguePartnerId", "1")
                                        .sessionAttr("loggedInAccountId", 2)
                                        .sessionAttr("timeZone", getTimeZone("Europe/Moscow")))
               .andExpect(status().isOk())
               .andExpect(view().name("dialogue"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/dialogue.jsp"))
               .andExpect(
                       model().attribute("messages", Matchers.hasItem(allOf(hasProperty("text", is("new message"))))))
               .andExpect(model().attribute("dialogueId", 2L))
               .andExpect(model().attribute("dialoguePartner", account1))
               .andExpect(model().attribute("numberPages", 1))
               .andExpect(model().attribute("page", 1));
    }

    @Test
    @WithMockUser
    void testShowDialogue_NotFriends_LoggedInAccountIsAdmin_DialogueExists() throws Exception {
        when(accountService.isFriend(2, 1)).thenReturn(false);
        when(accountService.isAdmin(2)).thenReturn(true);
        Account account1 = new Account();
        account1.setId(1);
        account1.setPhones(singletonList(new Phone()));
        Account account2 = new Account();
        account2.setId(2);
        account2.setPhones(singletonList(new Phone()));
        Dialogue dialogue1 = new Dialogue();
        dialogue1.setId(1);
        dialogue1.setAccount1(account1);
        dialogue1.setAccount2(account2);
        Dialogue dialogue2 = new Dialogue();
        dialogue2.setId(1);
        dialogue2.setAccount1(account1);
        dialogue2.setAccount2(account2);
        when(dialogueService.getDialogueByAccountsId(2, 1)).thenReturn(of(dialogue1));
        when(dialogueService.createDialogue(2, 1)).thenReturn(dialogue2);
        Message message = new Message();
        message.setText("new message");
        message.setDate(LocalDateTime.now(UTC));
        List<Message> messages = singletonList(message);
        when(dialogueService.getNumberMessagePages(1, 5)).thenReturn(1);
        when(dialogueService.getMessages(1, 1, 5)).thenReturn(messages);
        mockMvc.perform(get("/dialogue").param("dialoguePartnerId", "1")
                                        .sessionAttr("loggedInAccountId", 2)
                                        .sessionAttr("timeZone", getTimeZone("Europe/Moscow")))
               .andExpect(status().isOk())
               .andExpect(view().name("dialogue"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/dialogue.jsp"))
               .andExpect(
                       model().attribute("messages", Matchers.hasItem(allOf(hasProperty("text", is("new message"))))))
               .andExpect(model().attribute("dialogueId", 1L))
               .andExpect(model().attribute("dialoguePartner", account1))
               .andExpect(model().attribute("numberPages", 1))
               .andExpect(model().attribute("page", 1));
    }

    @Test
    @WithMockUser
    void testShowDialogue_Friends_LoggedInAccountIsNotAdmin_DialogueExists() throws Exception {
        when(accountService.isFriend(2, 1)).thenReturn(true);
        when(accountService.isAdmin(1)).thenReturn(false);
        Account account1 = new Account();
        account1.setId(1);
        account1.setPhones(singletonList(new Phone()));
        Account account2 = new Account();
        account2.setId(2);
        account2.setPhones(singletonList(new Phone()));
        Dialogue dialogue1 = new Dialogue();
        dialogue1.setId(1);
        dialogue1.setAccount1(account1);
        dialogue1.setAccount2(account2);
        Dialogue dialogue2 = new Dialogue();
        dialogue2.setId(1);
        dialogue2.setAccount1(account1);
        dialogue2.setAccount2(account2);
        when(dialogueService.getDialogueByAccountsId(2, 1)).thenReturn(of(dialogue1));
        when(dialogueService.createDialogue(2, 1)).thenReturn(dialogue2);
        Message message = new Message();
        message.setText("new message");
        message.setDate(LocalDateTime.now(UTC));
        List<Message> messages = singletonList(message);
        when(dialogueService.getNumberMessagePages(1, 5)).thenReturn(1);
        when(dialogueService.getMessages(1, 1, 5)).thenReturn(messages);
        mockMvc.perform(get("/dialogue").param("dialoguePartnerId", "1")
                                        .sessionAttr("loggedInAccountId", 2)
                                        .sessionAttr("timeZone", getTimeZone("Europe/Moscow")))
               .andExpect(status().isOk())
               .andExpect(view().name("dialogue"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/dialogue.jsp"))
               .andExpect(
                       model().attribute("messages", Matchers.hasItem(allOf(hasProperty("text", is("new message"))))))
               .andExpect(model().attribute("dialogueId", 1L))
               .andExpect(model().attribute("dialoguePartner", account1))
               .andExpect(model().attribute("numberPages", 1))
               .andExpect(model().attribute("page", 1));
    }

    @Test
    @WithMockUser
    void testShowDialogue_NotFriends_LoggedInAccountIsNotAdmin_DialogueExists() throws Exception {
        when(accountService.isFriend(2, 1)).thenReturn(false);
        when(accountService.isAdmin(1)).thenReturn(false);
        Account account1 = new Account();
        account1.setId(1);
        Account account2 = new Account();
        account2.setId(2);
        Dialogue dialogue1 = new Dialogue();
        dialogue1.setId(1);
        dialogue1.setAccount1(account1);
        dialogue1.setAccount2(account2);
        Dialogue dialogue2 = new Dialogue();
        dialogue2.setId(1);
        dialogue2.setAccount1(account1);
        dialogue2.setAccount2(account2);
        when(dialogueService.getDialogueByAccountsId(2, 1)).thenReturn(of(dialogue1));
        when(dialogueService.createDialogue(2, 1)).thenReturn(dialogue2);
        Message message = new Message();
        message.setText("new message");
        List<Message> messages = singletonList(message);
        when(dialogueService.getNumberMessagePages(1, 5)).thenReturn(1);
        when(dialogueService.getMessages(1, 1, 5)).thenReturn(messages);
        mockMvc.perform(get("/dialogue").param("dialoguePartnerId", "1")
                                        .sessionAttr("loggedInAccountId", 2)
                                        .sessionAttr("timeZone", getTimeZone("Europe/Moscow")))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/2"));
    }


}