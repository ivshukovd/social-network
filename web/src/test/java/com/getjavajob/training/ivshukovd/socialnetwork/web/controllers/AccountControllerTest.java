package com.getjavajob.training.ivshukovd.socialnetwork.web.controllers;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Phone;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Post;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.AccountDto;
import com.getjavajob.training.ivshukovd.socialnetwork.service.AccountService;
import com.getjavajob.training.ivshukovd.socialnetwork.service.PostService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static java.time.LocalDateTime.now;
import static java.time.ZoneOffset.UTC;
import static java.util.Collections.singletonList;
import static java.util.Optional.of;
import static java.util.TimeZone.getTimeZone;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest(AccountController.class)
class AccountControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private AccountService accountService;
    @MockBean
    private PostService postService;
    @MockBean
    private PasswordEncoder passwordEncoder;

    @Test
    @WithAnonymousUser
    void testInit_AnonymousUser() throws Exception {
        mockMvc.perform(get("/"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http://localhost" + "/login"));
    }

    @Test
    @WithMockUser
    void testInit() throws Exception {
        mockMvc.perform(get("/")).andExpect(status().is3xxRedirection()).andExpect(redirectedUrl("/account/0"));
    }

    @Test
    @WithMockUser
    void testShowRegistrationPage_LoggedInAccount() throws Exception {
        mockMvc.perform(get("/registerAccount")).andExpect(status().is4xxClientError());
    }

    @Test
    @WithAnonymousUser
    void testShowRegistrationPage() throws Exception {
        mockMvc.perform(get("/registerAccount"))
               .andExpect(status().isOk())
               .andExpect(view().name("accountRegistration"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/accountRegistration.jsp"));
    }

    @Test
    @WithMockUser
    void testRegisterNewAccount_LoggedInAccount() throws Exception {
        mockMvc.perform(post("/registerAccount")).andExpect(status().is4xxClientError());
    }

    @Test
    @WithAnonymousUser
    void testRegisterNewAccount() throws Exception {
        List<Account> accounts = new ArrayList<>();
        doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            accounts.add((Account) args[0]);
            return null;
        }).when(accountService).addAccount(ArgumentMatchers.any(Account.class));
        mockMvc.perform(post("/registerAccount").param("firstName", "Ivan").param("phones[0]", "12345"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/login"));
        assertEquals("Ivan", accounts.get(0).getFirstName());
    }

    @Test
    @WithAnonymousUser
    void testShowAccount_AnonymousUser() throws Exception {
        mockMvc.perform(get("/account/1"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testShowAccount_AccountNotExists() throws Exception {
        when(accountService.existsAccountById(1)).thenReturn(false);
        mockMvc.perform(get("/account/1").sessionAttr("loggedInAccountId", 2)
                                         .sessionAttr("timeZone", getTimeZone("Europe/Moscow")))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/error"));
    }

    @Test
    @WithMockUser
    void testShowAccount_LoggedInAccountPage() throws Exception {
        when(accountService.existsAccountById(1)).thenReturn(true);
        when(accountService.isAdmin(1)).thenReturn(true);
        when(accountService.isFollower(1, 1)).thenReturn(false);
        when(accountService.isFriend(1, 1)).thenReturn(false);
        Post post1 = new Post();
        post1.setMessage("123");
        Account author = new Account();
        author.setId(1);
        post1.setAuthor(author);
        post1.setDate(now(UTC));
        List<Post> posts = new ArrayList<>();
        posts.add(post1);
        when(postService.getAccountWallPosts(1, 1, 5)).thenReturn(posts);
        when(postService.getNumberAccountPostPages(1, 5)).thenReturn(1);
        mockMvc.perform(get("/account/1").sessionAttr("loggedInAccountId", 1)
                                         .sessionAttr("timeZone", getTimeZone("Europe/Moscow")))
               .andExpect(status().isOk())
               .andExpect(view().name("accountView"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/accountView.jsp"))
               .andExpect(model().attribute("admin", true))
               .andExpect(model().attribute("ownPage", true))
               .andExpect(model().attribute("follower", false))
               .andExpect(model().attribute("friend", false))
               .andExpect(model().attribute("posts", hasItem(allOf(hasProperty("message", is("123"))))))
               .andExpect(model().attribute("numberPages", 1));
    }

    @Test
    @WithMockUser
    void testShowAccount() throws Exception {
        when(accountService.existsAccountById(1)).thenReturn(true);
        when(accountService.isAdmin(1)).thenReturn(false);
        when(accountService.isFollower(1, 1)).thenReturn(false);
        when(accountService.isFriend(2, 1)).thenReturn(true);
        Post post1 = new Post();
        post1.setMessage("123");
        Account author = new Account();
        author.setId(1);
        post1.setAuthor(author);
        post1.setDate(now(UTC));
        List<Post> posts = new ArrayList<>();
        posts.add(post1);
        when(postService.getAccountWallPosts(1, 1, 5)).thenReturn(posts);
        when(postService.getNumberAccountPostPages(1, 5)).thenReturn(1);
        mockMvc.perform(get("/account/1").sessionAttr("loggedInAccountId", 2)
                                         .sessionAttr("timeZone", getTimeZone("Europe/Moscow")))
               .andExpect(status().isOk())
               .andExpect(view().name("accountView"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/accountView.jsp"))
               .andExpect(model().attribute("admin", false))
               .andExpect(model().attribute("ownPage", false))
               .andExpect(model().attribute("follower", false))
               .andExpect(model().attribute("friend", true))
               .andExpect(model().attribute("posts", hasItem(allOf(hasProperty("message", is("123"))))))
               .andExpect(model().attribute("numberPages", 1));
    }

    @Test
    @WithAnonymousUser
    void testShowEditAccountPage_AnonymousAccount() throws Exception {
        mockMvc.perform(get("/editAccount/1"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testShowEditAccountPage_AccountNotExists() throws Exception {
        when(accountService.existsAccountById(1)).thenReturn(false);
        mockMvc.perform(get("/editAccount/1").sessionAttr("loggedInAccountId", 2))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/error"));
    }

    @Test
    @WithMockUser
    void testShowEditAccountPage_LoggedInAccountIsNotAdmin() throws Exception {
        when(accountService.existsAccountById(1)).thenReturn(true);
        when(accountService.isAdmin(3)).thenReturn(false);
        mockMvc.perform(get("/editAccount/1").sessionAttr("loggedInAccountId", 3))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/3"));
    }

    @Test
    @WithMockUser
    void testShowEditAccountPage_LoggedInAccountIsAdmin() throws Exception {
        when(accountService.existsAccountById(1)).thenReturn(true);
        when(accountService.isAdmin(3)).thenReturn(true);
        Account account = new Account();
        account.setId(1);
        Phone phone = new Phone();
        phone.setId(1);
        phone.setAccount(account);
        phone.setNumber("12345");
        account.setPhones(singletonList(phone));
        when(accountService.getAccountWithPhones(1)).thenReturn(of(account));
        mockMvc.perform(get("/editAccount/1").sessionAttr("loggedInAccountId", 3))
               .andExpect(status().isOk())
               .andExpect(view().name("accountEdit"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/accountEdit.jsp"))
               .andExpect(model().attribute("account", account))
               .andExpect(model().attribute("ownPage", false))
               .andExpect(model().attribute("admin", true));
    }

    @Test
    @WithMockUser
    void testShowEditAccountPage_OwnPage() throws Exception {
        when(accountService.existsAccountById(1)).thenReturn(true);
        when(accountService.isAdmin(1)).thenReturn(false);
        Account account = new Account();
        account.setId(1);
        Phone phone = new Phone();
        phone.setId(1);
        phone.setAccount(account);
        phone.setNumber("12345");
        account.setPhones(singletonList(phone));
        when(accountService.getAccountWithPhones(1)).thenReturn(of(account));
        mockMvc.perform(get("/editAccount/1").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(view().name("accountEdit"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/accountEdit.jsp"))
               .andExpect(model().attribute("account", account))
               .andExpect(model().attribute("ownPage", true))
               .andExpect(model().attribute("admin", false));
    }

    @Test
    @WithAnonymousUser
    void testEditAccount_AnonymousAccount() throws Exception {
        mockMvc.perform(post("/editAccount"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testEditAccount_LoggedInAccountIsNotAdmin() throws Exception {
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        Phone phone = new Phone();
        phone.setId(1);
        phone.setAccount(account);
        phone.setNumber("12345");
        account.setPhones(singletonList(phone));
        when(accountService.isAdmin(1)).thenReturn(false);
        doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            account.setFirstName(((AccountDto) args[0]).getFirstName());
            return null;
        }).when(accountService).updateAccount(ArgumentMatchers.any(AccountDto.class));
        mockMvc.perform(post("/editAccount").param("firstName", "Petr")
                                            .param("phones[0]", "12345")
                                            .param("accountId", "2")
                                            .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/2"));
        assertEquals("Ivan", account.getFirstName());
    }

    @Test
    @WithMockUser
    void testEditAccount_LoggedInAccountIsAdmin() throws Exception {
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        Phone phone = new Phone();
        phone.setId(1);
        phone.setAccount(account);
        phone.setNumber("12345");
        account.setPhones(singletonList(phone));
        when(accountService.isAdmin(1)).thenReturn(true);
        doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            account.setFirstName(((AccountDto) args[0]).getFirstName());
            return null;
        }).when(accountService).updateAccount(ArgumentMatchers.any(AccountDto.class));
        mockMvc.perform(post("/editAccount").param("firstName", "Petr")
                                            .param("phones[0]", "12345")
                                            .param("accountId", "2")
                                            .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/2"));
        assertEquals("Petr", account.getFirstName());
    }

    @Test
    @WithMockUser
    void testEditAccount_OwnPage() throws Exception {
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        Phone phone = new Phone();
        phone.setId(1);
        phone.setAccount(account);
        phone.setNumber("12345");
        account.setPhones(singletonList(phone));
        when(accountService.isAdmin(2)).thenReturn(false);
        doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            account.setFirstName(((AccountDto) args[0]).getFirstName());
            return null;
        }).when(accountService).updateAccount(ArgumentMatchers.any(AccountDto.class));
        mockMvc.perform(post("/editAccount").param("firstName", "Petr")
                                            .param("phones[0]", "12345")
                                            .param("accountId", "2")
                                            .sessionAttr("loggedInAccountId", 2))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/2"));
        assertEquals("Petr", account.getFirstName());
    }

    @Test
    @WithAnonymousUser
    void testShowChangeAvatarPage_AnonymousAccount() throws Exception {
        mockMvc.perform(get("/changeAccountAvatar/1"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    @WithMockUser
    void testShowChangeAvatarPage_AccountNotExists() throws Exception {
        when(accountService.existsAccountById(1)).thenReturn(false);
        mockMvc.perform(get("/changeAccountAvatar/1").sessionAttr("loggedInAccountId", 2))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/error"));
    }

    @Test
    @WithMockUser
    void testShowChangeAvatarPage_LoggedInAccountIsNotAdmin() throws Exception {
        when(accountService.existsAccountById(1)).thenReturn(true);
        when(accountService.isAdmin(3)).thenReturn(false);
        mockMvc.perform(get("/changeAccountAvatar/1").sessionAttr("loggedInAccountId", 3))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/1"));
    }

    @Test
    @WithMockUser
    void testShowChangeAvatarPage_LoggedInAccountIsAdmin() throws Exception {
        when(accountService.existsAccountById(1)).thenReturn(true);
        when(accountService.isAdmin(3)).thenReturn(true);
        Account account = new Account();
        account.setId(1);
        Phone phone = new Phone();
        phone.setId(1);
        phone.setAccount(account);
        phone.setNumber("12345");
        account.setPhones(singletonList(phone));
        account.setPhotoId(1);
        when(accountService.getAccount(1)).thenReturn(of(account));
        mockMvc.perform(get("/changeAccountAvatar/1").sessionAttr("loggedInAccountId", 3))
               .andExpect(status().isOk())
               .andExpect(view().name("accountAvatarChange"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/accountAvatarChange.jsp"))
               .andExpect(model().attribute("accountId", 1L))
               .andExpect(model().attribute("accountPhotoId", 1L));
    }

    @Test
    @WithMockUser
    void testShowChangeAvatarPage_OwnPage() throws Exception {
        when(accountService.existsAccountById(1)).thenReturn(true);
        when(accountService.isAdmin(1)).thenReturn(false);
        Account account = new Account();
        account.setId(1);
        Phone phone = new Phone();
        phone.setId(1);
        phone.setAccount(account);
        phone.setNumber("12345");
        account.setPhones(singletonList(phone));
        account.setPhotoId(1);
        when(accountService.getAccount(1)).thenReturn(of(account));
        mockMvc.perform(get("/changeAccountAvatar/1").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(view().name("accountAvatarChange"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/accountAvatarChange.jsp"))
               .andExpect(model().attribute("accountId", 1L))
               .andExpect(model().attribute("accountPhotoId", 1L));
    }

    @Test
    @WithAnonymousUser
    void testShowChangePasswordPage_AnonymousAccount() throws Exception {
        mockMvc.perform(get("/changePassword/1"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    @WithMockUser
    void testShowChangePasswordPage_AccountNotExists() throws Exception {
        when(accountService.existsAccountById(1)).thenReturn(false);
        mockMvc.perform(get("/changePassword/1").sessionAttr("loggedInAccountId", 2))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/error"));
    }

    @Test
    @WithMockUser
    void testShowChangePasswordPage_NotOwnPage() throws Exception {
        when(accountService.existsAccountById(1)).thenReturn(true);
        mockMvc.perform(get("/changeAccountAvatar/1").sessionAttr("loggedInAccountId", 3))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/1"));
    }

    @Test
    @WithMockUser
    void testShowChangePasswordPage_OwnPage_WithOptionalRequestParams() throws Exception {
        when(accountService.existsAccountById(1)).thenReturn(true);
        mockMvc.perform(get("/changePassword/1").param("wrongPassword", "true")
                                                .param("message", "message")
                                                .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(view().name("passwordChange"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/passwordChange.jsp"))
               .andExpect(model().attribute("accountId", 1L))
               .andExpect(model().attribute("wrongPassword", true))
               .andExpect(model().attribute("message", "message"));
    }

    @Test
    @WithAnonymousUser
    void testChangePassword_AnonymousAccount() throws Exception {
        mockMvc.perform(post("/changePassword"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testChangePassword_NowtOwnPage() throws Exception {
        mockMvc.perform(post("/changePassword/").param("accountId", "1")
                                                .param("oldPassword", "1111")
                                                .param("newPassword", "2222")
                                                .param("repeatedPassword", "2222")
                                                .sessionAttr("loggedInAccountId", 3))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/1"));
    }

    @Test
    @WithMockUser
    void testChangePassword_IncorrectRepeatedAccount() throws Exception {
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        Phone phone = new Phone();
        phone.setId(1);
        phone.setAccount(account);
        phone.setNumber("12345");
        account.setPhones(singletonList(phone));
        account.setPassword("1111");
        when(accountService.getAccount(1)).thenReturn(of(account));
        when(passwordEncoder.matches("1111", "1111")).thenReturn(true);
        when(passwordEncoder.encode("1111")).thenReturn("1111");
        mockMvc.perform(post("/changePassword/").param("accountId", "1")
                                                .param("oldPassword", "1111")
                                                .param("newPassword", "2222")
                                                .param("repeatedPassword", "2221")
                                                .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/changePassword/1?wrongPassword=true&message=Repeated password is incorrect"));
        assertEquals("1111", account.getPassword());
    }

    @Test
    @WithMockUser
    void testChangePassword_IncorrectOldPassword() throws Exception {
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        Phone phone = new Phone();
        phone.setId(1);
        phone.setAccount(account);
        phone.setNumber("12345");
        account.setPhones(singletonList(phone));
        account.setPassword("1111");
        when(accountService.getAccount(1)).thenReturn(of(account));
        when(passwordEncoder.matches("1111", "1111")).thenReturn(true);
        when(passwordEncoder.encode("1111")).thenReturn("1111");
        mockMvc.perform(post("/changePassword/").param("accountId", "1")
                                                .param("oldPassword", "1112")
                                                .param("newPassword", "2222")
                                                .param("repeatedPassword", "2222")
                                                .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/changePassword/1?wrongPassword=true&message=The old password is incorrect"));
        assertEquals("1111", account.getPassword());
    }

    @Test
    @WithMockUser
    void testChangePassword() throws Exception {
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        Phone phone = new Phone();
        phone.setId(1);
        phone.setAccount(account);
        phone.setNumber("12345");
        account.setPhones(singletonList(phone));
        account.setPassword("1111");
        when(accountService.getAccount(1)).thenReturn(of(account));
        when(passwordEncoder.matches("1111", "1111")).thenReturn(true);
        when(passwordEncoder.encode("2222")).thenReturn("2222");
        mockMvc.perform(post("/changePassword/").param("accountId", "1")
                                                .param("oldPassword", "1111")
                                                .param("newPassword", "2222")
                                                .param("repeatedPassword", "2222")
                                                .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/1"));
        assertEquals("2222", account.getPassword());
    }

    @Test
    @WithAnonymousUser
    void testMakeAdmin_AnonymousAccount() throws Exception {
        mockMvc.perform(post("/account/makeAdmin"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    @WithMockUser
    void testMakeAdmin_LoggedInUserIsNotAdmin() throws Exception {
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        Phone phone = new Phone();
        phone.setId(1);
        phone.setAccount(account);
        phone.setNumber("12345");
        account.setPhones(singletonList(phone));
        account.setPassword("1111");
        account.setAdmin(false);
        when(accountService.isAdmin(2)).thenReturn(account.isAdmin());
        doAnswer(invocation -> {
            account.setAdmin(true);
            return null;
        }).when(accountService).makeAdmin(2);
        mockMvc.perform(post("/account/makeAdmin").param("accountToMakeAdmin", "2").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isForbidden());
        assertFalse(account.isAdmin());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    void testMakeAdmin_LoggedInUserIsAdmin_AccountIsAlreadyAdmin() throws Exception {
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        Phone phone = new Phone();
        phone.setId(1);
        phone.setAccount(account);
        phone.setNumber("12345");
        account.setPhones(singletonList(phone));
        account.setPassword("1111");
        account.setAdmin(true);
        when(accountService.isAdmin(2)).thenReturn(account.isAdmin());
        doAnswer(invocation -> {
            account.setAdmin(true);
            return null;
        }).when(accountService).makeAdmin(2);
        mockMvc.perform(post("/account/makeAdmin").param("accountToMakeAdmin", "2").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/2"));
        assertTrue(account.isAdmin());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    void testMakeAdmin_LoggedInUserIsAdmin_AccountIsNotAdmin() throws Exception {
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        Phone phone = new Phone();
        phone.setId(1);
        phone.setAccount(account);
        phone.setNumber("12345");
        account.setPhones(singletonList(phone));
        account.setPassword("1111");
        account.setAdmin(false);
        when(accountService.isAdmin(2)).thenReturn(account.isAdmin());
        doAnswer(invocation -> {
            account.setAdmin(true);
            return null;
        }).when(accountService).makeAdmin(2);
        mockMvc.perform(post("/account/makeAdmin").param("accountToMakeAdmin", "2").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/2"));
        assertTrue(account.isAdmin());
    }

    @Test
    @WithAnonymousUser
    void testDeleteAccount_AnonymousAccount() throws Exception {
        mockMvc.perform(post("/account/delete"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testDeleteAccount_LoggedInUserIsNotAdmin() throws Exception {
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        Phone phone = new Phone();
        phone.setId(1);
        phone.setAccount(account);
        phone.setNumber("12345");
        account.setPhones(singletonList(phone));
        account.setPassword("1111");
        account.setAdmin(false);
        List<Account> accounts = new ArrayList<>();
        accounts.add(account);
        when(accountService.isAdmin(2)).thenReturn(account.isAdmin());
        doAnswer(invocation -> {
            accounts.remove(0);
            return null;
        }).when(accountService).deleteAccount(2);
        mockMvc.perform(post("/account/delete").param("accountToBeDeleted", "2").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isForbidden());
        assertEquals(1, accounts.size());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    void testDeleteAccount_LoggedInUserIsAdmin() throws Exception {
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        Phone phone = new Phone();
        phone.setId(1);
        phone.setAccount(account);
        phone.setNumber("12345");
        account.setPhones(singletonList(phone));
        account.setPassword("1111");
        account.setAdmin(false);
        List<Account> accounts = new ArrayList<>();
        accounts.add(account);
        when(accountService.isAdmin(2)).thenReturn(account.isAdmin());
        doAnswer(invocation -> {
            accounts.remove(0);
            return null;
        }).when(accountService).deleteAccount(2);
        mockMvc.perform(post("/account/delete").param("accountToBeDelete", "2").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/1"));
        assertEquals(0, accounts.size());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    void testDeleteAccount_AccountAlreadyAdmin() throws Exception {
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        Phone phone = new Phone();
        phone.setId(1);
        phone.setAccount(account);
        phone.setNumber("12345");
        account.setPhones(singletonList(phone));
        account.setPassword("1111");
        account.setAdmin(true);
        List<Account> accounts = new ArrayList<>();
        accounts.add(account);
        when(accountService.isAdmin(2)).thenReturn(account.isAdmin());
        doAnswer(invocation -> {
            accounts.remove(0);
            return null;
        }).when(accountService).deleteAccount(2);
        mockMvc.perform(post("/account/delete").param("accountToBeDelete", "2").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/editAccount/2"));
        assertEquals(1, accounts.size());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    void testDeleteOwnAccount() throws Exception {
        Account account = new Account();
        account.setId(2);
        account.setFirstName("Ivan");
        Phone phone = new Phone();
        phone.setId(1);
        phone.setAccount(account);
        phone.setNumber("12345");
        account.setPhones(singletonList(phone));
        account.setPassword("1111");
        account.setAdmin(true);
        List<Account> accounts = new ArrayList<>();
        accounts.add(account);
        when(accountService.isAdmin(2)).thenReturn(account.isAdmin());
        doAnswer(invocation -> {
            accounts.remove(0);
            return null;
        }).when(accountService).deleteAccount(2);
        mockMvc.perform(post("/account/delete").param("accountToBeDelete", "2").sessionAttr("loggedInAccountId", 2))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/logout"));
        assertEquals(0, accounts.size());
    }

    @Test
    void testEmailExists() throws Exception {
        when(accountService.existsAccountByEmail("email@mail.ru")).thenReturn(true);
        mockMvc.perform(get("/emailCheck").param("parameter", "email@mail.ru"))
               .andExpect(status().isOk())
               .andExpect(content().string("true"));
    }

    @Test
    void testEmailNotExists() throws Exception {
        when(accountService.existsAccountByEmail("email@mail.ru")).thenReturn(false);
        mockMvc.perform(get("/emailCheck").param("parameter", "email@mail.ru"))
               .andExpect(status().isOk())
               .andExpect(content().string("false"));
    }

    @Test
    void testPhoneExists() throws Exception {
        when(accountService.existsPhoneByNumber("12345")).thenReturn(true);
        mockMvc.perform(get("/phoneCheck").param("parameter", "12345"))
               .andExpect(status().isOk())
               .andExpect(content().string("true"));
    }

    @Test
    void testPhoneNotExists() throws Exception {
        when(accountService.existsPhoneByNumber("email@mail.ru")).thenReturn(false);
        mockMvc.perform(get("/phoneCheck").param("parameter", "email@mail.ru"))
               .andExpect(status().isOk())
               .andExpect(content().string("false"));
    }

    @Test
    void testIcqExists() throws Exception {
        when(accountService.existsAccountByIcq("12345")).thenReturn(true);
        mockMvc.perform(get("/icqCheck").param("parameter", "12345"))
               .andExpect(status().isOk())
               .andExpect(content().string("true"));
    }

    @Test
    void testIcqNotExists() throws Exception {
        when(accountService.existsAccountByIcq("12345")).thenReturn(false);
        mockMvc.perform(get("/icqCheck").param("parameter", "12345"))
               .andExpect(status().isOk())
               .andExpect(content().string("false"));
    }

    @Test
    void testSkypeExists() throws Exception {
        when(accountService.existsAccountBySkype("skype")).thenReturn(true);
        mockMvc.perform(get("/skypeCheck").param("parameter", "skype"))
               .andExpect(status().isOk())
               .andExpect(content().string("true"));
    }

    @Test
    void testSkypeNotExists() throws Exception {
        when(accountService.existsAccountBySkype("skype")).thenReturn(false);
        mockMvc.perform(get("/skypeCheck").param("parameter", "skype"))
               .andExpect(status().isOk())
               .andExpect(content().string("false"));
    }

    @Test
    @WithMockUser
    void testAccountExistsByEmail_EmailExists_EmailOfThisAccount() throws Exception {
        when(accountService.existsAccountByEmail("email@mail.ru")).thenReturn(true);
        when(accountService.existsAccountByIdAndEmail(1, "email@mail.ru")).thenReturn(true);
        mockMvc.perform(get("/updatedEmailCheck").param("parameter", "email@mail.ru").param("accountId", "1"))
               .andExpect(status().isOk())
               .andExpect(content().string("false"));
    }

    @Test
    @WithMockUser
    void testAccountExistsByEmail_EmailExists_EmailOfAnotherAccount() throws Exception {
        when(accountService.existsAccountByEmail("email@mail.ru")).thenReturn(true);
        when(accountService.existsAccountByIdAndEmail(1, "email@mail.ru")).thenReturn(false);
        mockMvc.perform(get("/updatedEmailCheck").param("parameter", "email@mail.ru").param("accountId", "1"))
               .andExpect(status().isOk())
               .andExpect(content().string("true"));
    }

    @Test
    @WithMockUser
    void testAccountExistsByEmail_EmailNotExists() throws Exception {
        when(accountService.existsAccountByEmail("email@mail.ru")).thenReturn(false);
        when(accountService.existsAccountByIdAndEmail(1, "email@mail.ru")).thenReturn(false);
        mockMvc.perform(get("/updatedEmailCheck").param("parameter", "email@mail.ru").param("accountId", "1"))
               .andExpect(status().isOk())
               .andExpect(content().string("false"));
    }

    @Test
    @WithMockUser
    void testPhoneExists_PhoneExists_PhoneOfThisAccount() throws Exception {
        when(accountService.existsPhoneByNumber("12345")).thenReturn(true);
        when(accountService.existsPhoneByAccountIdAndNumber(1, "12345")).thenReturn(true);
        mockMvc.perform(get("/newPhoneCheck").param("parameter", "12345").param("accountId", "1"))
               .andExpect(status().isOk())
               .andExpect(content().string("false"));
    }

    @Test
    @WithMockUser
    void testPhoneExists_PhoneExists_PhoneOfAnotherAccount() throws Exception {
        when(accountService.existsPhoneByNumber("12345")).thenReturn(true);
        when(accountService.existsPhoneByAccountIdAndNumber(1, "12345")).thenReturn(false);
        mockMvc.perform(get("/newPhoneCheck").param("parameter", "12345").param("accountId", "1"))
               .andExpect(status().isOk())
               .andExpect(content().string("true"));
    }

    @Test
    @WithMockUser
    void testPhoneExists_PhoneNotExists() throws Exception {
        when(accountService.existsPhoneByNumber("12345")).thenReturn(false);
        when(accountService.existsPhoneByAccountIdAndNumber(1, "12345")).thenReturn(false);
        mockMvc.perform(get("/newPhoneCheck").param("parameter", "12345").param("accountId", "1"))
               .andExpect(status().isOk())
               .andExpect(content().string("false"));
    }

    @Test
    @WithMockUser
    void testAccountExistsByIcq_IcqExists_IcqOfThisAccount() throws Exception {
        when(accountService.existsAccountByIcq("icq")).thenReturn(true);
        when(accountService.existsAccountByIdAndIcq(1, "icq")).thenReturn(true);
        mockMvc.perform(get("/updatedIcqCheck").param("parameter", "icq").param("accountId", "1"))
               .andExpect(status().isOk())
               .andExpect(content().string("false"));
    }

    @Test
    @WithMockUser
    void testAccountExistsByIcq_IcqExists_IcqOfAnotherAccount() throws Exception {
        when(accountService.existsAccountByIcq("icq")).thenReturn(true);
        when(accountService.existsAccountByIdAndIcq(1, "icq")).thenReturn(false);
        mockMvc.perform(get("/updatedIcqCheck").param("parameter", "icq").param("accountId", "1"))
               .andExpect(status().isOk())
               .andExpect(content().string("true"));
    }

    @Test
    @WithMockUser
    void testAccountExistsByIcq_IcqNotExists() throws Exception {
        when(accountService.existsAccountByIcq("icq")).thenReturn(false);
        when(accountService.existsAccountByIdAndIcq(1, "icq")).thenReturn(false);
        mockMvc.perform(get("/updatedIcqCheck").param("parameter", "icq").param("accountId", "1"))
               .andExpect(status().isOk())
               .andExpect(content().string("false"));
    }

    @Test
    @WithMockUser
    void testAccountExistsBySkype_SkypeExists_SkypeOfThisAccount() throws Exception {
        when(accountService.existsAccountBySkype("skype")).thenReturn(true);
        when(accountService.existsAccountByIdAndSkype(1, "skype")).thenReturn(true);
        mockMvc.perform(get("/updatedSkypeCheck").param("parameter", "skype").param("accountId", "1"))
               .andExpect(status().isOk())
               .andExpect(content().string("false"));
    }

    @Test
    @WithMockUser
    void testAccountExistsBySkype_SkypeExists_SkypeOfAnotherAccount() throws Exception {
        when(accountService.existsAccountBySkype("skype")).thenReturn(true);
        when(accountService.existsAccountByIdAndSkype(1, "skype")).thenReturn(false);
        mockMvc.perform(get("/updatedSkypeCheck").param("parameter", "skype").param("accountId", "1"))
               .andExpect(status().isOk())
               .andExpect(content().string("true"));
    }

    @Test
    @WithMockUser
    void testAccountExistsBySkype_SkypeNotExists() throws Exception {
        when(accountService.existsAccountBySkype("skype")).thenReturn(false);
        when(accountService.existsAccountByIdAndSkype(1, "skype")).thenReturn(false);
        mockMvc.perform(get("/updatedSkypeCheck").param("parameter", "skype").param("accountId", "1"))
               .andExpect(status().isOk())
               .andExpect(content().string("false"));
    }

}