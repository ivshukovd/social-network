package com.getjavajob.training.ivshukovd.socialnetwork.web.controllers;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.service.AccountService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static java.util.Optional.of;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest(LoginController.class)
class LoginControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private AccountService accountService;

    @Test
    @WithAnonymousUser
    void testLogin() throws Exception {
        mockMvc.perform(get("/login"))
               .andExpect(status().isOk())
               .andExpect(view().name("login"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/login.jsp"))
               .andExpect(model().attribute("error", false));
    }

    @Test
    @WithAnonymousUser
    void testLogin_WrongEmailOrPassword() throws Exception {
        mockMvc.perform(get("/login?error=true"))
               .andExpect(status().isOk())
               .andExpect(view().name("login"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/login.jsp"))
               .andExpect(model().attribute("error", true));
    }

    @Test
    @WithMockUser
    void testLogin_AuthorizedUser() throws Exception {
        mockMvc.perform(get("/login")).andExpect(status().is4xxClientError());
    }

    @Test
    @WithAnonymousUser
    void testAfterSuccessLogin_AnonymousUser() throws Exception {
        mockMvc.perform(post("/afterSuccessLogin"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    @WithMockUser
    void testAfterSuccessLogin() throws Exception {
        mockMvc.perform(post("/afterSuccessLogin").param("email", "email@mail.ru"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/setLoggedInAccountId"));
    }

    @Test
    @WithAnonymousUser
    void setGlobalId_AnonymousUser() throws Exception {
        mockMvc.perform(get("/setLoggedInAccountId"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    @WithMockUser
    void setGlobalId() throws Exception {
        Account account = new Account();
        account.setId(1);
        when(accountService.getAccountByEmail("user")).thenReturn(of(account));
        mockMvc.perform(get("/setLoggedInAccountId"))
               .andExpect(status().isOk())
               .andExpect(view().name("clientTimeZone"));
    }

    @Test
    @WithMockUser
    void setGlobalId_EmptyAccountOptional() throws Exception {
        when(accountService.getAccountByEmail(ArgumentMatchers.any(String.class))).thenReturn(Optional.empty());
        mockMvc.perform(get("/setLoggedInAccountId"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/login"));
    }

    @Test
    @WithAnonymousUser
    void setClientTimeZone_AnonymousUser() throws Exception {
        mockMvc.perform(post("/setClientTimeZone").param("timeZoneId", "Europe/Moscow"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    @WithMockUser
    void setClientTimeZone() throws Exception {
        mockMvc.perform(post("/setClientTimeZone").param("timeZoneId", "Europe/Moscow"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/0"));
    }

}