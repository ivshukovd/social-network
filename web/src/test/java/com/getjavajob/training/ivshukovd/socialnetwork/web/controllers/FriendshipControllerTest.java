package com.getjavajob.training.ivshukovd.socialnetwork.web.controllers;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Phone;
import com.getjavajob.training.ivshukovd.socialnetwork.service.AccountService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest(FriendshipController.class)
class FriendshipControllerTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    private AccountService accountService;

    @Test
    @WithAnonymousUser
    void testShowFriendList_AnonymousUser() throws Exception {
        mockMvc.perform(get("/friends"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testShowFriendList() throws Exception {
        when(accountService.getNumberOfPagesInFriendList(1, 5)).thenReturn(1);
        List<Account> accounts = new ArrayList<>();
        Account account = new Account();
        account.setId(2);
        account.setPhones(singletonList(new Phone()));
        accounts.add(account);
        when(accountService.getFriends(1, 1, 5)).thenReturn(accounts);
        mockMvc.perform(get("/friends").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(view().name("friendList"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/friendList.jsp"))
               .andExpect(model().attribute("accounts", equalTo(accounts)))
               .andExpect(model().attribute("page", 1))
               .andExpect(model().attribute("numberPages", 1));

    }

    @Test
    @WithAnonymousUser
    void testShowFriendRequestList_AnonymousUser() throws Exception {
        mockMvc.perform(get("/friendRequests"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testShowFriendRequestList() throws Exception {
        when(accountService.getNumberOfPagesInFollowerList(1, 5)).thenReturn(1);
        List<Account> accounts = new ArrayList<>();
        Account account = new Account();
        account.setId(2);
        account.setPhones(singletonList(new Phone()));
        accounts.add(account);
        when(accountService.getFollowers(1, 1, 5)).thenReturn(accounts);
        mockMvc.perform(get("/friendRequests").sessionAttr("loggedInAccountId", 1))
               .andExpect(status().isOk())
               .andExpect(view().name("friendRequests"))
               .andExpect(forwardedUrl("/WEB-INF/jsp/friendRequests.jsp"))
               .andExpect(model().attribute("accounts", equalTo(accounts)))
               .andExpect(model().attribute("page", 1))
               .andExpect(model().attribute("numberPages", 1));

    }

    @Test
    @WithAnonymousUser
    void testAddFriend_AnonymousUser() throws Exception {
        mockMvc.perform(post("/addFriend"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testAddFriend() throws Exception {
        final int[] relationships = {0};
        doAnswer(invocation -> {
            relationships[0] = 1;
            return null;
        }).when(accountService).addRelationship(1, 2);
        mockMvc.perform(post("/addFriend").param("accountIdToBeFollow", "2")
                                          .param("currentPageURL", "/account/2")
                                          .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/2"));
        assertEquals(1, relationships[0]);
    }

    @Test
    @WithAnonymousUser
    void testDeleteFriend_AnonymousUser() throws Exception {
        mockMvc.perform(post("/deleteFriend"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testDeleteFriend() throws Exception {
        final int[] relationships = {1};
        doAnswer(invocation -> {
            relationships[0] = 0;
            return null;
        }).when(accountService).deleteFriend(1, 2);
        mockMvc.perform(post("/deleteFriend").param("friendId", "2")
                                             .param("currentPageURL", "/account/2")
                                             .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/2"));
        assertEquals(0, relationships[0]);
    }

    @Test
    @WithAnonymousUser
    void testDeleteFriendRequest_AnonymousUser() throws Exception {
        mockMvc.perform(post("/deleteFriendRequest"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("http" + "://localhost/login"));
    }

    @Test
    @WithMockUser
    void testDeleteFriendRequest() throws Exception {
        final int[] relationships = {1};
        doAnswer(invocation -> {
            relationships[0] = 0;
            return null;
        }).when(accountService).deleteFollower(2, 1);
        mockMvc.perform(post("/deleteFriendRequest").param("followerId", "2")
                                                    .param("currentPageURL", "/account/2")
                                                    .sessionAttr("loggedInAccountId", 1))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/account/2"));
        assertEquals(0, relationships[0]);
    }

}