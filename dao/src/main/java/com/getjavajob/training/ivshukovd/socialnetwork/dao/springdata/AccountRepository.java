package com.getjavajob.training.ivshukovd.socialnetwork.dao.springdata;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.AccountDao;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.AccountShortDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long>, AccountDao {

    @Query("SELECT a FROM Account a LEFT JOIN FETCH a.phones WHERE a.id = ?1")
    Optional<Account> findAccountWithPhones(long id);

    @Query("SELECT new com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.AccountShortDto(a.id, a"
            + ".firstName, a.surName, a.photoId) FROM Account a WHERE a.id = ?1")
    Optional<AccountShortDto> findShortAccount(long id);


    @Query("SELECT new com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.AccountShortDto(a.id, a"
            + ".firstName, a.surName, a.photoId) FROM Account a WHERE LOWER(a.firstName) LIKE LOWER(CONCAT('%', ?1,'%')"
            + ") OR LOWER(a.surName) LIKE LOWER(CONCAT('%', ?1,'%'))")
    List<AccountShortDto> findBySubstring(String str, Pageable pageable);

    @Query("SELECT new com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.AccountShortDto(a.id, a"
            + ".firstName, a.surName, a.photoId) FROM Account a WHERE (LOWER(a.firstName) LIKE LOWER(CONCAT('%', ?1,"
            + "'%')) AND LOWER(a.surName) LIKE LOWER(CONCAT('%', ?2,'%'))) OR  (LOWER(a.surName) LIKE LOWER(CONCAT"
            + "('%', " + "?1,'%')) AND LOWER(a.firstName) LIKE LOWER(CONCAT('%', ?2,'%')))")
    List<AccountShortDto> findBySubstring(String str1, String str2, Pageable pageable);

    @Modifying
    @Query("update Account a SET a.photoId = ?1 WHERE a.id = ?2")
    void updatePhoto(long photoId, long id);

    @Query("SELECT a.admin FROM Account a WHERE a.id = ?1")
    boolean isAdmin(long id);

    @Modifying
    @Query("update Account a SET a.admin = true WHERE a.id = ?1")
    void makeAdmin(long id);

}