package com.getjavajob.training.ivshukovd.socialnetwork.dao;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Group;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupMember;
import org.springframework.data.domain.Pageable;

import java.util.List;

import static com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupMember.GroupMemberKey;

public interface GroupMemberDao {

    GroupMember save(GroupMember member);

    List<GroupMember> findAllByAccountIdAndAccountRole(long id, String role);

    List<Account> findGroupMembers(long id, Pageable pageable);

    List<Group> findAccountGroups(long id, Pageable pageable);

    String findAccountRole(GroupMemberKey id);

    Account findFirstByGroupIdAndAccountRole(long id, String role);

    long countByGroupId(long id);

    long countByMemberId(long id);

    boolean existsById(GroupMemberKey id);

    void makeCreator(long groupId, long accountId);

    void makeAdmin(long groupId, long accountId);

    void makeMember(long groupId, long accountId);

    void deleteById(GroupMemberKey id);

    void deleteAllByAccountId(long id);

}
