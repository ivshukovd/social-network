package com.getjavajob.training.ivshukovd.socialnetwork.dao;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Dialogue;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface DialogueDao extends Dao<Dialogue> {

    List<Dialogue> findByAccountIdOrderByLastUpdateDesc(long id, Pageable pageable);

    Optional<Dialogue> findByAccountsId(long accountId1, long accountId2);

    long countDialogPartners(long id);

    void deleteAllByAccountId(long id);

}