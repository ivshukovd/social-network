package com.getjavajob.training.ivshukovd.socialnetwork.dao.springdata;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.PhoneDao;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Phone;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneRepository extends JpaRepository<Phone, Long>, PhoneDao {

}