package com.getjavajob.training.ivshukovd.socialnetwork.dao.hibernate;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Friendship;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

//@Repository
public class FriendshipDaoImpl {

    @PersistenceContext
    private EntityManager em;

    //@Override
    public List<Account> findFriends(long id, Pageable pageable) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Friendship> criteriaQuery = criteriaBuilder.createQuery(Friendship.class);
        Root<Friendship> from = criteriaQuery.from(Friendship.class);
        Subquery<Long> subquery = criteriaQuery.subquery(Long.class);
        Root<Friendship> subqueryFrom = subquery.from(Friendship.class);
        subquery.select(subqueryFrom.get("accountFrom"))
                .where(criteriaBuilder.equal(subqueryFrom.get("accountTo"), id));
        CriteriaQuery<Friendship> select = criteriaQuery.select(from)
                                                        .where(criteriaBuilder.equal(from.get("accountFrom"), id),
                                                               criteriaBuilder.in(from.get("accountTo"))
                                                                              .value(subquery));
        List<Friendship> relationships = em.createQuery(select)
                                           .setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                                           .setMaxResults(pageable.getPageSize())
                                           .getResultList();
        return relationships.stream().map(Friendship::getAccountTo).collect(Collectors.toList());
    }

    //@Override
    public long countFriends(long id) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<Friendship> from = criteriaQuery.from(Friendship.class);
        Subquery<Long> subquery = criteriaQuery.subquery(Long.class);
        Root<Friendship> subqueryFrom = subquery.from(Friendship.class);
        subquery.select(subqueryFrom.get("accountFrom"))
                .where(criteriaBuilder.equal(subqueryFrom.get("accountTo"), id));
        CriteriaQuery<Long> select = criteriaQuery.select(criteriaBuilder.count(from))
                                                  .where(criteriaBuilder.equal(from.get("accountFrom"), id),
                                                         criteriaBuilder.in(from.get("accountTo")).value(subquery));
        return em.createQuery(select).getSingleResult();
    }

    //@Override
    public List<Account> findFollowers(long id, Pageable pageable) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Friendship> criteriaQuery = criteriaBuilder.createQuery(Friendship.class);
        Root<Friendship> from = criteriaQuery.from(Friendship.class);
        Subquery<Long> friends = criteriaQuery.subquery(Long.class);
        Root<Friendship> subqueryFrom = friends.from(Friendship.class);
        friends.select(subqueryFrom.get("accountTo")).where(criteriaBuilder.equal(subqueryFrom.get("accountFrom"), id));
        CriteriaQuery<Friendship> select = criteriaQuery.select(from)
                                                        .where(criteriaBuilder.equal(from.get("accountTo"), id),
                                                               criteriaBuilder.not(
                                                                       criteriaBuilder.in(from.get("accountFrom"))
                                                                                      .value(friends)));
        List<Friendship> relationships = em.createQuery(select)
                                           .setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                                           .setMaxResults(pageable.getPageSize())
                                           .getResultList();
        return relationships.stream().map(Friendship::getAccountFrom).collect(Collectors.toList());
    }

    //@Override
    public long countFollowers(long id) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<Friendship> from = criteriaQuery.from(Friendship.class);
        Subquery<Long> subquery = criteriaQuery.subquery(Long.class);
        Root<Friendship> subqueryFrom = subquery.from(Friendship.class);
        subquery.select(subqueryFrom.get("accountTo"))
                .where(criteriaBuilder.equal(subqueryFrom.get("accountFrom"), id));
        CriteriaQuery<Long> select = criteriaQuery.select(criteriaBuilder.count(from))
                                                  .where(criteriaBuilder.equal(from.get("accountTo"), id),
                                                         criteriaBuilder.not(criteriaBuilder.in(from.get("accountFrom"))
                                                                                            .value(subquery)));
        return em.createQuery(select).getSingleResult();
    }

    //@Override
    public boolean existsById(Friendship.FriendshipKey id) {
        return nonNull(em.find(Friendship.class, id));
    }

    //@Override
    public void deleteById(Friendship.FriendshipKey id) {
        em.remove(em.find(Friendship.class, id));
    }

}