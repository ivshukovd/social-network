package com.getjavajob.training.ivshukovd.socialnetwork.dao;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Phone;

import java.util.List;

public interface PhoneDao {

    Phone save(Phone phone);

    List<Phone> findByAccountId(long accountId);

    boolean existsByNumber(String number);

    boolean existsByAccountIdAndNumber(long accountId, String number);

    void delete(Phone phone);

    void deleteAllByAccountId(long accountId);

}