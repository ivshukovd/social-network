package com.getjavajob.training.ivshukovd.socialnetwork.dao;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Post;
import org.springframework.data.domain.Pageable;

import java.util.List;

import static com.getjavajob.training.ivshukovd.socialnetwork.domain.Post.Type;

public interface PostDao {

    Post save(Post post);

    List<Post> findByTargetIdAndTypeOrderByDateDesc(long targetId, Type type, Pageable pageable);

    int countByTargetIdAndType(long targetId, Type type);

    boolean existsByIdAndAuthorId(long id, long authorId);

    void deleteAllAccountPosts(long id);

    void deleteById(long id);

}