package com.getjavajob.training.ivshukovd.socialnetwork.dao.springdata;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.MessageDao;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Message;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long>, MessageDao {

    @Query("SELECT m FROM Message m WHERE m.dialogue.id = ?1 ORDER BY m.date DESC")
    List<Message> findByDialogueId(long id, Pageable pageable);

    @Query("SELECT COUNT(m) FROM Message m WHERE m.dialogue.id = ?1")
    long countByDialogueId(long id);

}