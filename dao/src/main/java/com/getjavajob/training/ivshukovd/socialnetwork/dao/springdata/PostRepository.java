package com.getjavajob.training.ivshukovd.socialnetwork.dao.springdata;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.PostDao;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface PostRepository extends JpaRepository<Post, Long>, PostDao {

    @Modifying
    @Query("DELETE FROM Post p WHERE p.author.id = ?1")
    void deleteAllAccountPosts(long id);

}