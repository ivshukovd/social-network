package com.getjavajob.training.ivshukovd.socialnetwork.dao.springdata;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.GroupDao;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Group;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.GroupDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface GroupRepository extends JpaRepository<Group, Long>, GroupDao {

    @Modifying
    @Query("update Group g SET g.photoId = ?1 WHERE g.id = ?2")
    void updatePhoto(long photoId, long id);

    @Query("SELECT new com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.GroupDto(g.id, g.name, g.photoId) "
            + "FROM Group g WHERE LOWER(g.name) LIKE LOWER(CONCAT('%', ?1,'%'))")
    List<GroupDto> findBySubstring(String str, Pageable pageable);

}