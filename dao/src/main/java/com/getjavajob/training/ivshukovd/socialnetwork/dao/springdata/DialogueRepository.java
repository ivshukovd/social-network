package com.getjavajob.training.ivshukovd.socialnetwork.dao.springdata;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.DialogueDao;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Dialogue;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface DialogueRepository extends JpaRepository<Dialogue, Long>, DialogueDao {

    @Query("SELECT d FROM Dialogue d WHERE d.account1.id = ?1 OR d.account2.id = ?1 ORDER BY d.lastUpdate DESC")
    List<Dialogue> findByAccountIdOrderByLastUpdateDesc(long id, Pageable pageable);

    @Query("SELECT d FROM Dialogue d WHERE d.account1.id = ?1 AND d.account2.id = ?2")
    Optional<Dialogue> findByAccountsId(long accountId1, long accountId2);

    @Query("SELECT COUNT(d) FROM Dialogue d WHERE d.account1.id = ?1 OR d.account2.id = ?1")
    long countDialogPartners(long id);

    @Modifying
    @Query("DELETE FROM Dialogue d WHERE d.account1.id = ?1 OR d.account2 = ?1")
    void deleteAllByAccountId(long id);

}