package com.getjavajob.training.ivshukovd.socialnetwork.dao;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Group;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.GroupDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface GroupDao extends Dao<Group> {

    List<GroupDto> findBySubstring(String str, Pageable pageable);

    long countByNameContainingIgnoreCase(String str);

    void updatePhoto(long photoId, long id);

}