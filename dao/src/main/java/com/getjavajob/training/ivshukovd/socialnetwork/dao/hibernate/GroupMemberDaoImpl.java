package com.getjavajob.training.ivshukovd.socialnetwork.dao.hibernate;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Group;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupJoinRequest;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupMember;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupMember.GroupMemberKey;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.stream.Collectors;

import static com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupJoinRequest.GroupJoinRequestKey;
import static java.util.Objects.nonNull;

//@Repository
public class GroupMemberDaoImpl {

    @PersistenceContext
    private EntityManager em;

    //@Override
    public List<Account> findGroupMembers(long id, Pageable pageable) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<GroupMember> criteriaQuery = criteriaBuilder.createQuery(GroupMember.class);
        Root<GroupMember> from = criteriaQuery.from(GroupMember.class);
        CriteriaQuery<GroupMember> select =
                criteriaQuery.select(from).where(criteriaBuilder.equal(from.get("group"), id));
        List<GroupMember> groupMembers = em.createQuery(select)
                                           .setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                                           .setMaxResults(pageable.getPageSize())
                                           .getResultList();
        return groupMembers.stream().map(GroupMember::getAccount).collect(Collectors.toList());
    }

    //    //@Override
    public List<Account> getJoinRequests(long groupId, int limit, int offset) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<GroupJoinRequest> criteriaQuery = criteriaBuilder.createQuery(GroupJoinRequest.class);
        Root<GroupJoinRequest> from = criteriaQuery.from(GroupJoinRequest.class);
        CriteriaQuery<GroupJoinRequest> select =
                criteriaQuery.select(from).where(criteriaBuilder.equal(from.get("group"), groupId));
        List<GroupJoinRequest> groupMembers =
                em.createQuery(select).setFirstResult(offset).setMaxResults(limit).getResultList();
        return groupMembers.stream().map(GroupJoinRequest::getAccount).collect(Collectors.toList());
    }

    //@Override
    public long countByGroupId(long groupId) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<GroupMember> from = criteriaQuery.from(GroupMember.class);
        CriteriaQuery<Long> select = criteriaQuery.select(criteriaBuilder.count(from))
                                                  .where(criteriaBuilder.equal(from.get("group"), groupId));
        return em.createQuery(select).getSingleResult();
    }

    //    //@Override
    public long countJoinRequests(long groupId) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<GroupJoinRequest> from = criteriaQuery.from(GroupJoinRequest.class);
        CriteriaQuery<Long> select = criteriaQuery.select(criteriaBuilder.count(from))
                                                  .where(criteriaBuilder.equal(from.get("group"), groupId));
        return em.createQuery(select).getSingleResult();
    }

    //@Override
    public GroupMember save(GroupMember member) {
        em.persist(member);
        return member;
    }

    //@Override
    public void deleteById(GroupMemberKey id) {
        GroupMember groupMember = em.find(GroupMember.class, id);
        em.remove(groupMember);
    }

    //    //@Override
    public void insertJoinRequest(Group group, Account account) {
        GroupJoinRequestKey key = new GroupJoinRequestKey(group.getId(), account.getId());
        GroupJoinRequest gjr = new GroupJoinRequest();
        gjr.setId(key);
        gjr.setGroup(group);
        gjr.setAccount(account);
        em.persist(gjr);
    }

    //    //@Override
    public void deleteJoinRequest(long groupId, long accountId) {
        GroupJoinRequest groupJoinRequest =
                em.find(GroupJoinRequest.class, new GroupJoinRequestKey(groupId, accountId));
        em.remove(groupJoinRequest);
    }

    //@Override
    public void makeAdmin(long groupId, long accountId) {
        GroupMember groupMember = em.find(GroupMember.class, new GroupMemberKey(groupId, accountId));
        groupMember.setAccountRole("admin");
        em.merge(groupMember);
    }

    //@Override
    public void makeMember(long groupId, long accountId) {
        GroupMember groupMember = em.find(GroupMember.class, new GroupMemberKey(groupId, accountId));
        groupMember.setAccountRole("member");
        em.merge(groupMember);
    }

    //@Override
    public String findAccountRole(GroupMemberKey id) {
        GroupMember gm = em.find(GroupMember.class, id);
        return gm.getAccountRole();
    }

    //@Override
    public boolean existsById(GroupMemberKey id) {
        GroupMember gm = em.find(GroupMember.class, id);
        return nonNull(gm);
    }

    //    //@Override
    public boolean isJoinRequest(long groupId, long accountId) {
        GroupJoinRequestKey key = new GroupJoinRequestKey(groupId, accountId);
        GroupJoinRequest gjr = em.find(GroupJoinRequest.class, key);
        return nonNull(gjr);
    }

    //@Override
    public List<Group> findAccountGroups(long accountId, Pageable pageable) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<GroupMember> criteriaQuery = criteriaBuilder.createQuery(GroupMember.class);
        Root<GroupMember> from = criteriaQuery.from(GroupMember.class);
        CriteriaQuery<GroupMember> select =
                criteriaQuery.select(from).where(criteriaBuilder.equal(from.get("account"), accountId));
        List<GroupMember> groupMembers = em.createQuery(select)
                                           .setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                                           .setMaxResults(pageable.getPageSize())
                                           .getResultList();
        return groupMembers.stream().map(GroupMember::getGroup).collect(Collectors.toList());
    }

    //@Override
    public long countByMemberId(long accountId) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<GroupMember> from = criteriaQuery.from(GroupMember.class);
        CriteriaQuery<Long> select = criteriaQuery.select(criteriaBuilder.count(from))
                                                  .where(criteriaBuilder.equal(from.get("account"), accountId));
        return em.createQuery(select).getSingleResult();
    }

}