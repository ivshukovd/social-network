package com.getjavajob.training.ivshukovd.socialnetwork.dao;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Friendship;
import org.springframework.data.domain.Pageable;

import java.util.List;

import static com.getjavajob.training.ivshukovd.socialnetwork.domain.Friendship.FriendshipKey;

public interface FriendshipDao extends Dao<Friendship> {

    List<Account> findFriends(long id, Pageable pageable);

    List<Account> findFollowers(long id, Pageable pageable);

    long countFriends(long id);

    long countFollowers(long id);

    boolean existsById(FriendshipKey id);

    void deleteById(FriendshipKey id);

    void deleteAllByAccountId(long id);

}