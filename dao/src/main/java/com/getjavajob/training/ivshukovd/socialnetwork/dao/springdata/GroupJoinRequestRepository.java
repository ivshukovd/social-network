package com.getjavajob.training.ivshukovd.socialnetwork.dao.springdata;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.GroupJoinRequestDao;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupJoinRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

import static com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupJoinRequest.GroupJoinRequestKey;

public interface GroupJoinRequestRepository
        extends JpaRepository<GroupJoinRequest, GroupJoinRequestKey>, GroupJoinRequestDao {

    @Query("SELECT gjr.account FROM GroupJoinRequest gjr WHERE gjr.group.id = ?1")
    List<Account> findByGroupId(long groupId, Pageable pageable);

    @Query("SELECT COUNT(gjr) FROM GroupJoinRequest gjr WHERE gjr.group.id = ?1")
    long countByGroupId(long groupId);

}