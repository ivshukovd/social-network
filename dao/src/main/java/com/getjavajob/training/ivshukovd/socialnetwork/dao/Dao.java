package com.getjavajob.training.ivshukovd.socialnetwork.dao;

import java.util.Optional;

public interface Dao<E> {

    E save(E entity);

    Optional<E> findById(long id);

    void delete(E entity);

}