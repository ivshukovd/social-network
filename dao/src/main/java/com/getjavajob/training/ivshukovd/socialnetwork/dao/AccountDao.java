package com.getjavajob.training.ivshukovd.socialnetwork.dao;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.AccountShortDto;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface AccountDao extends Dao<Account> {

    List<Account> findAll();

    Optional<Account> findByEmailAndPassword(String email, String password);

    Optional<Account> findByEmail(String email);

    Optional<Account> findAccountWithPhones(long id);

    Optional<AccountShortDto> findShortAccount(long id);

    List<AccountShortDto> findBySubstring(String str, Pageable pageable);

    List<AccountShortDto> findBySubstring(String str1, String str2, Pageable pageable);

    long countByFirstNameContainingOrSurNameContainingAllIgnoreCase(String str1, String str2);

    long countByFirstNameContainingAndSurNameContainingAllIgnoreCase(String str1, String str2);

    boolean existsById(long id);

    boolean existsByEmail(String email);

    boolean existsByIcq(String icq);

    boolean existsBySkype(String skype);

    boolean existsByIdAndEmail(long id, String email);

    boolean existsByIdAndIcq(long id, String icq);

    boolean existsByIdAndSkype(long id, String email);

    boolean isAdmin(long id);

    void updatePhoto(long photoId, long id);

    void makeAdmin(long id);

    void deleteById(long id);

}