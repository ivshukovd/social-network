package com.getjavajob.training.ivshukovd.socialnetwork.dao.hibernate;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Group;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.GroupDto;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.ofNullable;

//@Repository
public class GroupDaoImpl {

    @PersistenceContext
    private EntityManager em;

    //@Override
    public void updatePhoto(long photoId, long id) {
        Group group = em.find(Group.class, id);
        group.setPhotoId(photoId);
        em.merge(group);
    }

    //@Override
    public Group save(Group group) {
        em.persist(group);
        em.flush();
        return group;
    }

    //@Override
    public Optional<Group> findById(long id) {
        return ofNullable(em.find(Group.class, id));
    }

    //@Override
    public List<GroupDto> findBySubstring(String str, Pageable pageable) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<GroupDto> criteriaQuery = builder.createQuery(GroupDto.class);
        Root<Group> root = criteriaQuery.from(Group.class);
        Predicate predicate = builder.like(builder.lower(root.get("name")), "%" + str.toLowerCase() + "%");
        criteriaQuery.select(builder.construct(GroupDto.class, root.get("id"), root.get("name"), root.get("photo")));
        criteriaQuery.where(predicate);
        return em.createQuery(criteriaQuery)
                 .setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                 .setMaxResults(pageable.getPageSize())
                 .getResultList();
    }

    //    //@Override
    public void updateEntity(Group group) {
        em.merge(group);
    }

    //@Override
    public void delete(Group group) {
        em.remove(group);
    }

    //@Override
    public long countByNameContainingIgnoreCase(String str) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<Group> from = criteriaQuery.from(Group.class);
        Predicate predicate =
                criteriaBuilder.like(criteriaBuilder.lower(from.get("name")), "%" + str.toLowerCase() + "%");
        CriteriaQuery<Long> select = criteriaQuery.select(criteriaBuilder.count(from)).where(predicate);
        return em.createQuery(select).getSingleResult();
    }

}