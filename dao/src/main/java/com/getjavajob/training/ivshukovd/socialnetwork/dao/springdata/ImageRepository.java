package com.getjavajob.training.ivshukovd.socialnetwork.dao.springdata;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.ImageDao;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Image;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRepository extends JpaRepository<Image, Long>, ImageDao {

}