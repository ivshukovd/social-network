package com.getjavajob.training.ivshukovd.socialnetwork.dao.springdata;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.FriendshipDao;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Friendship;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

import static com.getjavajob.training.ivshukovd.socialnetwork.domain.Friendship.FriendshipKey;

public interface FriendshipRepository extends JpaRepository<Friendship, FriendshipKey>, FriendshipDao {

    @Query("SELECT f1.accountFrom FROM Friendship f1 WHERE f1.accountTo.id = ?1 AND EXISTS (SELECT f2 FROM "
            + "Friendship" + " f2 WHERE f1.accountTo = f2.accountFrom AND f1.accountFrom = f2.accountTo)")
    List<Account> findFriends(long id, Pageable pageable);

    @Query("SELECT f1.accountFrom FROM Friendship f1 WHERE f1.accountTo.id = ?1 AND NOT EXISTS (SELECT f2 FROM "
            + "Friendship f2 WHERE f1.accountTo = f2.accountFrom AND f1.accountFrom = f2.accountTo)")
    List<Account> findFollowers(long id, Pageable pageable);

    @Query("SELECT COUNT(f1) FROM Friendship f1 WHERE f1.accountTo.id = ?1 AND EXISTS (SELECT f2 FROM Friendship f2 "
            + "WHERE f1.accountTo = f2.accountFrom AND f1.accountFrom = f2.accountTo)")
    long countFriends(long id);

    @Query("SELECT COUNT(f1) FROM Friendship f1 WHERE f1.accountTo.id = ?1 AND NOT EXISTS (SELECT f2 FROM Friendship "
            + "f2 WHERE f1.accountTo = f2.accountFrom AND f1.accountFrom = f2.accountTo)")
    long countFollowers(long id);

    @Modifying
    @Query("DELETE FROM Friendship f WHERE f.accountFrom.id = ?1 OR f.accountTo.id = ?1")
    void deleteAllByAccountId(long id);

}