package com.getjavajob.training.ivshukovd.socialnetwork.dao.springdata;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.GroupMemberDao;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Group;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupMember;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

import static com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupMember.GroupMemberKey;

public interface GroupMemberRepository extends JpaRepository<GroupMember, GroupMemberKey>, GroupMemberDao {

    @Query("SELECT gm.account FROM GroupMember gm WHERE gm.group.id = ?1")
    List<Account> findGroupMembers(long id, Pageable pageable);

    @Query("SELECT gm.group FROM GroupMember gm WHERE gm.account.id = ?1")
    List<Group> findAccountGroups(long id, Pageable pageable);

    @Query("SELECT gm.account FROM GroupMember gm WHERE gm.group.id = ?1 AND gm.accountRole = ?2")
    Account findFirstByGroupIdAndAccountRole(long id, String role);

    @Query("SELECT gm.accountRole FROM GroupMember gm WHERE gm.id = ?1")
    String findAccountRole(GroupMemberKey id);

    @Modifying
    @Query("UPDATE GroupMember gm SET gm.accountRole = 'creator' WHERE gm.group.id = ?1 AND gm.account.id = ?2")
    void makeCreator(long groupId, long accountId);

    @Modifying
    @Query("UPDATE GroupMember gm SET gm.accountRole = 'admin' WHERE gm.group.id = ?1 AND gm.account.id = ?2")
    void makeAdmin(long groupId, long accountId);

    @Modifying
    @Query("UPDATE GroupMember gm SET gm.accountRole = 'member' WHERE gm.group.id = ?1 AND gm.account.id = ?2")
    void makeMember(long groupId, long accountId);

    @Query("SELECT COUNT(gm) FROM GroupMember gm WHERE gm.group.id = ?1")
    long countByGroupId(long id);

    @Query("SELECT COUNT(gm) FROM GroupMember gm WHERE gm.account.id = ?1")
    long countByMemberId(long id);

}