package com.getjavajob.training.ivshukovd.socialnetwork.dao.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "com.getjavajob.training.ivshukovd.socialnetwork.dao.springdata")
@EntityScan(basePackages = "com.getjavajob.training.ivshukovd.socialnetwork.domain")
public class DaoConfig {

}