package com.getjavajob.training.ivshukovd.socialnetwork.dao.hibernate;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Post;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

//@Repository
public class PostDaoImpl {

    @PersistenceContext
    private EntityManager em;

    //@Override
    public List<Post> findByTargetIdAndTypeOrderByDateDesc(long targetId, Post.Type type, Pageable pageable) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Post> criteriaQuery = criteriaBuilder.createQuery(Post.class);
        Root<Post> from = criteriaQuery.from(Post.class);
        CriteriaQuery<Post> select = criteriaQuery.select(from)
                                                  .where(criteriaBuilder.equal(from.get("targetId"), targetId),
                                                         criteriaBuilder.equal(from.get("type"), type));
        return em.createQuery(select)
                 .setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                 .setMaxResults(pageable.getPageSize())
                 .getResultList();
    }

    //@Override
    public Post save(Post post) {
        em.persist(post);
        return post;
    }

}