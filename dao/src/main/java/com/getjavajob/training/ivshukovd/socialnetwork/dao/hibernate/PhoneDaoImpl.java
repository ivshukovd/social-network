package com.getjavajob.training.ivshukovd.socialnetwork.dao.hibernate;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Phone;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

//@Repository
public class PhoneDaoImpl {

    @PersistenceContext
    private EntityManager em;

    //@Override
    public Phone save(Phone phone) {
        em.persist(phone);
        return phone;
    }

    //@Override
    public List<Phone> findByAccountId(long accountId) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Phone> criteriaQuery = criteriaBuilder.createQuery(Phone.class);
        Root<Phone> from = criteriaQuery.from(Phone.class);
        CriteriaQuery<Phone> select =
                criteriaQuery.select(from).where(criteriaBuilder.equal(from.get("account"), accountId));
        return em.createQuery(select).getResultList();
    }

    //@Override
    public void delete(Phone phone) {
        em.remove(phone);
    }

    //@Override
    public void deleteAllByAccountId(long accountId) {
        List<Phone> phones = findByAccountId(accountId);
        for (Phone phone : phones) {
            em.remove(phone);
        }
    }

}