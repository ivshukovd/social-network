package com.getjavajob.training.ivshukovd.socialnetwork.dao.hibernate;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Dialogue;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Message;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

//@Repository
public class MessageDaoImpl {

    @PersistenceContext
    private EntityManager em;

    //@Override
    public Message save(Message message) {
        em.persist(message);
        return message;
    }

    //@Override
    public List<Message> findByDialogueId(long id, Pageable pageable) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Message> criteriaQuery = criteriaBuilder.createQuery(Message.class);
        Root<Message> from = criteriaQuery.from(Message.class);
        CriteriaQuery<Message> select = criteriaQuery.select(from)
                                                     .where(criteriaBuilder.equal(from.get("dialogueId"), id))
                                                     .orderBy(criteriaBuilder.desc(from.get("date")));
        return em.createQuery(select)
                 .setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                 .setMaxResults(pageable.getPageSize())
                 .getResultList();
    }

    //@Override
    public long countByDialogueId(long id) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<Message> from = criteriaQuery.from(Message.class);
        CriteriaQuery<Long> select = criteriaQuery.select(criteriaBuilder.count(from))
                                                  .where(criteriaBuilder.equal(from.get("dialogueId"), id));
        return em.createQuery(select).getSingleResult();
    }

    //@Override
    public void deleteById(long id) {
        em.remove(em.find(Message.class, id));
    }

    //    //@Override
    public List<Long> getDialogPartnersId(long id, int limit, int offset) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Dialogue> criteriaQuery = criteriaBuilder.createQuery(Dialogue.class);
        Root<Dialogue> from = criteriaQuery.from(Dialogue.class);
        Predicate predicate1 = criteriaBuilder.equal(from.get("accountId1"), id);
        Predicate predicate2 = criteriaBuilder.equal(from.get("accountId2"), id);
        Predicate predicate = criteriaBuilder.or(predicate1, predicate2);
        CriteriaQuery<Dialogue> select =
                criteriaQuery.select((from)).where(predicate).orderBy(criteriaBuilder.desc(from.get("lastUpdate")));
        List<Dialogue> dialogues = em.createQuery(select).setFirstResult(offset).setMaxResults(limit).getResultList();
        List<Long> dialoguePartnersId = new ArrayList<>();
        for (Dialogue dialogue : dialogues) {
            if (dialogue.getAccount1().getId() == id) {
                dialoguePartnersId.add(dialogue.getAccount2().getId());
            } else {
                dialoguePartnersId.add(dialogue.getAccount1().getId());
            }
        }
        return dialoguePartnersId;
    }

    //    //@Override
    public long countDialogPartners(long id) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<Dialogue> from = criteriaQuery.from(Dialogue.class);
        Predicate predicate1 = criteriaBuilder.equal(from.get("accountId1"), id);
        Predicate predicate2 = criteriaBuilder.equal(from.get("accountId2"), id);
        Predicate predicate = criteriaBuilder.or(predicate1, predicate2);
        CriteriaQuery<Long> select = criteriaQuery.select(criteriaBuilder.count(from)).where(predicate);
        return em.createQuery(select).getSingleResult();
    }

    //    //@Override
    public void insertDialogue(Dialogue dialogue) {
        em.persist(dialogue);
    }

    //    //@Override
    public Dialogue getDialogueById(long id) {
        return em.find(Dialogue.class, id);
    }

    //    //@Override
    public Dialogue getDialogueByAccountsId(long accountId1, long accountId2) {
        try {
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
            CriteriaQuery<Dialogue> criteriaQuery = criteriaBuilder.createQuery(Dialogue.class);
            Root<Dialogue> from = criteriaQuery.from(Dialogue.class);
            Predicate predicate1;
            Predicate predicate2;
            if (accountId1 > accountId2) {
                predicate1 = criteriaBuilder.equal(from.get("accountId2"), accountId1);
                predicate2 = criteriaBuilder.equal(from.get("accountId1"), accountId2);
            } else {
                predicate1 = criteriaBuilder.equal(from.get("accountId1"), accountId1);
                predicate2 = criteriaBuilder.equal(from.get("accountId2"), accountId2);
            }
            Predicate predicate = criteriaBuilder.and(predicate1, predicate2);
            CriteriaQuery<Dialogue> select = criteriaQuery.select((from)).where(predicate);
            return em.createQuery(select).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    //    //@Override
    public void updateDialogue(Dialogue dialogue) {
        em.merge(dialogue);
    }

}