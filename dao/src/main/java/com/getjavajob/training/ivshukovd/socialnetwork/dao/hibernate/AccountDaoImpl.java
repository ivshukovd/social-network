package com.getjavajob.training.ivshukovd.socialnetwork.dao.hibernate;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.AccountShortDto;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

import static java.time.LocalDate.now;
import static java.util.Optional.ofNullable;

//@Repository
public class AccountDaoImpl {

    @PersistenceContext
    private EntityManager em;

    //@Override
    public Account findByEmailAndPassword(String email, String password) {
        try {
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
            CriteriaQuery<Account> criteriaQuery = criteriaBuilder.createQuery(Account.class);
            Root<Account> from = criteriaQuery.from(Account.class);
            CriteriaQuery<Account> select = criteriaQuery.select(from)
                                                         .where(criteriaBuilder.equal(from.get("email"), email),
                                                                criteriaBuilder.equal(from.get("password"), password));
            return em.createQuery(select).getSingleResult();
        } catch (NoResultException exception) {
            return null;
        }
    }

    //@Override
    public Account save(Account account) {
        account.setRegistrationDate(now());
        em.persist(account);
        return account;
    }

    //@Override
    public Optional<Account> findById(long id) {
        return ofNullable(em.find(Account.class, id));
    }

    //@Override
    public Account findAccountWithPhones(long id) {
        try {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<Account> criteriaQuery = builder.createQuery(Account.class);
            Root<Account> root = criteriaQuery.from(Account.class);
            root.fetch("phones", JoinType.LEFT);
            criteriaQuery.where(builder.equal(root.get("id"), id));
            return em.createQuery(criteriaQuery).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    //@Override
    public AccountShortDto findShortAccount(long id) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<AccountShortDto> criteriaQuery = builder.createQuery(AccountShortDto.class);
        Root<Account> root = criteriaQuery.from(Account.class);
        criteriaQuery.select(
                builder.construct(AccountShortDto.class, root.get("id"), root.get("firstName"), root.get("surName"),
                                  root.get("photo")));
        criteriaQuery.where(builder.equal(root.get("id"), id));
        return em.createQuery(criteriaQuery).getSingleResult();
    }

    //@Override
    public List<Account> findAll() {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Account> criteriaQuery = criteriaBuilder.createQuery(Account.class);
        Root<Account> from = criteriaQuery.from(Account.class);
        CriteriaQuery<Account> select = criteriaQuery.select(from);
        return em.createQuery(select).getResultList();
    }

    //@Override
    public List<AccountShortDto> findBySubstring(String str, Pageable pageable) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<AccountShortDto> criteriaQuery = builder.createQuery(AccountShortDto.class);
        Root<Account> root = criteriaQuery.from(Account.class);
        Predicate firstNameContainsStr =
                builder.like(builder.lower(root.get("firstName")), "%" + str.toLowerCase() + "%");
        Predicate surNameContainsStr = builder.like(builder.lower(root.get("surName")), "%" + str.toLowerCase() + "%");
        Predicate predicate = builder.or(firstNameContainsStr, surNameContainsStr);
        criteriaQuery.select(
                builder.construct(AccountShortDto.class, root.get("id"), root.get("firstName"), root.get("surName"),
                                  root.get("photo")));
        criteriaQuery.where(predicate);
        return em.createQuery(criteriaQuery)
                 .setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                 .setMaxResults(pageable.getPageSize())
                 .getResultList();
    }

    //@Override
    public List<AccountShortDto> findBySubstring(String str1, String str2, Pageable pageable) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<AccountShortDto> criteriaQuery = builder.createQuery(AccountShortDto.class);
        Root<Account> root = criteriaQuery.from(Account.class);
        Predicate firstNameContainsStr1 =
                builder.like(builder.lower(root.get("firstName")), "%" + str1.toLowerCase() + "%");
        Predicate firstNameContainsStr2 =
                builder.like(builder.lower(root.get("firstName")), "%" + str2.toLowerCase() + "%");
        Predicate surNameContainsStr1 =
                builder.like(builder.lower(root.get("surName")), "%" + str1.toLowerCase() + "%");
        Predicate surNameContainsStr2 =
                builder.like(builder.lower(root.get("surName")), "%" + str2.toLowerCase() + "%");
        Predicate predicate1 = builder.and(firstNameContainsStr1, surNameContainsStr2);
        Predicate predicate2 = builder.and(firstNameContainsStr2, surNameContainsStr1);
        Predicate predicate = builder.or(predicate1, predicate2);
        criteriaQuery.select(
                builder.construct(AccountShortDto.class, root.get("id"), root.get("firstName"), root.get("surName"),
                                  root.get("photo")));
        criteriaQuery.where(predicate);
        return em.createQuery(criteriaQuery)
                 .setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                 .setMaxResults(pageable.getPageSize())
                 .getResultList();
    }

    //    //@Override
    public void updateEntity(Account account) {
        em.merge(account);
    }

    //@Override
    public void updatePhoto(long photoId, long id) {
        Account account = em.find(Account.class, id);
        account.setPhotoId(photoId);
        em.merge(account);
    }

    //@Override
    public void delete(Account account) {
        em.remove(account);
    }

    //@Override
    public void deleteById(long id) {
        em.remove(em.find(Account.class, id));
    }

    //@Override
    public long countByFirstNameContainingOrSurNameContainingAllIgnoreCase(String str, String str2) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<Account> from = criteriaQuery.from(Account.class);
        Predicate firstNameContainsStr =
                criteriaBuilder.like(criteriaBuilder.lower(from.get("firstName")), "%" + str.toLowerCase() + "%");
        Predicate surNameContainsStr =
                criteriaBuilder.like(criteriaBuilder.lower(from.get("surName")), "%" + str.toLowerCase() + "%");
        Predicate predicate = criteriaBuilder.or(firstNameContainsStr, surNameContainsStr);
        CriteriaQuery<Long> select = criteriaQuery.select(criteriaBuilder.count(from)).where(predicate);
        return em.createQuery(select).getSingleResult();
    }

    //@Override
    public long countByFirstNameContainingAndSurNameContainingAllIgnoreCase(String str1, String str2) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<Account> from = criteriaQuery.from(Account.class);
        Predicate firstNameContainsStr1 =
                criteriaBuilder.like(criteriaBuilder.lower(from.get("firstName")), "%" + str1.toLowerCase() + "%");
        Predicate firstNameContainsStr2 =
                criteriaBuilder.like(criteriaBuilder.lower(from.get("firstName")), "%" + str2.toLowerCase() + "%");
        Predicate surNameContainsStr1 =
                criteriaBuilder.like(criteriaBuilder.lower(from.get("surName")), "%" + str1.toLowerCase() + "%");
        Predicate surNameContainsStr2 =
                criteriaBuilder.like(criteriaBuilder.lower(from.get("surName")), "%" + str2.toLowerCase() + "%");
        Predicate predicate1 = criteriaBuilder.and(firstNameContainsStr1, surNameContainsStr2);
        Predicate predicate2 = criteriaBuilder.and(firstNameContainsStr2, surNameContainsStr1);
        Predicate predicate = criteriaBuilder.or(predicate1, predicate2);
        CriteriaQuery<Long> select = criteriaQuery.select(criteriaBuilder.count(from)).where(predicate);
        return em.createQuery(select).getSingleResult();
    }

    //@Override
    public boolean isAdmin(long id) {
        Optional<Account> account = findById(id);
        return account.map(Account::isAdmin).orElse(false);
    }

    //@Override
    public void makeAdmin(long id) {
        Account account = em.find(Account.class, id);
        account.setAdmin(true);
        em.merge(account);
    }

}