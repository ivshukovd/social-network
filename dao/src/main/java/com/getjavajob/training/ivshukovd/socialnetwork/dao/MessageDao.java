package com.getjavajob.training.ivshukovd.socialnetwork.dao;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Message;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface MessageDao {

    Message save(Message message);

    List<Message> findByDialogueId(long id, Pageable pageable);

    long countByDialogueId(long id);

    void deleteById(long id);

}