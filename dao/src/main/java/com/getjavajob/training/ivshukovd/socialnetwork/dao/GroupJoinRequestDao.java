package com.getjavajob.training.ivshukovd.socialnetwork.dao;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupJoinRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

import static com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupJoinRequest.GroupJoinRequestKey;

public interface GroupJoinRequestDao {

    GroupJoinRequest save(GroupJoinRequest groupJoinRequest);

    List<Account> findByGroupId(long groupId, Pageable pageable);

    long countByGroupId(long groupId);

    boolean existsById(GroupJoinRequestKey id);

    void deleteById(GroupJoinRequestKey id);

    void deleteAllByAccountId(long id);

}
