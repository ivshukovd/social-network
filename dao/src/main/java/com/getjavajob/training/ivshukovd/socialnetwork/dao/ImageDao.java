package com.getjavajob.training.ivshukovd.socialnetwork.dao;

import com.getjavajob.training.ivshukovd.socialnetwork.domain.Image;

import java.util.Optional;

public interface ImageDao {

    Image save(Image img);

    Optional<Image> findById(long id);

    void deleteById(long id);

}
