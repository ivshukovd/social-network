CREATE TABLE IF NOT EXISTS accounts
(
    id               BIGINT      NOT NULL AUTO_INCREMENT UNIQUE,
    firstName        VARCHAR(45) NOT NULL,
    surName          VARCHAR(45) NOT NULL,
    middleName       VARCHAR(45),
    birthDate        DATE        NOT NULL,
    homeAddress      VARCHAR(45),
    workAddress      VARCHAR(45),
    email            VARCHAR(45) NOT NULL UNIQUE,
    icq              VARCHAR(45) UNIQUE,
    skype            VARCHAR(45) UNIQUE,
    password         VARCHAR(45) NOT NULL,
    registrationDate DATE        NOT NULL,
    photoId          BIGINT  default 0,
    admin            boolean default false,
    PRIMARY KEY (id)
);
INSERT INTO accounts (firstName, surName, middleName, birthDate, homeAddress, workAddress, email, icq,
                      skype, password, registrationDate, admin, photoId)
VALUES ('Ivan', 'Ivanov', 'Ivanovich', '1990-01-10', 'Moscow', 'Moscow', 'ivanov@mail.ru', '123456', 'iivanov',
        '123', '1990-01-10', false, 0);
INSERT INTO accounts (firstName, surName, birthDate, email, password, registrationDate, admin, photoId)
VALUES ('Petr', 'Gromov', '1999-07-15', 'gromov@mail.ru', '456', '1999-07-15', true, 0);
INSERT INTO accounts (firstName, surName, birthDate, email, password, registrationDate, admin, photoId)
VALUES ('Aaaaaaa', 'Aaaaaaaa', '1993-01-10', 'aaaaaaaaaa@mail.ru', '546541651', '1993-01-10', false, 0),
       ('Bbbbbbb', 'Bbbbbbbb', '1994-01-10', 'bbbbbbbbb@mail.ru', '41354', '1994-01-10', false, 0),
       ('Ccccccc', 'Ccccccc', '1995-01-10', 'cccccccc@mail.ru', '5135', '1995-01-10', false, 0),
       ('Dddddddd', 'Dddddddd', '1996-01-10', 'dddddddd@mail.ru', '54654313', '1996-01-10', false, 0);

CREATE TABLE IF NOT EXISTS phones
(
    id        BIGINT      NOT NULL AUTO_INCREMENT UNIQUE,
    accountId BIGINT      NOT NULL,
    number    varchar(45) NOT NULL UNIQUE,
    PRIMARY KEY (id)
);
INSERT INTO phones (accountId, number)
VALUES (1, '123'),
       (1, '456'),
       (2, '789');

CREATE TABLE IF NOT EXISTS friends
(
    accountFrom_id BIGINT NOT NULL,
    accountTo_id   BIGINT NOT NULL,
    PRIMARY KEY (accountFrom_id, accountTo_id)
);
INSERT INTO friends (accountFrom_id, accountTo_id)
VALUES (2, 1),
       (3, 1),
       (4, 1),
       (5, 1),
       (6, 1),
       (1, 2),
       (1, 3);

CREATE TABLE IF NOT EXISTS posts
(
    id       BIGINT      NOT NULL AUTO_INCREMENT UNIQUE,
    date     TIMESTAMP   NOT NULL,
    author   BIGINT      NOT NULL,
    type     VARCHAR(45) NOT NULL,
    targetId BIGINT      NOT NULL,
    message  VARCHAR(300),
    photoId  bigint,
    PRIMARY KEY (id)
);
INSERT INTO posts (date, author, type, targetId, message, photoId)
VALUES ('2000-01-01 01:01:01', 1, 'ACCOUNT', 2, 'I am first', 1);
INSERT INTO posts (date, author, type, targetId, message, photoId)
VALUES ('2000-02-02 02:02:02', 2, 'ACCOUNT', 2, 'I am second', 0);
INSERT INTO posts (date, author, type, targetId, message, photoId)
VALUES ('2000-02-02 02:02:03', 1, 'GROUP', 2, 'I am second', 0);
INSERT INTO posts (date, author, type, targetId, message, photoId)
VALUES ('2000-02-02 02:02:04', 2, 'GROUP', 2, 'I am second', 0);
INSERT INTO posts (date, author, type, targetId, message, photoId)
VALUES ('2000-02-02 02:02:05', 3, 'GROUP', 2, 'I am second', 0);

CREATE TABLE IF NOT EXISTS dialogues
(
    id         BIGINT    NOT NULL AUTO_INCREMENT UNIQUE,
    accountId1 BIGINT    NOT NULL,
    accountId2 BIGINT    NOT NULL,
    lastUpdate TIMESTAMP NOT NULL,
    PRIMARY KEY (id)
);
INSERT INTO dialogues (accountId1, accountId2, lastUpdate)
VALUES (1, 2, '2000-02-02 02:02:02'),
       (1, 3, '2000-02-03 01:01:01');

CREATE TABLE IF NOT EXISTS messages
(
    id          BIGINT    NOT NULL AUTO_INCREMENT UNIQUE,
    date        TIMESTAMP NOT NULL,
    accountFrom BIGINT    NOT NULL,
    accountTo   BIGINT    NOT NULL,
    text        VARCHAR(300),
    dialogueId  BIGINT    NOT NULL,
    PRIMARY KEY (id)
);
INSERT INTO messages (date, accountFrom, accountTo, text, dialogueId)
VALUES ('2000-01-01 01:01:01', 1, 2, 'First Message', 1);
INSERT INTO messages (date, accountFrom, accountTo, text, dialogueId)
VALUES ('2000-02-02 02:02:02', 2, 1, 'Second Message', 1);
INSERT INTO messages (date, accountFrom, accountTo, text, dialogueId)
VALUES ('2000-01-03 01:01:01', 1, 3, 'Third Message', 2);

CREATE TABLE IF NOT EXISTS groups
(
    id           BIGINT      NOT NULL AUTO_INCREMENT UNIQUE,
    name         VARCHAR(45) NOT NULL UNIQUE,
    creationDate DATE        NOT NULL,
    description  VARCHAR(100),
    photoId      BIGINT default 0,
    PRIMARY KEY (id)
);
INSERT INTO groups (name, description, creationDate, photoId)
VALUES ('FirstGroup', 'This is first group', '1990-01-10', 0);
INSERT INTO groups (name, description, creationDate, photoId)
VALUES ('SecondGroup', 'This is second group', '1991-02-01', 0);

CREATE TABLE IF NOT EXISTS groupJoinRequests
(
    group_id   BIGINT NOT NULL,
    account_id BIGINT NOT NULL,
    PRIMARY KEY (group_id, account_id)
);
INSERT INTO groupJoinRequests (group_id, account_id)
VALUES (1, 1),
       (1, 2),
       (1, 3);

CREATE TABLE IF NOT EXISTS groupMembers
(
    group_id    BIGINT NOT NULL,
    account_id  BIGINT NOT NULL,
    accountRole varchar(10) DEFAULT 'member',
    PRIMARY KEY (group_id, account_id)
);
INSERT INTO groupMembers (group_id, account_id, accountRole)
VALUES (1, 1, 'member'),
       (1, 2, 'admin'),
       (2, 1, 'creator');

CREATE TABLE IF NOT EXISTS images
(
    id      BIGINT AUTO_INCREMENT NOT NULL,
    content BYTEA                 NOT NULL,
    PRIMARY KEY (id)
);
INSERT INTO images (content)
VALUES (0x123),
       (0x456),
       (0x789);

