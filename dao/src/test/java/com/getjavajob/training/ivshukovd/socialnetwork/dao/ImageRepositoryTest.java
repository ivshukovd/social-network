package com.getjavajob.training.ivshukovd.socialnetwork.dao;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.config.DaoTestConfig;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Image;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;

@ContextConfiguration(classes = {DaoTestConfig.class})
@DataJpaTest
@Sql(scripts = {"classpath:createTables.sql"}, executionPhase = BEFORE_TEST_METHOD)
@Sql(scripts = {"classpath:dropTables.sql"}, executionPhase = AFTER_TEST_METHOD)
class ImageRepositoryTest {

    @Autowired
    private ImageDao imageDao;

    @Test
    void testFindById_EntityExists() {
        assertTrue(imageDao.findById(1).isPresent());
    }

    @Test
    void testFindById_EntityNotExists() {
        assertFalse(imageDao.findById(4).isPresent());
    }

    @Test
    void testSaveImage() {
        Image img = new Image();
        img.setContent(new byte[]{0, 0, 0});
        imageDao.save(img);
        assertTrue(imageDao.findById(4).isPresent());
    }

    @Test
    void testDeleteById() {
        imageDao.deleteById(1);
        assertFalse(imageDao.findById(1).isPresent());
    }

}