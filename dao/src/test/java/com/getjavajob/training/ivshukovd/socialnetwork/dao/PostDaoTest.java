package com.getjavajob.training.ivshukovd.socialnetwork.dao;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.config.DaoTestConfig;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Post;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

import static com.getjavajob.training.ivshukovd.socialnetwork.domain.Post.Type.ACCOUNT;
import static com.getjavajob.training.ivshukovd.socialnetwork.domain.Post.Type.GROUP;
import static java.time.LocalDateTime.now;
import static java.time.ZoneOffset.UTC;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.data.domain.PageRequest.of;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;

@ContextConfiguration(classes = {DaoTestConfig.class})
@DataJpaTest
@Sql(scripts = {"classpath:createTables.sql"}, executionPhase = BEFORE_TEST_METHOD)
@Sql(scripts = {"classpath:dropTables.sql"}, executionPhase = AFTER_TEST_METHOD)
class PostDaoTest {

    @Autowired
    private PostDao postDao;
    @Autowired
    private AccountDao accountDao;

    @Test
    void testGetAccountPosts() {
        assertEquals(2, postDao.findByTargetIdAndTypeOrderByDateDesc(2, ACCOUNT, of(0, 5)).size());
    }

    @Test
    void testGetGroupPosts() {
        assertEquals(3, postDao.findByTargetIdAndTypeOrderByDateDesc(2, GROUP, of(0, 5)).size());
    }

    @Test
    void testCountAccountPosts() {
        assertEquals(2, postDao.countByTargetIdAndType(2, ACCOUNT));
    }

    @Test
    void testCountGroupPosts() {
        assertEquals(3, postDao.countByTargetIdAndType(2, GROUP));
    }

    @Test
    void testExistsByIdAndAuthorId() {
        assertTrue(postDao.existsByIdAndAuthorId(1, 1));
        assertFalse(postDao.existsByIdAndAuthorId(1, 2));
    }

    @Test
    void testInsertAccountPost() {
        Post post = new Post();
        post.setDate(now(UTC));
        Account account = accountDao.findById(1).get();
        post.setAuthor(account);
        post.setTargetId(2);
        post.setType(ACCOUNT);
        post.setMessage("ThirdMessage");
        postDao.save(post);
        assertEquals(3, postDao.findByTargetIdAndTypeOrderByDateDesc(2, ACCOUNT, of(0, 5)).size());
    }

    @Test
    void testInsertGroupPost() {
        Post post = new Post();
        post.setDate(now(UTC));
        Account account = accountDao.findById(1).get();
        post.setAuthor(account);
        post.setTargetId(2);
        post.setType(GROUP);
        post.setMessage("ThirdMessage");
        postDao.save(post);
        assertEquals(4, postDao.findByTargetIdAndTypeOrderByDateDesc(2, GROUP, of(0, 5)).size());
    }

    @Test
    void testDeleteById() {
        postDao.deleteById(1);
        assertEquals(1, postDao.findByTargetIdAndTypeOrderByDateDesc(2, ACCOUNT, of(0, 5)).size());
    }

}