package com.getjavajob.training.ivshukovd.socialnetwork.dao;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.config.DaoTestConfig;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Group;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupMember;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

import static com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupMember.GroupMemberKey;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.data.domain.PageRequest.of;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;

@ContextConfiguration(classes = {DaoTestConfig.class})
@DataJpaTest
@Sql(scripts = {"classpath:createTables.sql"}, executionPhase = BEFORE_TEST_METHOD)
@Sql(scripts = {"classpath:dropTables.sql"}, executionPhase = AFTER_TEST_METHOD)
class GroupMemberDaoTest {

    @Autowired
    private GroupMemberDao groupMemberDao;
    @Autowired
    private GroupDao groupDao;
    @Autowired
    private AccountDao accountDao;

    @Test
    void testGetNumberGroupMembers() {
        assertEquals(2, groupMemberDao.countByGroupId(1));
    }

    @Test
    void testGetGroupMembersId() {
        assertEquals(2, groupMemberDao.findGroupMembers(1, of(0, 5)).size());
    }

    @Test
    void testFindByAccountIdAndAccountRole() {
        assertEquals(1, groupMemberDao.findAllByAccountIdAndAccountRole(2, "admin").size());
    }

    @Test
    void testFindFirstByGroupIdAndAccountRole() {
        assertEquals("Ivanov", groupMemberDao.findFirstByGroupIdAndAccountRole(1, "member").getSurName());
    }

    @Test
    void testFindFirstByGroupIdAndAccountRole_NotExists() {
        assertNull(groupMemberDao.findFirstByGroupIdAndAccountRole(3, "member"));
    }

    @Test
    void testAddGroupCreator() {
        Account acc = accountDao.findById(3).get();
        Group group = groupDao.findById(1).get();
        GroupMember gm = new GroupMember();
        gm.setId(new GroupMemberKey(group.getId(), acc.getId()));
        gm.setGroup(group);
        gm.setAccount(acc);
        gm.setAccountRole("creator");
        groupMemberDao.save(gm);
        assertEquals(3, groupMemberDao.findGroupMembers(1, of(0, 5)).size());
    }

    @Test
    void testDeleteGroupMember() {
        groupMemberDao.deleteById(new GroupMemberKey(1, 2));
        assertEquals(1, groupMemberDao.findGroupMembers(1, of(0, 5)).size());
    }

    @Test
    void testDeleteAllByAccountId() {
        groupMemberDao.deleteAllByAccountId(1);
        assertEquals(1, groupMemberDao.findGroupMembers(1, of(0, 5)).size());
    }

    @Test
    void testGetMemberRole() {
        assertEquals("member", groupMemberDao.findAccountRole(new GroupMemberKey(1, 1)));
    }

    @Test
    void testAccountIsMember() {
        assertTrue(groupMemberDao.existsById(new GroupMemberKey(1, 1)));
    }

    @Test
    void testAccountIsNotMember() {
        assertFalse(groupMemberDao.existsById(new GroupMemberKey(1, 3)));
    }

    @Test
    void testMakeCreator() {
        groupMemberDao.makeCreator(1, 1);
        assertEquals("creator", groupMemberDao.findAccountRole(new GroupMemberKey(1, 1)));
    }

    @Test
    void testMakeAdmin() {
        groupMemberDao.makeAdmin(1, 1);
        assertEquals("admin", groupMemberDao.findAccountRole(new GroupMemberKey(1, 1)));
    }

    @Test
    void testMakeMember() {
        groupMemberDao.makeMember(1, 2);
        assertEquals("member", groupMemberDao.findAccountRole(new GroupMemberKey(1, 2)));
    }

    @Test
    void testGetNumberGroupsByMemberId() {
        assertEquals(2, groupMemberDao.countByMemberId(1));
        assertEquals(0, groupMemberDao.countByMemberId(3));
    }

    @Test
    void testGetGroupsByMemberId() {
        assertEquals(2, groupMemberDao.findAccountGroups(1, of(0, 5)).size());
    }

    @Test
    void testGetGroupListForAccountWithoutGroups() {
        assertEquals(0, groupMemberDao.findAccountGroups(3, of(0, 5)).size());
    }

}