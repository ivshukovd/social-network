package com.getjavajob.training.ivshukovd.socialnetwork.dao;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.config.DaoTestConfig;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Message;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

import static java.time.LocalDateTime.now;
import static java.time.ZoneOffset.UTC;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.data.domain.PageRequest.of;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;

@ContextConfiguration(classes = {DaoTestConfig.class})
@DataJpaTest
@Sql(scripts = {"classpath:createTables.sql"}, executionPhase = BEFORE_TEST_METHOD)
@Sql(scripts = {"classpath:dropTables.sql"}, executionPhase = AFTER_TEST_METHOD)
class MessageDaoTest {

    @Autowired
    private MessageDao messageDao;
    @Autowired
    private DialogueDao dialogueDao;

    @Test
    void testInsertMessage() {
        Message message = new Message();
        message.setDate(now(UTC));
        message.setAccountFrom(1);
        message.setAccountTo(2);
        message.setText("Third message");
        message.setDialogue(dialogueDao.findById(1).get());
        messageDao.save(message);
        assertEquals(3, messageDao.findByDialogueId(1, of(0, 5)).size());
    }

    @Test
    void testGetMessages() {
        assertEquals(2, messageDao.findByDialogueId(1, of(0, 5)).size());
    }

    @Test
    void testGetEmptyMessageList() {
        assertEquals(0, messageDao.findByDialogueId(3, of(0, 5)).size());
    }

    @Test
    void testDeleteMessages() {
        messageDao.deleteById(1);
        assertEquals(1, messageDao.findByDialogueId(1, of(0, 5)).size());
    }

    @Test
    void testGetNumberMessages() {
        assertEquals(2, messageDao.countByDialogueId(1));
        assertEquals(0, messageDao.countByDialogueId(3));
    }

}