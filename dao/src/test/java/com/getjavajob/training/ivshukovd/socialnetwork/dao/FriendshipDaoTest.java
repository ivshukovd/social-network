package com.getjavajob.training.ivshukovd.socialnetwork.dao;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.config.DaoTestConfig;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Friendship;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

import static com.getjavajob.training.ivshukovd.socialnetwork.domain.Friendship.FriendshipKey;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.data.domain.PageRequest.of;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;

@ContextConfiguration(classes = {DaoTestConfig.class})
@DataJpaTest
@Sql(scripts = {"classpath:createTables.sql"}, executionPhase = BEFORE_TEST_METHOD)
@Sql(scripts = {"classpath:dropTables.sql"}, executionPhase = AFTER_TEST_METHOD)
class FriendshipDaoTest {

    @Autowired
    private FriendshipDao friendshipDao;
    @Autowired
    private AccountDao accountDao;

    @Test
    void testGetFollowers() {
        assertEquals(3, friendshipDao.findFollowers(1, of(0, 5)).size());
    }

    @Test
    void testGetFriends() {
        assertEquals(2, friendshipDao.findFriends(1, of(0, 5)).size());
    }

    @Test
    void testRelationshipExists() {
        assertTrue(friendshipDao.existsById(new FriendshipKey(1, 2)));
    }

    @Test
    void testIsFollower() {
        boolean follower =
                friendshipDao.existsById(new FriendshipKey(4, 1)) && !friendshipDao.existsById(new FriendshipKey(1, 4));
        assertTrue(follower);
    }

    @Test
    void testIsNotFollower_IsFriend() {
        boolean follower =
                friendshipDao.existsById(new FriendshipKey(2, 1)) && !friendshipDao.existsById(new FriendshipKey(1, 2));
        assertFalse(follower);
    }

    @Test
    void testIsNotFollower_RelationshipNotExists() {
        boolean follower =
                friendshipDao.existsById(new FriendshipKey(7, 1)) && !friendshipDao.existsById(new FriendshipKey(1, 7));
        assertFalse(follower);
    }

    @Test
    void testIsFriend() {
        boolean friend =
                friendshipDao.existsById(new FriendshipKey(2, 1)) && friendshipDao.existsById(new FriendshipKey(1, 2));
        assertTrue(friend);
    }

    @Test
    void testIsNotFriend_IsFollower() {
        boolean friend =
                friendshipDao.existsById(new FriendshipKey(4, 1)) && friendshipDao.existsById(new FriendshipKey(1, 4));
        assertFalse(friend);
    }

    @Test
    void testIsNotFriend_RelationshipNotExists() {
        boolean friend =
                friendshipDao.existsById(new FriendshipKey(7, 1)) && friendshipDao.existsById(new FriendshipKey(1, 7));
        assertFalse(friend);
    }

    @Test
    void testInsertFriend() {
        Account accFrom = accountDao.findById(1).get();
        Account accTo = accountDao.findById(4).get();
        Friendship friendship = new Friendship();
        friendship.setAccountFrom(accFrom);
        friendship.setAccountTo(accTo);
        friendship.setId(new FriendshipKey(1, 4));
        friendshipDao.save(friendship);
        assertEquals(3, friendshipDao.findFriends(1, of(0, 5)).size());
    }

    @Test
    void testDeleteFriend() {
        friendshipDao.deleteById(new FriendshipKey(1, 2));
        assertEquals(1, friendshipDao.findFriends(1, of(0, 5)).size());
    }

    @Test
    void testDeleteFollower() {
        friendshipDao.deleteById(new FriendshipKey(4, 1));
        assertEquals(2, friendshipDao.findFollowers(1, of(0, 5)).size());
    }

    @Test
    void testDeleteAllByAccountId() {
        friendshipDao.deleteAllByAccountId(2);
        assertEquals(1, friendshipDao.findFriends(1, of(0, 5)).size());
    }

    @Test
    void testNumberFollowers() {
        assertEquals(3, friendshipDao.countFollowers(1));
    }

    @Test
    void testNumberFriends() {
        assertEquals(2, friendshipDao.countFriends(1));
    }

}