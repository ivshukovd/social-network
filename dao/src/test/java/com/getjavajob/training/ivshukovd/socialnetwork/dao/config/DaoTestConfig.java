package com.getjavajob.training.ivshukovd.socialnetwork.dao.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = "com.getjavajob.training.ivshukovd.socialnetwork.dao")
public class DaoTestConfig {

}