package com.getjavajob.training.ivshukovd.socialnetwork.dao;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.config.DaoTestConfig;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Phone;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;

@ContextConfiguration(classes = {DaoTestConfig.class})
@DataJpaTest
@Sql(scripts = {"classpath:createTables.sql"}, executionPhase = BEFORE_TEST_METHOD)
@Sql(scripts = {"classpath:dropTables.sql"}, executionPhase = AFTER_TEST_METHOD)
class PhoneDaoTest {

    @Autowired
    private PhoneDao phoneDao;
    @Autowired
    private AccountDao accountDao;

    @Test
    void testInsertPhone() {
        Phone newPhone = new Phone();
        Account account = accountDao.findById(2).get();
        newPhone.setAccount(account);
        newPhone.setNumber("135");
        phoneDao.save(newPhone);
        assertEquals(2, phoneDao.findByAccountId(2).size());
    }

    @Test
    void testGetAccountPhones() {
        assertEquals(2, phoneDao.findByAccountId(1).size());
    }

    @Test
    void testExistsByNumber() {
        assertTrue(phoneDao.existsByNumber("123"));
    }

    @Test
    void testExistsByNumber_NotExists() {
        assertFalse(phoneDao.existsByNumber("111"));
    }

    @Test
    void testExistsByAccountIdNumber() {
        assertTrue(phoneDao.existsByAccountIdAndNumber(1, "123"));
    }

    @Test
    void testExistsByAccountIdNumber_WrongNumber() {
        assertFalse(phoneDao.existsByAccountIdAndNumber(1, "111"));
    }

    @Test
    void testExistsByAccountIdNumber_WrongAccountId() {
        assertFalse(phoneDao.existsByAccountIdAndNumber(2, "123"));
    }

    @Test
    void testDeletePhone() {
        List<Phone> phone = phoneDao.findByAccountId(1);
        phoneDao.delete(phone.get(0));
        assertEquals(1, phoneDao.findByAccountId(1).size());
    }

    @Test
    void testDeleteAccountPhones() {
        phoneDao.deleteAllByAccountId(1);
        assertEquals(0, phoneDao.findByAccountId(1).size());
    }

}