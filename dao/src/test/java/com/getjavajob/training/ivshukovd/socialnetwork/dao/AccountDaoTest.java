package com.getjavajob.training.ivshukovd.socialnetwork.dao;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.config.DaoTestConfig;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

import static java.time.LocalDate.parse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.data.domain.PageRequest.of;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;

@ContextConfiguration(classes = {DaoTestConfig.class})
@DataJpaTest
@Sql(scripts = {"classpath:createTables.sql"}, executionPhase = BEFORE_TEST_METHOD)
@Sql(scripts = {"classpath:dropTables.sql"}, executionPhase = AFTER_TEST_METHOD)
class AccountDaoTest {

    @Autowired
    private AccountDao accountDao;

    @Test
    void testGetID() {
        assertEquals(2, accountDao.findByEmailAndPassword("gromov@mail.ru", "456").get().getId());
    }

    @Test
    void testGetIDWrongEmail() {
        assertFalse(accountDao.findByEmailAndPassword("iivanov@mail.ru", "123").isPresent());
    }

    @Test
    void testGetIDWrongPassword() {
        assertFalse(accountDao.findByEmailAndPassword("ivanov@mail.ru", "1234").isPresent());
    }

    @Test
    void testGetIDWrongEmailAndPassword() {
        assertFalse(accountDao.findByEmailAndPassword("iivanov@mail.ru", "1234").isPresent());
    }

    @Test
    void testGetAccount() {
        assertEquals("Petr", accountDao.findById(2).get().getFirstName());
        assertEquals("789", accountDao.findById(2).get().getPhones().get(0).getNumber());
    }

    @Test
    void testGetShortAccount() {
        assertEquals("Petr", accountDao.findShortAccount(2).get().getFirstName());
    }

    @Test
    void testGetAccountWithPhones() {
        assertEquals("Petr", accountDao.findAccountWithPhones(2).get().getFirstName());
        assertEquals("789", accountDao.findAccountWithPhones(2).get().getPhones().get(0).getNumber());
    }

    @Test
    void testGetNullAccount() {
        assertFalse(accountDao.findById(7).isPresent());
    }

    @Test
    void testGetNullAccountWithPhones() {
        assertFalse(accountDao.findAccountWithPhones(7).isPresent());
    }

    @Test
    void testGetAllAccounts() {
        assertEquals(6, accountDao.findAll().size());
    }

    @Test
    void testGetSearchedAccounts() {
        assertEquals("Ivanov", accountDao.findBySubstring("ov", of(0, 5)).get(0).getSurName());
        assertEquals(2, accountDao.findBySubstring("ov", of(0, 5)).size());
        assertEquals(2, accountDao.findBySubstring("Ov", of(0, 5)).size());
        assertEquals(1, accountDao.findBySubstring("grom", of(0, 5)).size());
    }

    @Test
    void testGetAccountsBySubstrings() {
        Account acc = new Account();
        acc.setFirstName("Gromoverzhets");
        acc.setSurName("Petrov");
        acc.setBirthDate(parse("1980-05-20"));
        acc.setEmail("grompet@gmail.com");
        acc.setPassword("789");
        acc.setRegistrationDate(parse("1980-05-20"));
        accountDao.save(acc);
        assertEquals(2, accountDao.findBySubstring("Petr", "Gromov", of(0, 5)).size());
        assertEquals(2, accountDao.findBySubstring("Gromov", "Petr", of(0, 5)).size());
    }

    @Test
    void testInsertAccount() {
        Account acc = new Account();
        acc.setFirstName("Irina");
        acc.setSurName("Bobrova");
        acc.setBirthDate(parse("1980-05-20"));
        acc.setEmail("bobrova@gmail.com");
        acc.setPassword("789");
        acc.setRegistrationDate(parse("1980-05-20"));
        accountDao.save(acc);
        assertEquals(7, accountDao.findAll().size());
    }

    @Test
    void testDeleteAccount() {
        Account account = accountDao.findById(1).get();
        accountDao.delete(account);
        assertEquals(5, accountDao.findAll().size());
    }

    @Test
    void testDeleteAccountById() {
        accountDao.deleteById(1);
        assertEquals(5, accountDao.findAll().size());
    }

    @Test
    void testUpdateAccount() {
        Account acc = accountDao.findById(1).get();
        acc.setFirstName("updated");
        accountDao.save(acc);
        assertEquals("updated", accountDao.findById(1).get().getFirstName());
    }

    @Test
    void testGetNumberAccounts() {
        assertEquals(1, accountDao.countByFirstNameContainingOrSurNameContainingAllIgnoreCase("iv", "iv"));
        assertEquals(2, accountDao.countByFirstNameContainingOrSurNameContainingAllIgnoreCase("ov", "ov"));
        assertEquals(2, accountDao.countByFirstNameContainingOrSurNameContainingAllIgnoreCase("Ov", "Ov"));
        assertEquals(6, accountDao.countByFirstNameContainingOrSurNameContainingAllIgnoreCase("", ""));
    }

    @Test
    void testGetNumberAccountsByTwoSubstring() {
        assertEquals(1, accountDao.countByFirstNameContainingAndSurNameContainingAllIgnoreCase("Petr", "gromov"));
    }

    @Test
    void testGetZeroAccounts() {
        assertEquals(0, accountDao.countByFirstNameContainingOrSurNameContainingAllIgnoreCase("vlad", "vlad"));
    }

    @Test
    void testUpdatePhoto() {
        accountDao.updatePhoto(2, 1);
        assertEquals(2, accountDao.findById(1).get().getPhotoId());
    }

    @Test
    void testIsNotAmin() {
        assertFalse(accountDao.isAdmin(1));
    }

    @Test
    void testIsAmin() {
        assertTrue(accountDao.isAdmin(2));
    }

    @Test
    void testMakeAdmin() {
        accountDao.makeAdmin(1);
        assertTrue(accountDao.isAdmin(1));
    }

    @Test
    void testExistsByEmail() {
        assertTrue(accountDao.existsByEmail("ivanov@mail.ru"));
    }

    @Test
    void testExistsByEmail_NotExists() {
        assertFalse(accountDao.existsByEmail("iivanov@mail.ru"));
    }

    @Test
    void testExistsByIcq() {
        assertTrue(accountDao.existsByIcq("123456"));
    }

    @Test
    void testExistsByIcq_NotExists() {
        assertFalse(accountDao.existsByIcq("111111"));
    }

    @Test
    void testExistsBySkype() {
        assertTrue(accountDao.existsBySkype("iivanov"));
    }

    @Test
    void testExistsBySkype_NotExists() {
        assertFalse(accountDao.existsBySkype("iiivanov"));
    }

    @Test
    void testExistsByIdAndEmail() {
        assertTrue(accountDao.existsByIdAndEmail(1, "ivanov@mail.ru"));
    }

    @Test
    void testExistsByIdAndEmail_WrongEmail() {
        assertFalse(accountDao.existsByIdAndEmail(1, "iivanov@mail.ru"));
    }

    @Test
    void testExistsByIdAndEmail_WrongId() {
        assertFalse(accountDao.existsByIdAndEmail(2, "ivanov@mail.ru"));
    }

    @Test
    void testExistsByIdAndIcq() {
        assertTrue(accountDao.existsByIdAndIcq(1, "123456"));
    }

    @Test
    void testExistsByAndIcq_WrongIcq() {
        assertFalse(accountDao.existsByIdAndIcq(1, "111111"));
    }

    @Test
    void testExistsByAndIcq_WrongId() {
        assertFalse(accountDao.existsByIdAndIcq(2, "123456"));
    }

    @Test
    void testExistsByIdAndSkype() {
        assertTrue(accountDao.existsByIdAndSkype(1, "iivanov"));
    }

    @Test
    void testExistsBySkype_WrongSkype() {
        assertFalse(accountDao.existsByIdAndSkype(1, "iiivanov"));
    }

    @Test
    void testExistsBySkype_WrongId() {
        assertFalse(accountDao.existsByIdAndSkype(2, "iivanov"));
    }

}