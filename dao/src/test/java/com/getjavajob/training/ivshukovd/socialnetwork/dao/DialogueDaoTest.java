package com.getjavajob.training.ivshukovd.socialnetwork.dao;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.config.DaoTestConfig;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Dialogue;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

import static java.time.LocalDateTime.now;
import static java.time.ZoneOffset.UTC;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.data.domain.PageRequest.of;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;

@ContextConfiguration(classes = {DaoTestConfig.class})
@DataJpaTest
@Sql(scripts = {"classpath:createTables.sql"}, executionPhase = BEFORE_TEST_METHOD)
@Sql(scripts = {"classpath:dropTables.sql"}, executionPhase = AFTER_TEST_METHOD)
class DialogueDaoTest {

    @Autowired
    private DialogueDao dialogueDao;
    @Autowired
    private AccountDao accountDao;

    @Test
    void testInsertDialogue() {
        Dialogue dialogue = new Dialogue();
        dialogue.setAccount1(accountDao.findById(1).get());
        dialogue.setAccount2(accountDao.findById(4).get());
        dialogue.setLastUpdate(now(UTC));
        dialogueDao.save(dialogue);
        assertTrue(dialogueDao.findById(3).isPresent());
    }

    @Test
    void testGetDialogues() {
        assertEquals(2, dialogueDao.findByAccountIdOrderByLastUpdateDesc(1, of(0, 5)).size());
    }

    @Test
    void testGetEmptyDialogueList() {
        assertEquals(0, dialogueDao.findByAccountIdOrderByLastUpdateDesc(4, of(0, 5)).size());
    }

    @Test
    void testGetNumberDialogPartners() {
        assertEquals(2, dialogueDao.countDialogPartners(1));
    }

    @Test
    void testGetNumberDialogPartners_NotExists() {
        assertEquals(0, dialogueDao.countDialogPartners(4));
    }

    @Test
    void testGetDialogueById() {
        assertEquals(3, dialogueDao.findById(2).get().getAccount2().getId());
    }

    @Test
    void testGetDialogueByAccountsId() {
        assertEquals(2, dialogueDao.findByAccountsId(1, 3).get().getId());
    }

    @Test
    void testDeleteAllByAccountId() {
        dialogueDao.deleteAllByAccountId(1);
        assertTrue(dialogueDao.findByAccountIdOrderByLastUpdateDesc(1, of(0, 5)).isEmpty());
    }

}