package com.getjavajob.training.ivshukovd.socialnetwork.dao;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.config.DaoTestConfig;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Group;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

import static java.time.LocalDate.parse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.data.domain.PageRequest.of;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;

@ContextConfiguration(classes = {DaoTestConfig.class})
@DataJpaTest
@Sql(scripts = {"classpath:createTables.sql"}, executionPhase = BEFORE_TEST_METHOD)
@Sql(scripts = {"classpath:dropTables.sql"}, executionPhase = AFTER_TEST_METHOD)
class GroupDaoTest {

    @Autowired
    private GroupDao groupDao;

    @Test
    void testGetGroup() {
        assertEquals("SecondGroup", groupDao.findById(2).get().getName());
    }

    @Test
    void tesGetSearchedGroups() {
        assertEquals("FirstGroup", groupDao.findBySubstring("group", of(0, 5)).get(0).getName());
    }

    @Test
    void testDeleteGroup() {
        groupDao.delete(groupDao.findById(1).get());
        assertEquals(1, groupDao.findBySubstring("", of(0, 5)).size());
    }

    @Test
    void testInsertGroup() {
        Group group = new Group();
        group.setName("ThirdGroup");
        group.setDescription("third group");
        group.setCreationDate(parse("1980-05-20"));
        Group group1 = groupDao.save(group);
        assertEquals(3, groupDao.findBySubstring("", of(0, 5)).size());
        assertEquals(3, group1.getId());
    }

    @Test
    void testUpdateName() {
        Group group = groupDao.findById(1).get();
        group.setName("updatedName");
        groupDao.save(group);
        assertEquals("updatedName", groupDao.findById(1).get().getName());
    }

    @Test
    void testGetNumberGroupsBySubstring() {
        assertEquals(1, groupDao.countByNameContainingIgnoreCase("sec"));
        assertEquals(2, groupDao.countByNameContainingIgnoreCase("gRou"));
    }

    @Test
    void testUpdatePhoto() {
        groupDao.updatePhoto(3, 1);
        assertEquals(3, groupDao.findById(1).get().getPhotoId());
    }

}