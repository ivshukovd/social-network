package com.getjavajob.training.ivshukovd.socialnetwork.dao;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.config.DaoTestConfig;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Group;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupJoinRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

import static com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupJoinRequest.GroupJoinRequestKey;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.data.domain.PageRequest.of;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;

@ContextConfiguration(classes = {DaoTestConfig.class})
@DataJpaTest
@Sql(scripts = {"classpath:createTables.sql"}, executionPhase = BEFORE_TEST_METHOD)
@Sql(scripts = {"classpath:dropTables.sql"}, executionPhase = AFTER_TEST_METHOD)
class GroupJoinRequestRepositoryTest {

    @Autowired
    private GroupJoinRequestDao groupJoinRequestDao;
    @Autowired
    private GroupDao groupDao;
    @Autowired
    private AccountDao accountDao;

    @Test
    void testGetJoinAccountId() {
        assertEquals(3, groupJoinRequestDao.findByGroupId(1, of(0, 5)).size());
    }

    @Test
    void testIsJoinRequest() {
        assertTrue(groupJoinRequestDao.existsById(new GroupJoinRequestKey(1, 1)));
    }

    @Test
    void testIsNotJoinRequest() {
        assertFalse(groupJoinRequestDao.existsById(new GroupJoinRequestKey(1, 4)));
    }

    @Test
    void testGetNumberJoinRequests() {
        assertEquals(3, groupJoinRequestDao.countByGroupId(1));
    }

    @Test
    void testGetZeroJoinRequests() {
        assertEquals(0, groupJoinRequestDao.countByGroupId(2));
    }

    @Test
    void testAddJoinGroupRequest() {
        Group group = groupDao.findById(1).get();
        Account account = accountDao.findById(4).get();
        GroupJoinRequest gjr = new GroupJoinRequest();
        gjr.setAccount(account);
        gjr.setGroup(group);
        gjr.setId(new GroupJoinRequestKey(group.getId(), account.getId()));
        groupJoinRequestDao.save(gjr);
        assertTrue(groupJoinRequestDao.existsById(new GroupJoinRequestKey(1, 4)));
    }

    @Test
    void testDeleteJoinGroupRequest() {
        groupJoinRequestDao.deleteById(new GroupJoinRequestKey(1, 1));
        assertEquals(2, groupJoinRequestDao.findByGroupId(1, of(0, 5)).size());
    }

    @Test
    void testDeleteByAccountId() {
        groupJoinRequestDao.deleteAllByAccountId(1);
        assertEquals(2, groupJoinRequestDao.findByGroupId(1, of(0, 5)).size());
    }

}