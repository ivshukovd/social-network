package com.getjavajob.training.ivshukovd.socialnetwork.service;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.AccountDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.DialogueDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.FriendshipDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.GroupDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.GroupJoinRequestDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.GroupMemberDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.ImageDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.PhoneDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.PostDao;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Friendship;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Phone;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.AccountDto;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.AccountShortDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.getjavajob.training.ivshukovd.socialnetwork.domain.Friendship.FriendshipKey;
import static java.util.Collections.singletonList;
import static java.util.Optional.of;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AccountServiceTest {

    private AccountDao accountDaoMock;
    private PhoneDao phoneDaoMock;
    private FriendshipDao friendshipDaoMock;
    private PostDao postDaoMock;
    private ImageDao imageDaoMock;
    private DialogueDao dialogueDaoMock;
    private GroupDao groupDaoMock;
    private GroupMemberDao groupMemberDaoMock;
    private GroupJoinRequestDao groupJoinRequestDaoMock;
    private AccountService accountService;
    private List<Account> accounts;
    private List<Phone> phones;

    @BeforeEach
    void init() {
        accountDaoMock = mock(AccountDao.class);
        phoneDaoMock = mock(PhoneDao.class);
        friendshipDaoMock = mock(FriendshipDao.class);
        postDaoMock = mock(PostDao.class);
        imageDaoMock = mock(ImageDao.class);
        dialogueDaoMock = mock(DialogueDao.class);
        groupDaoMock = mock(GroupDao.class);
        groupMemberDaoMock = mock(GroupMemberDao.class);
        groupJoinRequestDaoMock = mock(GroupJoinRequestDao.class);
        accountService = new AccountService(accountDaoMock, phoneDaoMock, imageDaoMock, friendshipDaoMock, postDaoMock,
                                            dialogueDaoMock, groupDaoMock, groupJoinRequestDaoMock, groupMemberDaoMock);
        phones = new ArrayList<>();
        Phone phone1 = new Phone();
        phone1.setNumber("123");
        phones.add(phone1);
        Phone phone2 = new Phone();
        phone2.setNumber("456");
        phones.add(phone2);
        accounts = new ArrayList<>();
        Account account1 = new Account();
        account1.setFirstName("Ivan");
        account1.setSurName("Ivanov");
        account1.setPhones(phones);
        account1.setBirthDate(LocalDate.parse("1980-05-20"));
        account1.setEmail("ivanov@mail.ru");
        account1.setId(1);
        account1.setPassword("123");
        accounts.add(account1);
    }

    @Test
    void testCreateAccount() {
        List<Phone> phones = new ArrayList<>();
        Phone newPhone = new Phone();
        newPhone.setNumber("789");
        phones.add(newPhone);
        Account newAccount = new Account();
        newAccount.setPhones(phones);
        newAccount.setId(2);
        doAnswer(invocationOnMock -> {
            Object[] args = invocationOnMock.getArguments();
            accounts.add((Account) args[0]);
            this.phones.add(new Phone());
            return new Account();
        }).when(accountDaoMock).save(newAccount);
        accountService.addAccount(newAccount);
        assertEquals(2, accounts.size());
        assertEquals(3, this.phones.size());
    }

    @Test
    void testUpdateAccount_ReceiveAccountDto() {
        List<Account> updatedAccounts = new ArrayList<>();
        AccountDto accountDto = new AccountDto();
        accountDto.setId(1);
        accountDto.setFirstName("Petr");
        accountDto.setPhones(singletonList("789"));
        Account account = new Account();
        account.setPhones(new ArrayList<>());
        Optional<Account> optionalAccount = of(account);
        when(accountDaoMock.findById(any(Long.class))).thenReturn(optionalAccount);
        doAnswer(invocationOnMock -> {
            updatedAccounts.add(account);
            return null;
        }).when(accountDaoMock).save(account);
        accountService.updateAccount(accountDto);
        assertEquals("Petr", updatedAccounts.get(0).getFirstName());
        assertEquals("789", updatedAccounts.get(0).getPhones().get(0).getNumber());
    }

    @Test
    void testUpdateAccount_ReceiveAccount() {
        Account updatedAccount = new Account();
        updatedAccount.setId(1);
        updatedAccount.setFirstName("Petr");
        Phone phone = new Phone();
        phone.setNumber("789");
        updatedAccount.setPhones(singletonList(phone));
        Account account = new Account();
        account.setPhones(new ArrayList<>());
        doAnswer(invocationOnMock -> {
            account.setFirstName(updatedAccount.getFirstName());
            account.setPhones(updatedAccount.getPhones());
            return null;
        }).when(accountDaoMock).save(updatedAccount);
        accountService.updateAccount(updatedAccount);
        assertEquals("Petr", account.getFirstName());
        assertEquals("789", account.getPhones().get(0).getNumber());
    }

    @Test
    void testDeleteAccount() {
        doAnswer(invocationOnMock -> {
            Object[] args = invocationOnMock.getArguments();
            accounts.remove(0);
            return null;
        }).when(accountDaoMock).deleteById(any(Long.class));
        accountService.deleteAccount(1);
        assertTrue(accounts.isEmpty());
    }

    @Test
    void addFriend() {
        Map<Integer, Integer> friends = new HashMap<>();
        doAnswer(invocationOnMock -> {
            Object[] args = invocationOnMock.getArguments();
            friends.put(1, 2);
            return null;
        }).when(friendshipDaoMock).save(any(Friendship.class));
        when(accountDaoMock.findById(any(Long.class))).thenReturn(of(new Account()));
        accountService.addRelationship(1L, 2L);
        assertEquals(2, friends.get(1).intValue());
    }

    @Test
    void testDeleteFriend() {
        List<Integer> friends = new ArrayList<>();
        friends.add(1);
        friends.add(2);
        friends.add(3);
        doAnswer(invocationOnMock -> {
            Object[] args = invocationOnMock.getArguments();
            friends.remove(0);
            return null;
        }).when(friendshipDaoMock).deleteById(any(FriendshipKey.class));
        accountService.deleteFriend(1, 2);
        assertEquals(1, friends.size());
    }

    @Test
    void testDeleteFollower() {
        List<Integer> followers = new ArrayList<>();
        followers.add(1);
        followers.add(2);
        followers.add(3);
        doAnswer(invocationOnMock -> {
            followers.remove(0);
            return null;
        }).when(friendshipDaoMock).deleteById(any(FriendshipKey.class));
        accountService.deleteFollower(1, 2);
        assertEquals(2, followers.size());
    }

    @Test
    void testGetFriends() {
        List<Account> friends = new LinkedList<>();
        friends.add(new Account());
        friends.add(new Account());
        when(friendshipDaoMock.findFriends(1, PageRequest.of(0, 5))).thenReturn(friends);
        when(accountDaoMock.findById(any(Integer.class))).thenReturn(of(new Account()));
        List<Account> foundFriends = accountService.getFriends(1, 1, 5);
        assertEquals(2, foundFriends.size());
    }

    @Test
    void testGetFollowers() {
        List<Account> followers = new LinkedList<>();
        followers.add(new Account());
        followers.add(new Account());
        when(friendshipDaoMock.findFollowers(1, PageRequest.of(0, 5))).thenReturn(followers);
        when(accountDaoMock.findById(any(Integer.class))).thenReturn(of(new Account()));
        List<Account> foundFollowers = accountService.getFollowers(1, 1, 5);
        assertEquals(2, foundFollowers.size());
    }

    @Test
    void testGetNumberPages() {
        when(accountDaoMock.countByFirstNameContainingOrSurNameContainingAllIgnoreCase(any(String.class),
                                                                                       any(String.class))).thenReturn(
                6L);
        assertEquals(2, accountService.getNumberAccountPages("str", 5));
    }

    @Test
    void testGetNumberPagesByFullName() {
        when(accountDaoMock.countByFirstNameContainingAndSurNameContainingAllIgnoreCase(any(String.class),
                                                                                        any(String.class))).thenReturn(
                6L);
        assertEquals(2, accountService.getNumberAccountPages("str1", "str2", 5));
    }

    @Test
    void testGetFollowerNumberPages() {
        when(friendshipDaoMock.countFollowers(any(Long.class))).thenReturn(6L);
        assertEquals(2, accountService.getNumberOfPagesInFollowerList(1, 5));
    }

    @Test
    void testGetFriendNumberPages() {
        when(friendshipDaoMock.countFriends(any(Long.class))).thenReturn(11L);
        assertEquals(3, accountService.getNumberOfPagesInFriendList(1, 5));
    }

    @Test
    void testGetAccount() {
        Account account = new Account();
        account.setFirstName("Ivan");
        when(accountDaoMock.findById(1)).thenReturn(of(account));
        assertEquals("Ivan", accountService.getAccount(1).get().getFirstName());
    }

    @Test
    void testGetAccountWithPhones() {
        Account account = new Account();
        account.setFirstName("Ivan");
        when(accountDaoMock.findAccountWithPhones(1)).thenReturn(of(account));
        assertEquals("Ivan", accountService.getAccountWithPhones(1).get().getFirstName());
    }

    @Test
    void testGetAccountByEmail() {
        Account account = new Account();
        account.setFirstName("Ivan");
        when(accountDaoMock.findByEmail("email@mail.ru")).thenReturn(of(account));
        assertEquals("Ivan", accountService.getAccountByEmail("email@mail.ru").get().getFirstName());
    }

    @Test
    void testGetAccountsBySubstring() {
        List<AccountShortDto> accountShortDto = new ArrayList<>();
        accountShortDto.add(new AccountShortDto(1, "1", "1", 0));
        accountShortDto.add(new AccountShortDto(2, "2", "2", 0));
        when(accountDaoMock.findBySubstring(any(String.class), any(Pageable.class))).thenReturn(accountShortDto);
        assertEquals(2, accountService.getAccountsBySubstr("str", 1, 5).size());
    }

    @Test
    void testGetAccountsByFullName() {
        List<AccountShortDto> accountShortDtos = new ArrayList<>();
        accountShortDtos.add(new AccountShortDto(1, "1", "1", 0));
        accountShortDtos.add(new AccountShortDto(2, "2", "2", 0));
        when(accountDaoMock.findBySubstring(any(String.class), any(String.class), any(Pageable.class))).thenReturn(
                accountShortDtos);
        assertEquals(2, accountService.getAccountsByFullName("str1", "str2", 1, 5).size());
    }

    @Test
    void testExistsAccountById() {
        when(accountDaoMock.existsById(1)).thenReturn(true);
        assertTrue(accountService.existsAccountById(1));
        assertFalse(accountService.existsAccountById(2));
    }

    @Test
    void testExistsAccountByEmail() {
        String email = "email@mail.ru";
        when(accountDaoMock.existsByEmail(email)).thenReturn(true);
        assertTrue(accountService.existsAccountByEmail(email));
        assertFalse(accountService.existsAccountByEmail("email@yandex.ru"));
    }

    @Test
    void testExistsAccountByIcq() {
        String icq = "123";
        when(accountDaoMock.existsByIcq(icq)).thenReturn(true);
        assertTrue(accountService.existsAccountByIcq(icq));
        assertFalse(accountService.existsAccountByIcq("456"));
    }

    @Test
    void testExistsAccountBySkype() {
        String skype = "skype";
        when(accountDaoMock.existsBySkype(skype)).thenReturn(true);
        assertTrue(accountService.existsAccountBySkype(skype));
        assertFalse(accountService.existsAccountBySkype("SKYPE"));
    }

    @Test
    void testExistsAccountByIdAndEmail() {
        String email = "email@mail.ru";
        when(accountDaoMock.existsByIdAndEmail(1, email)).thenReturn(true);
        assertTrue(accountService.existsAccountByIdAndEmail(1, email));
        assertFalse(accountService.existsAccountByIdAndEmail(1, "email@yandex.ru"));
        assertFalse(accountService.existsAccountByIdAndEmail(2, email));
    }

    @Test
    void testExistsAccountByIdAndIcq() {
        String icq = "123";
        when(accountDaoMock.existsByIdAndIcq(1, icq)).thenReturn(true);
        assertTrue(accountService.existsAccountByIdAndIcq(1, icq));
        assertFalse(accountService.existsAccountByIdAndIcq(1, "456"));
        assertFalse(accountService.existsAccountByIdAndIcq(2, icq));
    }

    @Test
    void testExistsAccountByIdAndSkype() {
        String skype = "skype";
        when(accountDaoMock.existsByIdAndSkype(1, skype)).thenReturn(true);
        assertTrue(accountService.existsAccountByIdAndSkype(1, skype));
        assertFalse(accountService.existsAccountByIdAndSkype(1, "SKYPE"));
        assertFalse(accountService.existsAccountByIdAndSkype(2, skype));
    }

    @Test
    void testExistsPhoneByNumber() {
        when(phoneDaoMock.existsByNumber("123")).thenReturn(true);
        assertTrue(accountService.existsPhoneByNumber("123"));
        assertFalse(accountService.existsPhoneByNumber("456"));
    }

    @Test
    void testExistsPhoneByAccountIdAndNumber() {
        when(phoneDaoMock.existsByAccountIdAndNumber(1, "123")).thenReturn(true);
        assertTrue(accountService.existsPhoneByAccountIdAndNumber(1, "123"));
        assertFalse(accountService.existsPhoneByAccountIdAndNumber(1, "456"));
        assertFalse(accountService.existsPhoneByAccountIdAndNumber(2, "123"));
    }

    @Test
    void testUpdatePhoto() {
        Account acc = new Account();
        doAnswer(invocationOnMock -> {
            acc.setPhotoId(1);
            return null;
        }).when(accountDaoMock).updatePhoto(1, 1);
        accountService.updateAccountAvatar(1, 1);
        assertEquals(1, acc.getPhotoId());
    }

    @Test
    void testIsAdmin() {
        when(accountDaoMock.isAdmin(1)).thenReturn(false);
        assertFalse(accountService.isAdmin(1));
    }

    @Test
    void testMakeAdmin() {
        AtomicBoolean admin = new AtomicBoolean(false);
        doAnswer(invocationOnMock -> {
            admin.set(true);
            return null;
        }).when(accountDaoMock).makeAdmin(1);
        accountService.makeAdmin(1);
        assertTrue(admin.get());
    }

    @Test
    void testIsFollower() {
        when(friendshipDaoMock.existsById(new FriendshipKey(1, 2))).thenReturn(true);
        when(friendshipDaoMock.existsById(new FriendshipKey(2, 1))).thenReturn(false);
        assertTrue(accountService.isFollower(1, 2));
    }

    @Test
    void testIsNotFollower() {
        when(friendshipDaoMock.existsById(any(FriendshipKey.class))).thenReturn(false);
        assertFalse(accountService.isFollower(1, 2));
    }

    @Test
    void testIsFriend() {
        when(friendshipDaoMock.existsById(any(FriendshipKey.class))).thenReturn(true);
        assertTrue(accountService.isFriend(1, 2));
    }

    @Test
    void testIsNotFriend() {
        when(friendshipDaoMock.existsById(any(FriendshipKey.class))).thenReturn(false);
        assertFalse(accountService.isFriend(1, 2));
    }

}