package com.getjavajob.training.ivshukovd.socialnetwork.service;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.ImageDao;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Image;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Optional.of;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ImageServiceTest {

    private ImageDao imageDao;
    private ImageService imageService;
    private List<Image> images;

    @BeforeEach
    void init() {
        imageDao = mock(ImageDao.class);
        imageService = new ImageService(imageDao);
        images = new ArrayList<>();
        Image image1 = new Image();
        image1.setId(1);
        image1.setContent(new byte[]{1, 2, 3});
        Image image2 = new Image();
        image2.setId(2);
        image2.setContent(new byte[]{4, 5, 6});
        images.add(image1);
        images.add(image2);
    }

    @Test
    void testGetImage() {
        when(imageDao.findById(1)).thenReturn(of(images.get(0)));
        assertEquals(1, imageService.getImage(1).get().getId());
    }

    @Test
    void testSaveImage() {
        Image newImage = new Image();
        newImage.setId(3);
        newImage.setContent(new byte[]{7, 8, 9});
        when(imageDao.save(any(Image.class))).thenReturn(newImage);
        assertEquals(3, imageService.saveImage(newImage.getContent()).getId());
    }

}