package com.getjavajob.training.ivshukovd.socialnetwork.service;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.AccountDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.DialogueDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.MessageDao;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Dialogue;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Message;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.time.LocalDateTime.now;
import static java.time.ZoneOffset.UTC;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.data.domain.PageRequest.of;

class DialogueServiceTest {

    private DialogueService dialogueService;
    private MessageDao messageDaoMock;
    private DialogueDao dialogueDaoMock;
    private AccountDao accountDaoMock;
    private List<Message> messages;

    @BeforeEach
    void init() {
        messageDaoMock = mock(MessageDao.class);
        dialogueDaoMock = mock(DialogueDao.class);
        accountDaoMock = mock(AccountDao.class);
        dialogueService = new DialogueService(messageDaoMock, dialogueDaoMock, accountDaoMock);
        messages = new ArrayList<>();
        messages.add(new Message());
        messages.add(new Message());
        messages.add(new Message());
    }

    @Test
    void testWriteMessage() {
        doAnswer(invocationOnMock -> {
            messages.add(new Message());
            return null;
        }).when(messageDaoMock).save(any(Message.class));
        when(accountDaoMock.findById(any(Long.class))).thenReturn(Optional.of(new Account()));
        when(dialogueDaoMock.findByAccountsId(any(Long.class), any(Long.class))).thenReturn(
                Optional.of(new Dialogue()));
        Message newMessage = new Message();
        newMessage.setAccountFrom(1);
        newMessage.setAccountTo(2);
        newMessage.setDate(now(UTC));
        dialogueService.writeMessage(newMessage);
        assertEquals(4, messages.size());
    }

    @Test
    void testGetMessages() {
        when(messageDaoMock.findByDialogueId(2, of(0, 5))).thenReturn(messages);
        assertEquals(3, dialogueService.getMessages(2, 1, 5).size());
    }

    @Test
    void testGetMessageNumberPages() {
        when(messageDaoMock.countByDialogueId(any(Long.class))).thenReturn(6L);
        assertEquals(2, dialogueService.getNumberMessagePages(2, 5));
    }

    @Test
    void testGetMessageNumberPages_ZeroPages() {
        when(messageDaoMock.countByDialogueId(any(Long.class))).thenReturn(0L);
        assertEquals(0, dialogueService.getNumberMessagePages(2, 5));
    }

    @Test
    void testCreateDialogue() {
        Dialogue newDialogue = new Dialogue();
        Account account1 = new Account();
        account1.setId(1);
        Account account2 = new Account();
        newDialogue.setAccount1(account1);
        newDialogue.setAccount2(account2);
        when(accountDaoMock.findById(1)).thenReturn(Optional.of(account1));
        when(accountDaoMock.findById(2)).thenReturn(Optional.of(account2));
        doAnswer(invocation -> {
            Dialogue dialogue = (Dialogue) invocation.getArguments()[0];
            if (newDialogue.getAccount1().equals(dialogue.getAccount1()) && newDialogue.getAccount2()
                                                                                       .equals(dialogue.getAccount2())) {
                return newDialogue;
            }
            return null;
        }).when(dialogueDaoMock).save(any(Dialogue.class));
        assertEquals(1, dialogueService.createDialogue(1, 2).getAccount1().getId());
    }

    @Test
    void testGetDialoguesByAccountsId() {
        Dialogue dialogue = new Dialogue();
        Account account1 = new Account();
        account1.setId(1);
        Account account2 = new Account();
        dialogue.setAccount1(account1);
        dialogue.setAccount2(account2);
        when(dialogueDaoMock.findByAccountsId(1, 2)).thenReturn(Optional.of(dialogue));
        assertEquals(1, dialogueService.getDialogueByAccountsId(2, 1).get().getAccount1().getId());
    }

    @Test
    void testGetDialogPartners() {
        Account account1 = new Account();
        account1.setId(1);
        Account account2 = new Account();
        account2.setId(2);
        Account account3 = new Account();
        account3.setId(3);
        Account account4 = new Account();
        account4.setId(4);
        Dialogue dialogue1 = new Dialogue();
        dialogue1.setAccount1(account1);
        dialogue1.setAccount2(account2);
        Dialogue dialogue2 = new Dialogue();
        dialogue2.setAccount1(account1);
        dialogue2.setAccount2(account3);
        Dialogue dialogue3 = new Dialogue();
        dialogue3.setAccount1(account4);
        dialogue3.setAccount2(account1);
        List<Dialogue> dialogues = asList(dialogue1, dialogue2, dialogue3);
        when(dialogueDaoMock.findByAccountIdOrderByLastUpdateDesc(1, of(0, 5))).thenReturn(dialogues);
        assertEquals(3, dialogueService.getDialogues(1, 1, 5).size());
    }

    @Test
    void testGetDialogNumberPages() {
        when(dialogueDaoMock.countDialogPartners(1)).thenReturn(6L);
        assertEquals(2, dialogueService.getNumberDialoguePages(1, 5));
    }

}