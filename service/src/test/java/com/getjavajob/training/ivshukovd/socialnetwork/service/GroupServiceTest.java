package com.getjavajob.training.ivshukovd.socialnetwork.service;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.GroupDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.GroupJoinRequestDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.GroupMemberDao;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Group;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupJoinRequest;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupMember;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.GroupDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupJoinRequest.GroupJoinRequestKey;
import static com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupMember.GroupMemberKey;
import static java.util.Optional.of;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class GroupServiceTest {

    private GroupDao groupDaoMock;
    private GroupMemberDao groupMemberDaoMock;
    private GroupJoinRequestDao groupJoinRequestDaoMock;
    private GroupService groupService;
    private List<Group> groups;

    @BeforeEach
    void init() {
        groupDaoMock = mock(GroupDao.class);
        groupMemberDaoMock = mock(GroupMemberDao.class);
        groupJoinRequestDaoMock = mock(GroupJoinRequestDao.class);
        groupService = new GroupService(groupDaoMock, groupMemberDaoMock, groupJoinRequestDaoMock);
        Group group1 = new Group();
        group1.setId(1);
        group1.setName("one");
        Group group2 = new Group();
        group2.setId(2);
        group2.setName("two");
        groups = new ArrayList<>();
        groups.add(group1);
        groups.add(group2);
    }

    @Test
    void testCreateGroup() {
        doAnswer(invocationOnMock -> {
            groups.add(new Group());
            return new Group();
        }).when(groupDaoMock).save(any(Group.class));
        long id = groupService.createGroup(new Group(), new Account());
        assertEquals(3, groups.size());
    }

    @Test
    void testUpdateGroup() {
        when(groupDaoMock.save(groups.get(1))).thenReturn(groups.get(1));
        when(groupDaoMock.findById(any(Long.class))).thenReturn(of(groups.get(1)));
        Group updatedGroup = new Group();
        updatedGroup.setId(2);
        updatedGroup.setName("three");
        groupService.updateGroup(updatedGroup);
        assertEquals("three", groups.get(1).getName());
    }

    @Test
    void testGetGroup() {
        when(groupDaoMock.findById(any(Long.class))).thenReturn(of(groups.get(1)));
        assertEquals("two", groupService.getGroup(2).get().getName());
    }

    @Test
    void testDeleteGroup() {
        doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            groups.remove(0);
            return null;
        }).when(groupDaoMock).delete(any(Group.class));
        when(groupDaoMock.findById(any(Long.class))).thenReturn(of(groups.get(0)));
        groupService.deleteGroup(1);
        assertEquals(1, groups.size());
    }

    @Test
    void testGetNumberPages() {
        when(groupDaoMock.countByNameContainingIgnoreCase(any(String.class))).thenReturn(6L);
        assertEquals(2, groupService.getNumberGroupPages("str", 5));
    }

    @Test
    void testGetNumberPagesById() {
        when(groupMemberDaoMock.countByMemberId(any(Long.class))).thenReturn(6L);
        assertEquals(2, groupService.getNumberGroupPagesByMemberId(1, 5));
    }

    @Test
    void testGetGroupsBySubstring() {
        List<GroupDto> groupDtos = new ArrayList<>();
        groupDtos.add(new GroupDto(1, "1", 0));
        groupDtos.add(new GroupDto(2, "2", 0));
        when(groupDaoMock.findBySubstring(any(String.class), eq(PageRequest.of(0, 5)))).thenReturn(groupDtos);
        assertEquals(2, groupService.getGroupsBySubstring("str", 1, 5).size());
    }

    @Test
    void testGetGroupsByMemberId() {
        when(groupMemberDaoMock.findAccountGroups(any(Long.class), eq(PageRequest.of(0, 5)))).thenReturn(groups);
        assertEquals(2, groupService.getGroupsByMemberId(1, 1, 5).size());
    }

    @Test
    void testUpdatePhoto() {
        Account acc = new Account();
        doAnswer(invocationOnMock -> {
            acc.setPhotoId(1);
            return null;
        }).when(groupDaoMock).updatePhoto(1, 1);
        groupService.updateGroupAvatar(1, 1);
        assertEquals(1, acc.getPhotoId());
    }

    @Test
    void testGetGroupMembersId() {
        List<Account> membersId =
                Arrays.asList(new Account(), new Account(), new Account(), new Account(), new Account());
        when(groupMemberDaoMock.findGroupMembers(any(Long.class), eq(PageRequest.of(0, 5)))).thenReturn(membersId);
        assertArrayEquals(membersId.toArray(), groupService.getGroupMembers(1, 1, 5).toArray());
    }

    @Test
    void testGetNumberGroupMembers() {
        when(groupMemberDaoMock.countByGroupId(any(Long.class))).thenReturn(5L);
        assertEquals(5, groupService.getNumberGroupMembers(1));
    }

    @Test
    void testGetNumberGroupMembersPages() {
        when(groupMemberDaoMock.countByGroupId(any(Long.class))).thenReturn(5L);
        assertEquals(2, groupService.getNumberGroupMemberPages(1, 4));
    }

    @Test
    void testIsMember() {
        when(groupMemberDaoMock.existsById(any(GroupMemberKey.class))).thenReturn(true);
        when(groupMemberDaoMock.findAccountRole(any(GroupMemberKey.class))).thenReturn("member");
        assertTrue(groupService.isMember(1, 1));
    }

    @Test
    void testIsNotMember() {
        when(groupMemberDaoMock.findAccountRole(any(GroupMemberKey.class))).thenReturn("admin");
        assertFalse(groupService.isMember(1, 1));
    }

    @Test
    void testIsAdmin() {
        when(groupMemberDaoMock.existsById(any(GroupMemberKey.class))).thenReturn(true);
        when(groupMemberDaoMock.findAccountRole(any(GroupMemberKey.class))).thenReturn("admin");
        assertTrue(groupService.isAdmin(1, 1));
    }

    @Test
    void testIsNotAdmin() {
        when(groupMemberDaoMock.findAccountRole(any(GroupMemberKey.class))).thenReturn("member");
        assertFalse(groupService.isAdmin(1, 1));
    }

    @Test
    void testIsCreator() {
        when(groupMemberDaoMock.existsById(any(GroupMemberKey.class))).thenReturn(true);
        when(groupMemberDaoMock.findAccountRole(any(GroupMemberKey.class))).thenReturn("creator");
        assertTrue(groupService.isCreator(1, 1));
    }

    @Test
    void testIsNotCreator() {
        when(groupMemberDaoMock.findAccountRole(any(GroupMemberKey.class))).thenReturn("member");
        assertFalse(groupService.isCreator(1, 1));
    }

    @Test
    void testIsGroupMember() {
        when(groupMemberDaoMock.existsById(any(GroupMemberKey.class))).thenReturn(true);
        assertTrue(groupService.isGroupMember(1, 1));
    }

    @Test
    void testIsNotGroupMember() {
        when(groupMemberDaoMock.existsById(any(GroupMemberKey.class))).thenReturn(false);
        assertFalse(groupService.isGroupMember(1, 1));
    }

    @Test
    void testMakeAdmin() {
        final String[] role = {"member"};
        doAnswer(invocationOnMock -> {
            role[0] = "admin";
            return null;
        }).when(groupMemberDaoMock).makeAdmin(any(Long.class), any(Long.class));
        groupService.makeAdmin(1, 1);
        when(groupMemberDaoMock.existsById(any(GroupMemberKey.class))).thenReturn(true);
        when(groupMemberDaoMock.findAccountRole(any(GroupMemberKey.class))).thenReturn(role[0]);
        assertTrue(groupService.isAdmin(1, 1));
    }

    @Test
    void testMakeMember() {
        final String[] role = {"admin"};
        doAnswer(invocationOnMock -> {
            role[0] = "member";
            return null;
        }).when(groupMemberDaoMock).makeMember(any(Long.class), any(Long.class));
        groupService.makeMember(1, 1);
        when(groupMemberDaoMock.existsById(any(GroupMemberKey.class))).thenReturn(true);
        when(groupMemberDaoMock.findAccountRole(any(GroupMemberKey.class))).thenReturn(role[0]);
        assertTrue(groupService.isMember(1, 1));
    }

    @Test
    void testAddGroupMember() {
        List<Integer> members = new ArrayList<>();
        doAnswer(invocationOnMock -> {
            members.add(1);
            return null;
        }).when(groupMemberDaoMock).save(any(GroupMember.class));
        groupService.addMember(new Group(), new Account());
        when(groupMemberDaoMock.existsById(any(GroupMemberKey.class))).thenReturn(members.size() > 0);
        assertTrue(groupService.isGroupMember(1, 1));
    }

    @Test
    void testDeleteGroupMember() {
        List<Integer> members = new ArrayList<>();
        members.add(1);
        doAnswer(invocationOnMock -> {
            members.remove(0);
            return null;
        }).when(groupMemberDaoMock).deleteById(any(GroupMemberKey.class));
        groupService.deleteGroupMember(1, 1);
        when(groupMemberDaoMock.existsById(any(GroupMemberKey.class))).thenReturn(members.size() > 0);
        assertFalse(groupService.isGroupMember(1, 1));
    }

    @Test
    void testIsJoinRequest() {
        when(groupJoinRequestDaoMock.existsById(any(GroupJoinRequestKey.class))).thenReturn(true);
        assertTrue(groupService.isJoinRequest(1, 1));
    }

    @Test
    void testIsNotJoinRequest() {
        when(groupJoinRequestDaoMock.existsById(any(GroupJoinRequestKey.class))).thenReturn(false);
        assertFalse(groupService.isJoinRequest(1, 1));
    }

    @Test
    void testGetNumberJoinRequestsPages() {
        when(groupJoinRequestDaoMock.countByGroupId(any(Long.class))).thenReturn(5L);
        assertEquals(2, groupService.getNumberJoinRequestPages(1, 4));
    }

    @Test
    void testGetJoinAccountId() {
        List<Account> membersId =
                Arrays.asList(new Account(), new Account(), new Account(), new Account(), new Account());
        when(groupJoinRequestDaoMock.findByGroupId(any(Long.class), eq(PageRequest.of(0, 5)))).thenReturn(membersId);
        assertArrayEquals(membersId.toArray(), groupService.getJoinAccounts(1, 1, 5).toArray());
    }

    @Test
    void testAddJoinRequest() {
        List<Integer> requests = new ArrayList<>();
        doAnswer(invocationOnMock -> {
            requests.add(1);
            return null;
        }).when(groupJoinRequestDaoMock).save(any(GroupJoinRequest.class));
        groupService.addGroupJoinRequest(new Group(), new Account());
        when(groupJoinRequestDaoMock.existsById(any(GroupJoinRequestKey.class))).thenReturn(requests.size() > 0);
        assertTrue(groupService.isJoinRequest(1, 1));
    }

    @Test
    void testDeleteJoinRequest() {
        List<Integer> requests = new ArrayList<>();
        requests.add(1);
        doAnswer(invocationOnMock -> {
            requests.clear();
            return null;
        }).when(groupJoinRequestDaoMock).deleteById(any(GroupJoinRequestKey.class));
        groupService.deleteGroupJoinRequest(1, 1);
        when(groupJoinRequestDaoMock.existsById(any(GroupJoinRequestKey.class))).thenReturn(requests.size() > 0);
        assertFalse(groupService.isJoinRequest(1, 1));
    }

}