package com.getjavajob.training.ivshukovd.socialnetwork.service;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.AccountDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.PostDao;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Post;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.PostDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.ivshukovd.socialnetwork.domain.Post.Type.ACCOUNT;
import static com.getjavajob.training.ivshukovd.socialnetwork.domain.Post.Type.GROUP;
import static java.util.Optional.of;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class PostServiceTest {

    private AccountDao accountDaoMock;
    private PostDao postDaoMock;
    private PostService postService;

    @BeforeEach
    void init() {
        accountDaoMock = mock(AccountDao.class);
        postDaoMock = mock(PostDao.class);
        postService = new PostService(accountDaoMock, postDaoMock);
    }

    @Test
    void testWriteGroupPost() {
        List<PostDto> posts = new ArrayList<>();
        posts.add(new PostDto());
        doAnswer(invocationOnMock -> {
            posts.add(new PostDto());
            return null;
        }).when(postDaoMock).save(any(Post.class));
        when(accountDaoMock.findById(any(Long.class))).thenReturn(of(new Account()));
        postService.writePostToGroupWall(new PostDto());
        assertEquals(2, posts.size());
    }

    @Test
    void testGetGroupPost() {
        List<Post> posts = new ArrayList<>();
        Post post = new Post();
        Account account = new Account();
        account.setFirstName("first");
        account.setSurName("second");
        account.setPhotoId(1);
        post.setAuthor(account);
        posts.add(post);
        when(postDaoMock.findByTargetIdAndTypeOrderByDateDesc(any(Long.class), eq(GROUP),
                                                              eq(PageRequest.of(0, 5)))).thenReturn(posts);
        when(accountDaoMock.findById(1)).thenReturn(of(account));
        assertEquals("first", postService.getGroupWallPosts(1, 1, 5).get(0).getAuthor().getFirstName());
    }

    @Test
    void testGetNumberGroupPostPages() {
        when(postDaoMock.countByTargetIdAndType(1, GROUP)).thenReturn(6);
        assertEquals(2, postService.getNumberGroupPostPages(1, 5));
    }

    @Test
    void testWriteAccountPost() {
        List<PostDto> posts = new ArrayList<>();
        posts.add(new PostDto());
        doAnswer(invocationOnMock -> {
            posts.add(new PostDto());
            return null;
        }).when(postDaoMock).save(any(Post.class));
        when(accountDaoMock.findById(any(Long.class))).thenReturn(of(new Account()));
        postService.writePostToAccountWall(new PostDto());
        assertEquals(2, posts.size());
    }

    @Test
    void testGetAccountPost() {
        List<Post> posts = new ArrayList<>();
        Account account = new Account();
        account.setFirstName("first");
        account.setSurName("second");
        account.setPhotoId(1);
        Post post = new Post();
        post.setAuthor(account);
        posts.add(post);
        when(postDaoMock.findByTargetIdAndTypeOrderByDateDesc(1, ACCOUNT, PageRequest.of(0, 5))).thenReturn(posts);
        assertEquals("first", postService.getAccountWallPosts(1, 1, 5).get(0).getAuthor().getFirstName());
    }

    @Test
    void testGetNumberAccountPostPages() {
        when(postDaoMock.countByTargetIdAndType(1, ACCOUNT)).thenReturn(6);
        assertEquals(2, postService.getNumberAccountPostPages(1, 5));
    }

    @Test
    void testIsPostAuthor() {
        when(postDaoMock.existsByIdAndAuthorId(1, 1)).thenReturn(true);
        assertTrue(postService.isAuthor(1, 1));
    }

    @Test
    void testDeletePost() {
        final int[] numberPosts = {1};
        doAnswer(invocation -> {
            numberPosts[0] = 0;
            return null;
        }).when(postDaoMock).deleteById(1);
        postService.deletePost(1);
        assertEquals(0, numberPosts[0]);
    }

}