package com.getjavajob.training.ivshukovd.socialnetwork.service;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.GroupDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.GroupJoinRequestDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.GroupMemberDao;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Group;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupJoinRequest;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupMember;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.GroupDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupJoinRequest.GroupJoinRequestKey;
import static com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupMember.GroupMemberKey;
import static java.lang.Math.ceil;
import static org.springframework.data.domain.PageRequest.of;

@Service
@Transactional(readOnly = true)
public class GroupService {

    private final GroupDao groupDao;
    private final GroupMemberDao groupMemberDao;
    private final GroupJoinRequestDao groupJoinRequestDao;

    @Autowired
    public GroupService(GroupDao groupDao, GroupMemberDao groupMemberDao, GroupJoinRequestDao groupJoinRequestDao) {
        this.groupDao = groupDao;
        this.groupMemberDao = groupMemberDao;
        this.groupJoinRequestDao = groupJoinRequestDao;
    }

    @Transactional
    public long createGroup(Group group, Account creator) {
        Group newGroup = groupDao.save(group);
        GroupMember groupMember = new GroupMember();
        groupMember.setId(new GroupMemberKey(group.getId(), creator.getId()));
        groupMember.setGroup(newGroup);
        groupMember.setAccount(creator);
        groupMember.setAccountRole("creator");
        groupMemberDao.save(groupMember);
        return newGroup.getId();
    }

    public Optional<Group> getGroup(long id) {
        return groupDao.findById(id);
    }

    /**
     * Searches for {@link GroupDto} which name contains <i>str</i>
     *
     * @param str  substring to search
     * @param page page number
     * @param size number {@link GroupDto} per page
     * @return the list of groups on <i>page</i>
     */
    public List<GroupDto> getGroupsBySubstring(String str, int page, int size) {
        return groupDao.findBySubstring(str, of(page - 1, size));
    }

    /**
     * Searches for {@link GroupDto} of account <i>id</i>
     *
     * @param id   of account
     * @param page page number
     * @param size number {@link Group} per page
     * @return the list of groups on <i>page</i>
     */
    public List<Group> getGroupsByMemberId(long id, int page, int size) {
        return groupMemberDao.findAccountGroups(id, of(page - 1, size));
    }

    public int getNumberGroupPages(String str, int recordsPerPage) {
        return (int) ceil(groupDao.countByNameContainingIgnoreCase(str) * 1.0 / recordsPerPage);
    }

    public int getNumberGroupPagesByMemberId(long memberId, int recordsPerPage) {
        return (int) ceil(groupMemberDao.countByMemberId(memberId) * 1.0 / recordsPerPage);
    }

    @Transactional
    public void updateGroup(Group updatedGroup) {
        groupDao.findById(updatedGroup.getId()).ifPresent(group -> {
            group.setName(updatedGroup.getName());
            group.setDescription(updatedGroup.getDescription());
            groupDao.save(group);
        });
    }

    @Transactional
    public void updateGroupAvatar(long photoId, long id) {
        groupDao.updatePhoto(photoId, id);
    }

    @Transactional
    public void deleteGroup(long id) {
        groupDao.findById(id).ifPresent(groupDao::delete);
    }

    @Transactional
    public void addMember(Group group, Account account) {
        GroupMember groupMember = new GroupMember();
        groupMember.setGroup(group);
        groupMember.setAccount(account);
        groupMember.setAccountRole("member");
        groupMember.setId(new GroupMemberKey(group.getId(), account.getId()));
        groupMemberDao.save(groupMember);
        groupJoinRequestDao.deleteById(new GroupJoinRequestKey(group.getId(), account.getId()));
    }

    public List<Account> getGroupMembers(long groupId, int page, int size) {
        return groupMemberDao.findGroupMembers(groupId, of(page - 1, size));
    }

    public long getNumberGroupMembers(long groupId) {
        return groupMemberDao.countByGroupId(groupId);
    }

    public int getNumberGroupMemberPages(long groupId, int recordsPerPage) {
        return (int) ceil(groupMemberDao.countByGroupId(groupId) * 1.0 / recordsPerPage);
    }

    @Transactional
    public void deleteGroupMember(long groupId, long accountId) {
        groupMemberDao.deleteById(new GroupMemberKey(groupId, accountId));
    }

    @Transactional
    public void addGroupJoinRequest(Group group, Account account) {
        GroupJoinRequest groupJoinRequest = new GroupJoinRequest();
        groupJoinRequest.setGroup(group);
        groupJoinRequest.setAccount(account);
        groupJoinRequest.setId(new GroupJoinRequestKey(group.getId(), account.getId()));
        groupJoinRequestDao.save(groupJoinRequest);
    }

    /**
     * Searches for {@link Account} from which join group request exists
     *
     * @param groupId group id
     * @param page    page number
     * @param size    number accounts per page
     * @return the list of account from which join group request exists
     */
    public List<Account> getJoinAccounts(long groupId, int page, int size) {
        return groupJoinRequestDao.findByGroupId(groupId, of(page - 1, size));
    }

    public int getNumberJoinRequestPages(long groupId, int recordsPerPage) {
        return (int) ceil(groupJoinRequestDao.countByGroupId(groupId) * 1.0 / recordsPerPage);
    }

    @Transactional
    public void deleteGroupJoinRequest(long groupId, long accountId) {
        groupJoinRequestDao.deleteById(new GroupJoinRequestKey(groupId, accountId));
    }

    /**
     * Checks the account <i>accountId</i> has role 'member' in group <i>groupId</i>
     *
     * @param groupId   group id
     * @param accountId account id
     * @return true if account has role 'member' otherwise false
     */
    public boolean isMember(long groupId, long accountId) {
        return "member".equals(getMemberRole(groupId, accountId));
    }

    /**
     * Checks the account <i>accountId</i> has role 'admin' in group <i>groupId</i>
     *
     * @param groupId   group id
     * @param accountId account id
     * @return true if account has role 'admin' otherwise false
     */
    public boolean isAdmin(long groupId, long accountId) {
        return "admin".equals(getMemberRole(groupId, accountId));
    }

    /**
     * Checks the account <i>accountId</i> has role 'creator' in group <i>groupId</i>
     *
     * @param groupId   group id
     * @param accountId account id
     * @return true if account has role 'creator' otherwise false
     */
    public boolean isCreator(long groupId, long accountId) {
        return "creator".equals(getMemberRole(groupId, accountId));
    }

    /**
     * Checks the account <i>accountId</i> is participant of group <i>groupId</i> (account's role in group doesn't
     * matter)
     *
     * @param groupId   group id
     * @param accountId account id
     * @return true if account is participant of group otherwise false
     */
    public boolean isGroupMember(long groupId, long accountId) {
        return groupMemberDao.existsById(new GroupMemberKey(groupId, accountId));
    }

    /**
     * Checks that join group <i>groupId</i> request from account <i>accountId</i> exists
     *
     * @param groupId   group id
     * @param accountId account id
     * @return true is join request exists otherwise false
     */
    public boolean isJoinRequest(long groupId, long accountId) {
        return groupJoinRequestDao.existsById(new GroupJoinRequestKey(groupId, accountId));
    }

    public String getMemberRole(long groupId, long accountId) {
        if (isGroupMember(groupId, accountId)) {
            return groupMemberDao.findAccountRole(new GroupMemberKey(groupId, accountId));
        } else {
            return "";
        }
    }

    /**
     * Changes account <i>accountId</i> role in group <i>groupId</i> to 'admin'
     *
     * @param groupId   group id
     * @param accountId account id to make 'admin'
     */
    @Transactional
    public void makeAdmin(long groupId, long accountId) {
        groupMemberDao.makeAdmin(groupId, accountId);
    }

    /**
     * Changes account <i>accountId</i> role in group <i>groupId</i> to 'member'
     *
     * @param groupId   group id
     * @param accountId account id to make 'member'
     */
    @Transactional
    public void makeMember(long groupId, long accountId) {
        groupMemberDao.makeMember(groupId, accountId);
    }

}