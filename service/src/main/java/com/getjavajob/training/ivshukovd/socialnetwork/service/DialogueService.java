package com.getjavajob.training.ivshukovd.socialnetwork.service;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.AccountDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.DialogueDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.MessageDao;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Dialogue;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static java.lang.Math.ceil;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.time.LocalDateTime.now;
import static java.time.ZoneOffset.UTC;
import static org.springframework.data.domain.PageRequest.of;

@Service
@Transactional(readOnly = true)
public class DialogueService {

    private final MessageDao messageDao;
    private final DialogueDao dialogueDao;
    private final AccountDao accountDao;

    @Autowired
    public DialogueService(MessageDao messageDao, DialogueDao dialogueDao, AccountDao accountDao) {
        this.messageDao = messageDao;
        this.dialogueDao = dialogueDao;
        this.accountDao = accountDao;
    }

    @Transactional
    public Message writeMessage(Message message) {
        long smallerAccountId = min(message.getAccountFrom(), message.getAccountTo());
        long biggerAccountId = max(message.getAccountFrom(), message.getAccountTo());
        dialogueDao.findByAccountsId(smallerAccountId, biggerAccountId).ifPresent(dialogue -> {
            dialogue.setLastUpdate(message.getDate());
            dialogueDao.save(dialogue);
            message.setDialogue(dialogue);
        });
        return messageDao.save(message);
    }

    public List<Message> getMessages(long dialogueId, int page, int size) {
        return messageDao.findByDialogueId(dialogueId, of(page - 1, size));
    }

    public int getNumberMessagePages(long dialogueId, int recordsPerPage) {
        return (int) ceil(messageDao.countByDialogueId(dialogueId) * 1.0 / recordsPerPage);
    }

    @Transactional
    public Dialogue createDialogue(long accountId1, long accountId2) {
        Dialogue dialogue = new Dialogue();
        long smallerAccountId = min(accountId1, accountId2);
        long biggerAccountId = max(accountId1, accountId2);
        accountDao.findById(smallerAccountId).ifPresent(dialogue::setAccount1);
        accountDao.findById(biggerAccountId).ifPresent(dialogue::setAccount2);
        dialogue.setLastUpdate(now(UTC));
        return dialogueDao.save(dialogue);
    }

    /**
     * Searches for {@link Dialogue} in which the account <i>id</i> participates
     *
     * @param id   of account
     * @param page page number
     * @param size number {@link Dialogue} per page
     * @return the list of dialogues on <i>page</i>
     */
    public List<Dialogue> getDialogues(long id, int page, int size) {
        return dialogueDao.findByAccountIdOrderByLastUpdateDesc(id, of(page - 1, size));
    }

    public Optional<Dialogue> getDialogueByAccountsId(long accountId1, long accountId2) {
        long smallerAccountId = min(accountId1, accountId2);
        long biggerAccountId = max(accountId1, accountId2);
        return dialogueDao.findByAccountsId(smallerAccountId, biggerAccountId);
    }

    public int getNumberDialoguePages(long id, int recordsPerPage) {
        return (int) ceil(dialogueDao.countDialogPartners(id) * 1.0 / recordsPerPage);
    }

}