package com.getjavajob.training.ivshukovd.socialnetwork.service;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.AccountDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.PostDao;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Post;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.PostDto;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.util.EntityDtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.getjavajob.training.ivshukovd.socialnetwork.domain.Post.Type.ACCOUNT;
import static com.getjavajob.training.ivshukovd.socialnetwork.domain.Post.Type.GROUP;
import static com.getjavajob.training.ivshukovd.socialnetwork.domain.util.EntityDtoConverter.convertPostDtoToEntity;
import static java.lang.Math.ceil;
import static org.springframework.data.domain.PageRequest.of;

@Service
@Transactional(readOnly = true)
public class PostService {

    private final AccountDao accountDao;
    private final PostDao postDao;

    @Autowired
    public PostService(AccountDao accountDao, PostDao postDao) {
        this.accountDao = accountDao;
        this.postDao = postDao;
    }

    @Transactional
    public void writePostToAccountWall(PostDto postDto) {
        Post post = convertPostDtoToEntity(postDto, new Post());
        accountDao.findById(postDto.getAuthor()).ifPresent(post::setAuthor);
        postDao.save(post);
    }

    public List<Post> getAccountWallPosts(long accountId, int page, int size) {
        return postDao.findByTargetIdAndTypeOrderByDateDesc(accountId, ACCOUNT, of(page - 1, size));
    }

    public int getNumberAccountPostPages(long id, int recordsPerPage) {
        return (int) ceil(postDao.countByTargetIdAndType(id, ACCOUNT) * 1.0 / recordsPerPage);
    }

    @Transactional
    public void writePostToGroupWall(PostDto postDto) {
        Post post = EntityDtoConverter.convertPostDtoToEntity(postDto, new Post());
        accountDao.findById(postDto.getAuthor()).ifPresent(post::setAuthor);
        postDao.save(post);
    }

    public List<Post> getGroupWallPosts(long groupId, int page, int size) {
        return postDao.findByTargetIdAndTypeOrderByDateDesc(groupId, GROUP, of(page - 1, size));
    }

    public int getNumberGroupPostPages(long id, int recordsPerPage) {
        return (int) ceil(postDao.countByTargetIdAndType(id, GROUP) * 1.0 / recordsPerPage);
    }

    @Transactional
    public void deletePost(long id) {
        postDao.deleteById(id);
    }

    public boolean isAuthor(long postId, long accountId) {
        return postDao.existsByIdAndAuthorId(postId, accountId);
    }

}