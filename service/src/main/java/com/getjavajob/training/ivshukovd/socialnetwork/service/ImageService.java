package com.getjavajob.training.ivshukovd.socialnetwork.service;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.ImageDao;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Image;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class ImageService {

    private final ImageDao imageDao;

    public ImageService(ImageDao imageDao) {
        this.imageDao = imageDao;
    }

    @Transactional
    public Image saveImage(byte[] img) {
        Image image = new Image();
        image.setContent(img);
        return imageDao.save(image);
    }

    public Optional<Image> getImage(long id) {
        return imageDao.findById(id);
    }

}
