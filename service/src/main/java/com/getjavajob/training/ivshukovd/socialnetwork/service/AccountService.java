package com.getjavajob.training.ivshukovd.socialnetwork.service;

import com.getjavajob.training.ivshukovd.socialnetwork.dao.AccountDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.DialogueDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.FriendshipDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.GroupDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.GroupJoinRequestDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.GroupMemberDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.ImageDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.PhoneDao;
import com.getjavajob.training.ivshukovd.socialnetwork.dao.PostDao;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Account;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.Friendship;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.GroupMember;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.AccountDto;
import com.getjavajob.training.ivshukovd.socialnetwork.domain.dto.AccountShortDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static com.getjavajob.training.ivshukovd.socialnetwork.domain.Friendship.FriendshipKey;
import static com.getjavajob.training.ivshukovd.socialnetwork.domain.util.EntityDtoConverter.convertAccountDtoToEntity;
import static java.lang.Math.ceil;
import static java.util.Objects.isNull;
import static org.springframework.data.domain.PageRequest.of;

@Service
@Transactional(readOnly = true)
public class AccountService {

    private final AccountDao accountDao;
    private final PhoneDao phoneDao;
    private final ImageDao imageDao;
    private final FriendshipDao friendshipDao;
    private final PostDao postDao;
    private final DialogueDao dialogueDao;
    private final GroupDao groupDao;
    private final GroupJoinRequestDao groupJoinRequestDao;
    private final GroupMemberDao groupMemberDao;

    @Autowired
    public AccountService(AccountDao accountDao, PhoneDao phoneDao, ImageDao imageDao, FriendshipDao friendshipDao,
                          PostDao postDao, DialogueDao dialogueDao, GroupDao groupDao,
                          GroupJoinRequestDao groupJoinRequestDao, GroupMemberDao groupMemberDao) {
        this.accountDao = accountDao;
        this.phoneDao = phoneDao;
        this.imageDao = imageDao;
        this.friendshipDao = friendshipDao;
        this.postDao = postDao;
        this.dialogueDao = dialogueDao;
        this.groupDao = groupDao;
        this.groupJoinRequestDao = groupJoinRequestDao;
        this.groupMemberDao = groupMemberDao;
    }

    @Transactional
    public Account addAccount(Account account) {
        accountDao.save(account);
        return account;
    }

    /**
     * Searches for {@link AccountShortDto} which firstName and/or surName contains <i>str</i>
     *
     * @param str  substring to search
     * @param page page number
     * @param size number {@link AccountShortDto} per page
     * @return the list of accounts on <i>page</i>
     */
    public List<AccountShortDto> getAccountsBySubstr(String str, int page, int size) {
        return accountDao.findBySubstring(str, of(page - 1, size));
    }

    /**
     * Searches for {@link AccountShortDto} which firstName contains <i>firstName</i> and surName contains
     * <i>surName</i>
     *
     * @param firstName string to search
     * @param surName   string to search
     * @param page      page number
     * @param size      number {@link AccountShortDto} per page
     * @return the list of accounts on <i>page</i>
     */
    public List<AccountShortDto> getAccountsByFullName(String firstName, String surName, int page, int size) {
        return accountDao.findBySubstring(firstName, surName, of(page - 1, size));
    }

    public Optional<Account> getAccount(long id) {
        return accountDao.findById(id);
    }

    public Optional<Account> getAccountWithPhones(long id) {
        return accountDao.findAccountWithPhones(id);
    }

    public Optional<Account> getAccountByEmail(String email) {
        return accountDao.findByEmail(email);
    }

    public int getNumberAccountPages(String str, int recordsPerPage) {
        return (int) ceil(
                accountDao.countByFirstNameContainingOrSurNameContainingAllIgnoreCase(str, str) * 1.0 / recordsPerPage);
    }

    public int getNumberAccountPages(String surName, String firstName, int recordsPerPage) {
        return (int) ceil(
                accountDao.countByFirstNameContainingAndSurNameContainingAllIgnoreCase(firstName, surName) * 1.0
                        / recordsPerPage);
    }

    /**
     * Checks if account with <i>id</i> is social network admin
     *
     * @param id of account
     * @return true if account is social network admin otherwise false
     */
    public boolean isAdmin(long id) {
        return accountDao.isAdmin(id);
    }

    /**
     * Make account <i>id</i> social network admin
     *
     * @param id of account
     */
    @Transactional
    public void makeAdmin(long id) {
        accountDao.makeAdmin(id);
    }

    @Transactional
    public void updateAccount(Account account) {
        accountDao.save(account);
    }

    @Transactional
    public void updateAccount(AccountDto accountDto) {
        accountDao.findById(accountDto.getId()).ifPresent(account -> {
            Account updatedAccount = convertAccountDtoToEntity(accountDto, account);
            accountDao.save(updatedAccount);
        });
    }

    @Transactional
    public void updateAccountAvatar(long photoId, long id) {
        accountDao.updatePhoto(photoId, id);
    }

    @Transactional
    public void deleteAccount(long id) {
        postDao.deleteAllAccountPosts(id);
        dialogueDao.deleteAllByAccountId(id);
        friendshipDao.deleteAllByAccountId(id);
        accountDao.findById(id).ifPresent(account -> {
            if (account.getPhotoId() > 0) {
                imageDao.deleteById(account.getPhotoId());
            }
        });
        groupJoinRequestDao.deleteAllByAccountId(id);
        List<GroupMember> groupMembers = groupMemberDao.findAllByAccountIdAndAccountRole(id, "creator");
        if (!groupMembers.isEmpty()) {
            for (GroupMember gm : groupMembers) {
                if (groupMemberDao.countByGroupId(gm.getGroup().getId()) == 1) {
                    groupDao.delete(gm.getGroup());
                } else {
                    Account admin = groupMemberDao.findFirstByGroupIdAndAccountRole(gm.getGroup().getId(), "admin");
                    if (!isNull(admin)) {
                        groupMemberDao.makeCreator(gm.getGroup().getId(), admin.getId());
                    } else {
                        Account member =
                                groupMemberDao.findFirstByGroupIdAndAccountRole(gm.getGroup().getId(), "member");
                        groupMemberDao.makeCreator(gm.getGroup().getId(), member.getId());
                    }
                }
            }
        }
        groupMemberDao.deleteAllByAccountId(id);
        accountDao.deleteById(id);
    }

    @Transactional
    public void addRelationship(long accountFromId, long accountToId) {
        Friendship friendship = new Friendship();
        accountDao.findById(accountFromId).ifPresent(friendship::setAccountFrom);
        accountDao.findById(accountToId).ifPresent(friendship::setAccountTo);
        friendship.setId(new FriendshipKey(accountFromId, accountToId));
        friendshipDao.save(friendship);
    }

    public List<Account> getFriends(long id, int page, int size) {
        return friendshipDao.findFriends(id, of(page - 1, size));
    }

    public boolean isFriend(long accountId1, long accountId2) {
        return friendshipDao.existsById(new FriendshipKey(accountId1, accountId2)) && friendshipDao.existsById(
                new FriendshipKey(accountId2, accountId1));
    }

    public int getNumberOfPagesInFriendList(long id, int recordsPerPage) {
        return (int) ceil(friendshipDao.countFriends(id) * 1.0 / recordsPerPage);
    }

    @Transactional
    public void deleteFriend(long accountIdFrom, long accountIdTo) {
        friendshipDao.deleteById(new FriendshipKey(accountIdFrom, accountIdTo));
        friendshipDao.deleteById(new FriendshipKey(accountIdTo, accountIdFrom));
    }

    public List<Account> getFollowers(long id, int page, int size) {
        return friendshipDao.findFollowers(id, of(page - 1, size));
    }

    public boolean isFollower(long accountRequestFrom, long accountRequestTo) {
        return friendshipDao.existsById(new FriendshipKey(accountRequestFrom, accountRequestTo))
                && !friendshipDao.existsById(new FriendshipKey(accountRequestTo, accountRequestFrom));
    }

    public int getNumberOfPagesInFollowerList(long id, int recordsPerPage) {
        return (int) ceil(friendshipDao.countFollowers(id) * 1.0 / recordsPerPage);
    }

    @Transactional
    public void deleteFollower(long accountIdFrom, long accountIdTo) {
        friendshipDao.deleteById(new FriendshipKey(accountIdFrom, accountIdTo));
    }

    public boolean existsAccountByEmail(String email) {
        return accountDao.existsByEmail(email);
    }

    public boolean existsAccountByIcq(String icq) {
        return accountDao.existsByIcq(icq);
    }

    public boolean existsAccountBySkype(String skype) {
        return accountDao.existsBySkype(skype);
    }

    public boolean existsPhoneByNumber(String number) {
        return phoneDao.existsByNumber(number);
    }

    public boolean existsAccountByIdAndEmail(long id, String email) {
        return accountDao.existsByIdAndEmail(id, email);
    }

    public boolean existsAccountByIdAndIcq(long id, String icq) {
        return accountDao.existsByIdAndIcq(id, icq);
    }

    public boolean existsAccountByIdAndSkype(long id, String skype) {
        return accountDao.existsByIdAndSkype(id, skype);
    }

    public boolean existsPhoneByAccountIdAndNumber(long accountId, String number) {
        return phoneDao.existsByAccountIdAndNumber(accountId, number);
    }

    public boolean existsAccountById(long id) {
        return accountDao.existsById(id);
    }

}